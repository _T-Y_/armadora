#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "fin_de_partie.h"
#include "controle.h"
#include "variables_globales.h"
#include "interface_graphique_fin_de_jeu.h"
#include "initialisation.h"
//--------------------------------------------------------------------------
// 							Variable globale
//--------------------------------------------------------------------------

/*
*	Structure de donn�es pour chaque zone form�e
*/
struct zone
{
	int mine;
	int orZone;
	int vainqueur;
	int point;
	int joueur1;
	int joueur2;
	int joueur3;
	int joueur4;
	int equipe1;
	int equipe2;
	int coordzone[50][2];//x y
	int numZone; 
};
struct zone datazone[20];

int zone;
int indiceMemo;

/*
PARTIE 1 = Initialisation
PARTIE 2 = Recherche si mur prsent
PARTIE 3 = Recherche zone
PARTIE 4 = Recherche si reste mur bordure
PARTIE 5 = Comptabilise les points
PARTIE 6 = Recherche interieur
*/

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 1
//-------------------------------------------------------------------------------------------------------------

/*
*	Initialisation de toutes les variables utilis�es
*/
void Initialisation(void) // Page 2
{
	int i,j;
	
	InitMemoireChemin();// initialisation.c
	InitMemoireCarrefour();// initialisation.c
	
	for(i=0;i<20;i++)
	{
		datazone[i].mine = -1;
		datazone[i].vainqueur = -1;
		datazone[i].point = -1;
		datazone[i].joueur1 = 0;
		datazone[i].joueur2 = 0;
		datazone[i].joueur3 = 0;
		datazone[i].joueur4 = 0;
		datazone[i].equipe1 = 0;
		datazone[i].equipe2 = 0;
		datazone[i].orZone = 0;
		for(j=0;j<50;j++)
		{
			datazone[i].coordzone[j][0] = -1;//x
			datazone[i].coordzone[j][1] = -1;//y
		}
	}
	
	chemin[0].x=0;
	chemin[0].y=1;
	chemin[0].direction = 3; // bas
	
	value.x = 0;
	value.y = 1;
	
	memo.x = 0;
	memo.y = 1;
	
	indiceChemin = 1;
	
	zone = 1;
	
	for(i=0;i<20;i++)
	{
		tabOrigine[i].x = -1;
		tabOrigine[i].y = -1;
		tabChoix1[i].x = -1;
		tabChoix1[i].y = -1;
		tabChoix2[i].x = -1;
		tabChoix2[i].y = -1;
		tabChoix3[i].x = -1;
		tabChoix3[i].y = -1;
	}
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 2
//-------------------------------------------------------------------------------------------------------------
/*
*	Les fonctions ci dessous recherches de nouvelles coordonn�es.
*	La recherche s'effectue en suivant le chemin des bordures.
*	Si un carrefour est rencontr�, alors une variable boolean se met � true.
*	Ce qui signifie que le programme appellera les fonctions de recherche de zone
*/

/*
*	Recherche coordonn� � droite
*/
_Bool RechercheDroite (int x,int y) // Page 4
{
//	printf("\n\nRecherche Droite");
	if(x == 15) // Si fin de mur bordure  droite
	{
		chemin[indiceChemin].x = 16;
		chemin[indiceChemin].y = 9;
		chemin[indiceChemin].direction = 1;//haut
		value.x = 16;
		value.y = 9;
		memo.x = value.x;
		memo.y = value.y;
		memo.direction = 1;
		indiceChemin++;
		indiceMemo = indiceChemin;
//		printf("\nindiceMemo = %d",indiceMemo);
//		printf("\nvalue.x = %d",value.x);
//		printf("\nvalue.y = %d",value.y);
		return false;
	}
	else
	{
		if(plateau[y-1][x+1].mur == 1) // Si mur present haut droite
		{
			value.x += 1;
			value.y -= 1;
			chemin[indiceChemin].x = value.x;
			chemin[indiceChemin].y = value.y;
			chemin[indiceChemin].direction = 1; // haut
			indiceChemin++;
//			printf("\nvalue.x = %d",value.x);
//			printf("\nvalue.y = %d",value.y);
			return true;
		}
		else
		{
			value.x += 2;
			chemin[indiceChemin].x = value.x;
			chemin[indiceChemin].y = value.y;
			chemin[indiceChemin].direction = 2; // droite
			indiceChemin++;
			indiceMemo = indiceChemin;
//			printf("\nindiceMemo = %d",indiceMemo);
			memo.x = value.x;
			memo.y = value.y;
			memo.direction = 2;
//			printf("\nvalue.x = %d",value.x);
//			printf("\nvalue.y = %d",value.y);
			return false;
		}
	}
}

/*
*	Recherche coordonn� � gauche
*/
_Bool RechercheGauche (int x,int y) // Page 5
{
//	printf("\n\nRecherche Gauche");
/*	
	if(	((value.x-1 == 0 || value.x-1 == 16) && ((value.y-1)%2 == 1)) && ((value.y-1 == 0 || value.y-1 == 10)&& ((value.x-1)%2 == 1))	)
	{
		printf("\nmur borudre");
		if(plateau[y+1][x-1].zone1 == -1)
		{
			
		}			
	}
	*/
	if(plateau[y+1][x-1].mur == 1) // Si mur present bas gauche
	{
		value.x -= 1;
		value.y += 1;
		chemin[indiceChemin].x = value.x;
		chemin[indiceChemin].y = value.y;
		chemin[indiceChemin].direction = 3; // bas
/*		memo.x = value.x;
		memo.y = value.y;
		memo.direction = 3;
		indiceMemo = indiceChemin;*/
		indiceChemin++;
//		printf("\nvalue.x = %d",value.x);
//		printf("\nvalue.y = %d",value.y);
		return true;
	}
	else
	{
		value.x -= 2;
		chemin[indiceChemin].x = value.x;
		chemin[indiceChemin].y = value.y;
		chemin[indiceChemin].direction = 4; // gauche
		indiceChemin++;
		indiceMemo = indiceChemin;
//		printf("\nindiceMemo = %d",indiceMemo);
		memo.x = value.x;
		memo.y = value.y;
		memo.direction = 4; // gauche
//		printf("\nvalue.x = %d",value.x);
//		printf("\nvalue.y = %d",value.y);
		return false;
	}
}

/*
*	Recherche coordonn� haute
*/
_Bool RechercheHaut (int x,int y) // Page 6
{
//	printf("\n\nRecherche Haut");
	if(y == 1) // Si fin de bordure haut 
	{
		chemin[indiceChemin].x = 15;
		chemin[indiceChemin].y = 0;
		chemin[indiceChemin].direction = 4;
		value.x = 15;
		value.y = 0;
		memo.x = value.x;
		memo.y = value.y;
		memo.direction = 4;
		indiceChemin++;		
		indiceMemo = indiceChemin;
//		printf("\nindiceMemo = %d",indiceMemo);
//		printf("\nvalue.x = %d",value.x);
//		printf("\nvalue.y = %d",value.y);
		return false;
	}
	else
	{
		if(plateau[y-1][x-1].mur == 1)// Si mur present haut gauche
		{
			value.x -= 1;
			value.y -= 1;
			chemin[indiceChemin].x = value.x;
			chemin[indiceChemin].y = value.y;
			chemin[indiceChemin].direction = 4; // gauche
			indiceChemin++;			
//			printf("\nvalue.x = %d",value.x);
//			printf("\nvalue.y = %d",value.y);
			return true;
		}
		else
		{
			value.y -= 2;
			chemin[indiceChemin].x = value.x;
			chemin[indiceChemin].y = value.y;
			chemin[indiceChemin].direction = 1; // haut
			indiceChemin++;
			indiceMemo = indiceChemin;
//			printf("\nindiceMemo = %d",indiceMemo);
			memo.x = value.x;
			memo.y = value.y;
			memo.direction = 1;		
//			printf("\nvalue.x = %d",value.x);
//			printf("\nvalue.y = %d",value.y);
			return false;
		}
	}
}

/*
*	Recherche coordonn� bas
*/
_Bool RechercheBas (int x,int y) // Page 7
{
//	printf("\n\nRecherche Bas");
	if(y == 9) // Si fin de bordure bas
	{
		chemin[indiceChemin].x = 1;
		chemin[indiceChemin].y = 10;
		chemin[indiceChemin].direction = 2; // droite
		value.x = 1;
		value.y = 10;
		memo.x = value.x;
		memo.y = value.y;
		memo.direction = 2; // droite
		indiceChemin++;
		indiceMemo = indiceChemin;
//		printf("\nindiceMemo = %d",indiceMemo);
//		printf("\nvalue.x = %d",value.x);
//		printf("\nvalue.y = %d",value.y);
		return false;
	}
	else
	{
		if(plateau[y+1][x+1].mur == 1)//Si mur present bas droite
		{
			value.x += 1;
			value.y += 1;
			chemin[indiceChemin].x = value.x;
			chemin[indiceChemin].y = value.y;
			chemin[indiceChemin].direction = 2; // droite
			indiceChemin++;
//			printf("\nvalue.x = %d",value.x);
//			printf("\nvalue.y = %d",value.y);
			return true;
		}
		else
		{
			value.y += 2;
			chemin[indiceChemin].x = value.x;
			chemin[indiceChemin].y = value.y;
			chemin[indiceChemin].direction = 3; // bas
			indiceChemin++;
			indiceMemo = indiceChemin;
//			printf("\nindiceMemo = %d",indiceMemo);
			memo.x = value.x;
			memo.y = value.y;
			memo.direction = 3;
//			printf("\nvalue.x = %d",value.x);
//			printf("\nvalue.y = %d",value.y);
			return false;
		}
	}
}

/*
*	Distingue des coordonn�es horizontal � vertical.
*	Puis appelle la fonction de recherche de nouvelle coordonn�e en fonction de son orientation, puis de son sens
*/
_Bool RechercheNouvelleCoord(int x, int y)//Page 3
{
	_Bool var;
	
	if((x%2 == 1) && (y%2 == 0)) // Si mur horizontal
	{
//		printf("\nTest horizontal ok");
		if(chemin[indiceChemin-1].direction == 4) // si direction = gauche
		{
			var = RechercheGauche(x,y);
		}
		else
		{
			var = RechercheDroite(x,y);
		}
	}
	else
	{
//		printf("\nTest vertical ok");
		if(chemin[indiceChemin-1].direction == 3) // si direction = bas
		{
			var = RechercheBas(x,y);
		}
		else
		{
			var = RechercheHaut(x,y);
		}
	}
//	printf("\nvar = %d",var);
//	printf("\nChemin[%d].x=%d",indiceChemin-1,chemin[indiceChemin-1].x);
//	printf("\nChemin[%d].y=%d",indiceChemin-1,chemin[indiceChemin-1].y);
//	printf("\nChemin[%d].direction=%d",indiceChemin-1,chemin[indiceChemin-1].direction);
//	printf("\nIndice Chemin = %d",indiceChemin);
	return var;
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 3
//-------------------------------------------------------------------------------------------------------------
/*
*	Lors d'une rencontre avec un carrefour dans la recherche de zone, il faut d�terminer quel 
*	chemin prendre pour avoir la formation de la zon complette et juste.
*	pour cela, le programme va comptabilis� le nombre de mur pr�sent sur l'axe des differents choix,
*	puis, si sur un axe il y a les coordonn�es d'un mur pr�sent sur le chemin, alors la somme sera retenu.
*	Pour d�terminer quel chemin prendre, le choix avec la comptabilisation la plus faible sera selectionn�.
*/
/*
*	Compte le nombre de mur pr�sent en bas
*/
int ComptabilisationNbYMurBas(int x, int y, int Y) // Page 20
{
	int i;
	int compte = 0;
	
	if(Y != -1)
	{
		for(i=y; i<=Y; i++)
		{
			if(plateau[i][x].mur == 1)
			{
				compte++;
			}
		}
	}
	
	return compte;
}

/*
*	Compte le nombre de mur pr�sent � gauche
*/
int ComptabilisationNbXMurGauche(int x, int y, int X) // Page 21
{
	int i;
	int compte = 0;
	
	if(X != -1)
	{
		for(i=X; i<=x; i++)
		{
			if(plateau[y][i].mur == 1)
			{
				compte++;
			}
		}
	}
	
	return compte;
}

/*
*	recherche si sur un axe, les coordonn�es x correspondes avec celle du Chemin deja parcouru
*	Ne retient que la coordonn�e la plus grande
*/
int RechercheCoordXGauche(int x, int y) // Page 16
{
	int i;
	int X = x;
	int memo = X;
	
	for(i=0;i<indiceChemin;i++)
	{
		if(chemin[i].y == y)
		{
			if(chemin[i].x<X)
			{
				X = chemin[i].x;
			}
		}
	}	
	if(memo == X)
	{
		X = -1;
	}
	return X;
}

/*
*	recherche si sur un axe, les coordonn�es y correspondes avec celle du Chemin deja parcouru
*	Ne retient que la coordonn�e la plus grande
*/
int RechercheCoordYBas(int x, int y) // Page 17
{
	int i;
	int Y = -1;
	
	for(i=0;i<indiceChemin;i++)
	{
		if(chemin[i].x == x)
		{
			if(chemin[i].y>Y)
			{
				Y = chemin[i].y;
			}
		}
	}
	return Y;
}

/*
*	Compte le nombre de mur pr�sent en haut
*/
int ComptabilisationNbYMurHaut(int x, int y, int Y) // Page 18
{
	int i;
	int compte = 0;
	
	if(Y != -1)
	{
		for(i=Y; i<=y; i++)
		{
			if(plateau[i][x].mur == 1)
			{
				compte++;
			}
		}
	}	
	return compte;
} 

/*
*	Compte le nombre de mur pr�sent en bas
*/
int ComptabilisationNbXMurDroit(int x, int y, int X) // Page 19
{
	int i;
	int compte = 0;
	
	if(X != -1)
	{
		for(i=x; i<=X; i++)
		{
			if(plateau[y][i].mur == 1)
			{
				compte++;
			}
		}
	}
	return compte;
}

/*
*	recherche si sur un axe, les coordonn�es x correspondes avec celle du Chemin deja parcouru
*	Ne retient que la coordonn�e la plus grande
*/
int RechercheCoordXDroit(int x, int y) // Page 14
{
	int i;
	int X = -1;
	
	for(i=0;i<indiceChemin;i++)
	{
		if(chemin[i].y == y)
		{
			if(chemin[i].x>X)
			{
				X = chemin[i].x;
			}
		}
	}
	return X;
}


/*
*	recherche si sur un axe, les coordonn�es y correspondes avec celle du Chemin deja parcouru
*	Ne retient que la coordonn�e la plus grande
*/
int RechercheCoordYHaut(int x, int y) // Page 15
{
	int i;
	int Y = y;
	int memo = Y;
	
	for(i=0;i<indiceChemin;i++)
	{
		if(chemin[i].x == x)
		{
			if(chemin[i].y<Y)
			{
				Y = chemin[i].y;
			}
		}
	}
	if(memo == Y)
	{
		Y=-1;
	}
	return Y;
}

/*
*	Fonction qui differencie l'orientation des coordonn�es �tudi�es, puis appel des fonctions
*	pour comptabiliser le nombre de mur pour faire un choix lors d'un carrefour.
*/
int ComptabilisationMur(int x, int y) // Page 13
{
	if((x%2 == 1) && (y%2 == 0)) // Si mur horizontal
	{
		int Y = -1;
		int compte = 0;	
//		printf("\n\nRechercheCoordYHaut");
		Y = RechercheCoordYHaut (x,y); // Page 15
//		printf("\nY = %d",Y);
//		printf("\n\nComptabilisationNbYMurHaut");
		compte = ComptabilisationNbYMurHaut(x,y,Y); // Page 18
//		printf("\ncompte = %d",compte);
		Y = -1;
//		printf("\n\nRechercheCoordYBas");
		Y = RechercheCoordYBas (x,y); // Page 17
//		printf("\nY = %d",Y);
//		printf("\n\nComptabilisationNbYMurBas");
		compte += ComptabilisationNbYMurBas(x,y,Y); // Page 20
//		printf("\ncompte = %d",compte);
		return compte;
	}
	else
	{
		int X = -1;
		int compte = 0;
//		printf("\n\nRechercheCoordXDroit");
		X = RechercheCoordXDroit(x,y); // Page 14
//		printf("\nX = %d",X);
//		printf("\n\nComptabilisationNbXMurDroit");
		compte = ComptabilisationNbXMurDroit(x,y,X); // Page 19
//		printf("\ncompte = %d",compte);
		X = -1;
//		printf("\n\nRechercheCoordXGauche");
		X = RechercheCoordXGauche (x,y); // Page 16
//		printf("\nX = %d",X);
//		printf("\n\nComptabilisationNbXMurGauche");
		compte += ComptabilisationNbXMurGauche(x,y,X); // Page 21
//		printf("\ncompte = %d",compte);
		return compte;
	}
}

/*
*	Ci dessous, 4 fonctions d'initialisations.
*	Lorsque le compte des murs pr�sents sur les axes sont tous �gales � 0, il faut utiliser une autre technique.
*	Les tableaux prennent les coordonn�es x ou y en fonction de l'orientation des choix.
*	Exemple, si le choix 1 est un mur vertical, alors chaque ca valeur y sera fix�. Le tableau prendra alors toutes les valeurs de x, pour cette valeur de y.
*	Idem pour le premier mur de chemin, l'origine.	
*	Quand on fera crois� les tableaux avec celui de l'origine, 1 seul se croisera. Ce sera lui le bon choix � prendre pour form� une zone.
*/
/*
*	Fonctiond'initialisation
*/
void InitTabOrigine(void) // Page 23
{
	int i;
	
	if((chemin[0].x%2 == 1) && (chemin[0].y%2 == 0)) // Si mur horizontal
	{
		int x = chemin[0].x;
		for(i=0;i<=10;i++)
		{
			tabOrigine[i].x = x;
			tabOrigine[i].y = i;
		}
	}
	else
	{
		int y = chemin[0].y;
		for(i=0;i<=16;i++)
		{
			tabOrigine[i].x = i;
			tabOrigine[i].y = y;
		}
	}
}

/*
*	Fonctiond'initialisation
*/
void InitTabChoix1(void) // Page 24
{
	int i;
	
	if((carrefour[indiceCarrefour-1].xchoix1%2 == 1) && (carrefour[indiceCarrefour-1].ychoix1%2 == 0)) // Si mur horizontal
	{
		int x = carrefour[indiceCarrefour-1].xchoix1;
		for(i=0;i<=10;i++)
		{
			tabChoix1[i].x = x;
			tabChoix1[i].y = i;
		}
	}
	else
	{
		int y = carrefour[indiceCarrefour-1].ychoix1;
		for(i=0;i<=16;i++)
		{
			tabChoix1[i].x = i;
			tabChoix1[i].y = y;
		}
	}
}

/*
*	Fonctiond'initialisation
*/
void InitTabChoix2(void) // Page 25
{
	int i;
	
	if((carrefour[indiceCarrefour-1].xchoix2%2 == 1) && (carrefour[indiceCarrefour-1].ychoix2%2 == 0)) // Si mur horizontal
	{
		int x = carrefour[indiceCarrefour-1].xchoix2;
		for(i=0;i<=10;i++)
		{
			tabChoix2[i].x = x;
			tabChoix2[i].y = i;
		}
	}
	else
	{
		int y = carrefour[indiceCarrefour-1].ychoix2;
		for(i=0;i<=16;i++)
		{
			tabChoix2[i].x = i;
			tabChoix2[i].y = y;
		}
	}
}

/*
*	Fonctiond'initialisation
*/
void InitTabChoix3(void) // Page 26
{
	int i;
	
	if((carrefour[indiceCarrefour-1].xchoix3%2 == 1) && (carrefour[indiceCarrefour-1].ychoix3%2 == 0))// Si mur horizontal
	{
		int x = carrefour[indiceCarrefour-1].xchoix3;
		for(i=0;i<=10;i++)
		{
			tabChoix3[i].x = x;
			tabChoix3[i].y = i;
		}
	}
	else
	{
		int y = carrefour[indiceCarrefour-1].ychoix3;
		for(i=0;i<=16;i++)
		{
			tabChoix3[i].x = i;
			tabChoix3[i].y = y;
		}
	}
}

/*
*	Fonction qui est appeler pour initialiser les tableaux de choix, et origine
*/
void InitTab(void) // Page 27
{
	InitTabOrigine();  // Page 23
	if(carrefour[indiceCarrefour-1].flagchoix1 == 1)
	{
		InitTabChoix1();  // Page 24
	}
	if(carrefour[indiceCarrefour-1].flagchoix2 == 1)
	{
		InitTabChoix2();  // Page 25
	}
	if(carrefour[indiceCarrefour-1].flagchoix3 == 1)
	{
		InitTabChoix3();  // Page 26
	}
}

/*
*	Les 3 boucles ci dessous croisent les donn�es x et y des tableaux choix avec origine
*	Si une valeur de origine et du tableau se croise, alors ce sera ce choix qui sera selectionn� dans chemin
*/
/*
Boucle de croisement des valeurs x et y entre origine est choix 1
*/
int Boucle1(int var) // Page 28
{
	int i,j;
	int stop = 0;
	
	for(i=0;i<20;i++)
	{
//		printf("\ntabOrigine[%d].x = %d",i,tabOrigine[i].x);
//		printf("\ntabOrigine[%d].y = %d",i,tabOrigine[i].y);
		
		if(tabOrigine[i].x == -1)
		{
//			printf("\nFin de tab origine");
			break;
		}
		for(j=0;j<20;j++)
		{
//			printf("\ntabChoix1[%d].x = %d",j,tabChoix1[j].x);
//			printf("\ntabChoix1[%d].y = %d",j,tabChoix1[j].y);
			if(tabChoix1[j].x == -1)
			{
//				printf("\nFin de tab choix 1");
				break;
			}
			if(tabOrigine[i].x == tabChoix1[j].x)
			{
//				printf("\ntabOrigine[i].x == tabChoix1[j].x");
				if(tabOrigine[i].y == tabChoix1[j].y)
				{
//					printf("\ntabOrigine[i].y == tabChoix1[j].y");
					var = 1;
					stop = 1;
					break;
				}
			}
		}
		if(stop==1)
		{
			break;
		}
	}
	return var;
}

/*
Boucle de croisement des valeurs x et y entre origine est choix 2
*/
int Boucle2(int var) // Page 28
{
	int i,j;
	int stop = 0;
	
	for(i=0;i<20;i++)
	{
//		printf("\ntabOrigine[%d].x = %d",i,tabOrigine[i].x);
//		printf("\ntabOrigine[%d].y = %d",i,tabOrigine[i].y);
		
		if(tabOrigine[i].x == -1)
		{
//			printf("\nFin de tab origine");
			break;
		}
		for(j=0;j<20;j++)
		{
//			printf("\ntabChoix2[%d].x = %d",j,tabChoix2[j].x);
//			printf("\ntabChoix2[%d].y = %d",j,tabChoix2[j].y);
			if(tabChoix2[j].x == -1)
			{
//				printf("\nFin de tab choix 1");
				break;
			}
			if(tabOrigine[i].x == tabChoix2[j].x)
			{
//				printf("\ntabOrigine[i].x == tabChoix2[j].x");
				if(tabOrigine[i].y == tabChoix2[j].y)
				{
//					printf("\ntabOrigine[i].y == tabChoix2[j].y");
					var = 1;
					stop = 1;
					break;
				}
			}
		}
		if(stop==1)
		{
			break;
		}
	}
	return var;
}

/*
Boucle de croisement des valeurs x et y entre origine est choix 3
*/
int Boucle3(int var) // Page 30
{
	int i,j;
	int stop = 0;
	for(i=0;i<20;i++)
	{
		if(tabOrigine[i].x == -1)
		{
			break;
		}
		for(j=0;j<20;j++)
		{
			if(tabChoix3[j].x == -1)
			{
				break;
			}
			if(tabOrigine[i].x == tabChoix3[j].x)
			{
				if(tabOrigine[i].y == tabChoix3[j].y)
				{
					var = 1;
					stop = 1;
					break;
				}
			}
		}
		if(stop==1)
		{
			break;
		}
	}
	return var;
}

/*
*	Cette fonction est la machine qui appel les fonction de boucle.
*	Elle gere les appels, est les attributions de nouvelles valeur x et y de chemin, ainsi que la direction
*/
void AmeliorationRecherche(void) // Page 22
{
	int var;
	InitTab(); // Page 27
	var = 0;
	if(carrefour[indiceCarrefour-1].flagchoix1 == 1)
	{
//		printf("\n\nBoucle1");
		var = Boucle1(var); // Page 28
		if(var == 1)
		{
			value.x = carrefour[indiceCarrefour-1].xchoix1;
			value.y = carrefour[indiceCarrefour-1].ychoix1;
			carrefour[indiceCarrefour-1].flagchoix1 = 0;
			chemin[indiceChemin].x=value.x;
			chemin[indiceChemin].y=value.y;
			chemin[indiceChemin].direction=carrefour[indiceCarrefour-1].directionChoix1;
			indiceChemin ++;
		}
		else
		{
			var = 0;
			if(carrefour[indiceCarrefour-1].flagchoix2 == 1)
			{
//				printf("\n\nBoucle2");
				var = Boucle2(var); // Page 29
				if(var == 1)
				{
					value.x = carrefour[indiceCarrefour-1].xchoix2;
					value.y = carrefour[indiceCarrefour-1].ychoix2;
					carrefour[indiceCarrefour-1].flagchoix2 = 0;
					chemin[indiceChemin].x=value.x;
					chemin[indiceChemin].y=value.y;
					chemin[indiceChemin].direction=carrefour[indiceCarrefour-1].directionChoix1;
					indiceChemin ++;
				}
				else
				{
					var = 0;
					if(carrefour[indiceCarrefour-1].flagchoix3 == 1)
					{
//						printf("\n\nBoucle3");
						var = Boucle3(var); // Page 30
						if(var == 1)
						{
							value.x = carrefour[indiceCarrefour-1].xchoix3;
							value.y = carrefour[indiceCarrefour-1].ychoix3;
							carrefour[indiceCarrefour-1].flagchoix3 = 0;
							chemin[indiceChemin].x=value.x;
							chemin[indiceChemin].y=value.y;
							chemin[indiceChemin].direction=carrefour[indiceCarrefour-1].directionChoix1;
							indiceChemin ++;
						}
						else
						{
							printf("\nErreur fonction : AmeliorationRecherche // Page 22\n\t->Pas de correspondance\n");
						}
					}
				}
			}
		}
	}
//	printf("\nChemin[%d].x=%d",indiceChemin-1,chemin[indiceChemin-1].x);
//	printf("\nChemin[%d].y=%d",indiceChemin-1,chemin[indiceChemin-1].y);
//	printf("\nChemin[%d].direction=%d",indiceChemin-1,chemin[indiceChemin-1].direction);
//	printf("\nIndice Chemin = %d",indiceChemin);
}

/*
*	Lorsque les comptes des differents choix sont fait, cette fonction permet de savoir
*	quel compte est le plus petit pour le selectionner, et r�cuperer ces coordonn�es
*/
void DecisionChemin (int compte1, int compte2, int compte3) // Page 31
{
	if(compte1 < compte3 && compte1 < compte2)
	{
		value.x = carrefour[indiceCarrefour].xchoix1;
		value.y = carrefour[indiceCarrefour].ychoix1;
		carrefour[indiceCarrefour].flagchoix1 = 0;
	}
	else if(compte2 < compte3 && compte2 < compte1)
	{
		value.x = carrefour[indiceCarrefour].xchoix2;
		value.y = carrefour[indiceCarrefour].ychoix2;
		carrefour[indiceCarrefour].flagchoix2 = 0;
	}
	else if(compte3 < compte1 && compte3 < compte2)
	{
		value.x = carrefour[indiceCarrefour].xchoix3;
		value.y = carrefour[indiceCarrefour].ychoix3;
		carrefour[indiceCarrefour].flagchoix3 = 0;
	}
	else if(compte1 == compte2 && compte1 < compte3)
	{
		value.x = carrefour[indiceCarrefour].xchoix1;
		value.y = carrefour[indiceCarrefour].ychoix1;
		carrefour[indiceCarrefour].flagchoix1 = 0;
	}
	else if(compte1 == compte3 && compte1 < compte2)
	{
		value.x = carrefour[indiceCarrefour].xchoix1;
		value.y = carrefour[indiceCarrefour].ychoix1;
		carrefour[indiceCarrefour].flagchoix1 = 0;
	}
	else if(compte2 == compte3 && compte2 < compte1)
	{
		value.x = carrefour[indiceCarrefour].xchoix2;
		value.y = carrefour[indiceCarrefour].ychoix2;
		carrefour[indiceCarrefour].flagchoix2 = 0;
	}
	else if(compte1 == compte3 && compte1 == compte3)
	{
		value.x = carrefour[indiceCarrefour].xchoix1;
		value.y = carrefour[indiceCarrefour].ychoix1;
		carrefour[indiceCarrefour].flagchoix1 = 0;
	}
	else
	{
		printf("\nERROR : Decision chemin -> Pas de correspondance\n");
		exit(1);
	}
	chemin[indiceChemin].x = value.x;
	chemin[indiceChemin].y = value.y;
	carrefour[indiceCarrefour].flagchoix3  = 0;
	MemoireDirection(); // Page 36 dans controle.c
	indiceChemin++;
}

/*
*	Cette fonction gere tout les appels de fontion pour faire les comptes, et choisir la bonne coordonn�e.
*/
void RechercheStrategique(void) // Page 12
{
	int compteChoix1 = 0;
	int compteChoix2 = 0;
	int compteChoix3 = 0;
	
	indiceCarrefour--; //Correction erreur. Ajustement avec les indices de la fonction
//	printf("\nindiceCarrefour = %d",indiceCarrefour);
	// Si le flag est � 1, alors il y a un chemin de possible
	if(carrefour[indiceCarrefour].flagchoix1 == 1)
	{
//		printf("\n\nComptabilise Mur flag 1");
		compteChoix1 = ComptabilisationMur(carrefour[indiceCarrefour].xchoix1,carrefour[indiceCarrefour].ychoix1); // Page 13
//		printf("\ncompteChoix1 = %d",compteChoix1);
	}
	if(carrefour[indiceCarrefour].flagchoix2 == 1)
	{
//		printf("\n\nComptabilise Mur flag 2");
		compteChoix2 = ComptabilisationMur(carrefour[indiceCarrefour].xchoix2,carrefour[indiceCarrefour].ychoix2); // Page 13
//		printf("\ncompteChoix2 = %d",compteChoix2);
	}
	if(carrefour[indiceCarrefour].flagchoix3 == 1)
	{
//		printf("\n\nComptabilise Mur flag 3");
		compteChoix3 = ComptabilisationMur(carrefour[indiceCarrefour].xchoix3,carrefour[indiceCarrefour].ychoix3); // Page 13
//		printf("\ncompteChoix3 = %d",compteChoix3);
	}
	/*
	*	Ci dessous, en fonction des r�sultats des comptes, les techniques pour trouver le bon 
	*	chemin seront differentes.
	*/
	// Si au moins un compte est superieur � 0
	if((compteChoix1 > 0) || (compteChoix2 > 0) || (compteChoix3 > 0))
	{
		if(compteChoix1 == 0)
		{
			compteChoix1=101;
		}
		if(compteChoix2 == 0)
		{
			compteChoix2=102;
		}
		if(compteChoix3 == 0)
		{
			compteChoix3=103;
		}
//		printf("\nAu moins un compte > 0");
		// Tout les comptes sont differents
		if((compteChoix1 != compteChoix2) && (compteChoix1 != compteChoix3) && (compteChoix2 != compteChoix3))
		{
//			printf("\nTout les comptes sont differents");
			// Recherche le compte le plus petit
			if((compteChoix1 < compteChoix2) && (compteChoix1 < compteChoix3))
			{
//				printf("\nCompte choix 1 le plus petit");
				value.x = carrefour[indiceCarrefour].xchoix1;
				value.y = carrefour[indiceCarrefour].ychoix1;
				chemin[indiceChemin].x = value.x;
				chemin[indiceChemin].y = value.y;
				carrefour[indiceCarrefour].flagchoix1 = 0;
				MemoireDirection(); // Page 36 dans controle.c
				indiceChemin++;
			}
			if((compteChoix2 < compteChoix1) && (compteChoix2 < compteChoix3))
			{
//				printf("\nCompte choix 2 le plus petit");
				value.x = carrefour[indiceCarrefour].xchoix2;
				value.y = carrefour[indiceCarrefour].ychoix2;
				chemin[indiceChemin].x = value.x;
				chemin[indiceChemin].y = value.y;
				carrefour[indiceCarrefour].flagchoix2 = 0;
				MemoireDirection(); // Page 36 dans controle.c
				indiceChemin++;
			}
			if((compteChoix3 < compteChoix2) && (compteChoix3 < compteChoix1))
			{
//				printf("\nCompte choix 3 le plus petit");
				value.x = carrefour[indiceCarrefour].xchoix3;
				value.y = carrefour[indiceCarrefour].ychoix3;
				chemin[indiceChemin].x = value.x;
				chemin[indiceChemin].y = value.y;
				carrefour[indiceCarrefour].flagchoix3  = 0;
				MemoireDirection(); // Page 36 dans controle.c
				indiceChemin++;
			}
		}
		// Si des comptes sont �gaux
		else
		{
//			printf("\n\nDecisionChemin");
			DecisionChemin(compteChoix1,compteChoix2,compteChoix3);
		}
	}
	else
	{
//		printf("\n\nAmeliorationRecherche");
		indiceCarrefour ++; // Correction erreur
		AmeliorationRecherche();
		indiceCarrefour --; // Correction erreur
	}
	indiceCarrefour ++; // Correction erreur
}

/*
*	Cette fonction enregistre les choix d'un carrefour.
*/
void ChoixChemin(int x, int y) // Page 11
{
//	printf("\n\nMemoire Carrefour");
	MemoireCarrefour(); // rutilisation fonction Memoire carrefour p.35 dans le programme "controle.c"
//	printf("\nindiceCarrefour = %d",indiceCarrefour);
//	printf("\nxChoix1 = %d",carrefour[indiceCarrefour-1].xchoix1);
//	printf("\nyChoix1 = %d",carrefour[indiceCarrefour-1].ychoix1);
//	printf("\nflagchoix1 = %d",carrefour[indiceCarrefour-1].flagchoix1);
//	printf("\ndirectionChoix1 = %d",carrefour[indiceCarrefour-1].directionChoix1);
//	printf("\nxChoix2 = %d",carrefour[indiceCarrefour-1].xchoix2);
//	printf("\nyChoix2 = %d",carrefour[indiceCarrefour-1].ychoix2);
//	printf("\nflagchoix2 = %d",carrefour[indiceCarrefour-1].flagchoix2);
//	printf("\ndirectionChoix2 = %d",carrefour[indiceCarrefour-1].directionChoix2);
//	printf("\nxChoix3 = %d",carrefour[indiceCarrefour-1].xchoix3);
//	printf("\nyChoix3 = %d",carrefour[indiceCarrefour-1].ychoix3);
//	printf("\nflagchoix3 = %d",carrefour[indiceCarrefour-1].flagchoix3);
//	printf("\ndirectionChoix3 = %d",carrefour[indiceCarrefour-1].directionChoix3);
	
/*	printf("\nChemin[%d].x=%d",indiceChemin-1,chemin[indiceChemin-1].x);
	printf("\nChemin[%d].y=%d",indiceChemin-1,chemin[indiceChemin-1].y);
	printf("\nChemin[%d].direction=%d",indiceChemin-1,chemin[indiceChemin-1].direction);
	printf("\nIndice Chemin = %d",indiceChemin);
	
	printf("\n\nCorrection Memoire");
*/	CorrectionMemoire(); // Une correction s'impose, car MemoireCarrefour d�cide d'un chemin au hasard. Alors qu'il faut suivre un certain processus. Cette fonction annule le choix fait par MemoireCarrefour.
/*	printf("\nChemin[%d].x=%d",indiceChemin-1,chemin[indiceChemin-1].x);
	printf("\nChemin[%d].y=%d",indiceChemin-1,chemin[indiceChemin-1].y);
	printf("\nChemin[%d].direction=%d",indiceChemin-1,chemin[indiceChemin-1].direction);
	printf("\nIndice Chemin = %d",indiceChemin);
	printf("\n\nRecherche Strategique");
*/	// Cette fonction recherche le bon chemin suite � la rencontre d'un carrefour
	RechercheStrategique(); // Page 12
}

/*
*	Vide Chemin pour une nouvelle recherche.
*/
void VideMemoirChemin(void) // Page 33
{
	int i;
	
	for(i=0;i<indiceChemin;i++)
	{
		chemin[i].x = -1;
		chemin[i].y = -1;
		chemin[i].direction = -1;
	}
	
	indiceChemin = 0;
}

/*
*	Cette fonction distingue d'un simple chemin, d'un carrefour.
*/
int RechercheNbChoix(int x, int y) // Page 10
{
	int nbChoix;
	value.x = chemin[indiceChemin-1].x;
	value.y = chemin[indiceChemin-1].y;
//	printf("\nvalue.x = %d\nvalue.y = %d",value.x,value.y);
	nbChoix = ControleNbChoixPossibleZoneInte();// Rutilisation de la fonction nb choix possible P.23 Dans le programme "controle.c" ) 
//	printf("\nNombre de choix = %d",nbChoix);
	return nbChoix;
}

/*
*	Une fois la zone trouv�, cette fonction la tamponne d'un certain numeros
*	Ces informations sont implant� dans le tableau de structure datazone.
*/
void TamponneZone(void) // Page 32
{
	int i;
	
	for(i=0;i<indiceChemin-1;i++)
	{
		
		if((plateau[chemin[i].y][chemin[i].x].zone1) == -1)
		{
			plateau[chemin[i].y][chemin[i].x].zone1 = zone;
		}
		else if((plateau[chemin[i].y][chemin[i].x].zone2) == -1)
		{
			plateau[chemin[i].y][chemin[i].x].zone2 = zone;
		}
		else
		{
			printf("\nErreur : TamponneZone(void) // Page 32\n\tMur a deja 2 zone \n");
		}
		
		datazone[zone].coordzone[i][0] = chemin[i].x;//x y
		datazone[zone].coordzone[i][1] = chemin[i].y;//x y
		printf("\n\nZone %d chemin[%d].x=%d",zone,i,chemin[i].x);
		printf("\nZone %d chemin[%d].y=%d",zone,i,chemin[i].y);
		printf("\nmur zone1 = %d",plateau[chemin[i].y][chemin[i].x].zone1);
		printf("\nmur zone2 = %d",plateau[chemin[i].y][chemin[i].x].zone2);
	}
	printf("\n\n");
	datazone[zone].numZone = zone;
	zone ++;
}

/*
*	Ajustement d'une fonction deja existante, pour l'adapter � la recherche interieur de zone.
*	Cette fonction compte le nombre de choix possible � gauche
*/
int CompteNbChoixPossGaucheZoneInte(void) // verifie si simple mur ou si carrefour, sur un mur horizontal
{
	int var;
	var = 0;
	
	if(plateau[value.y-1][value.x-1].mur == 1 && (value.y-1>=0 && value.x-1>= 0)) // Mur pr�sent en haut � gauche?
	{
//		printf("\nvalue.y - 1 modulo 2 = %d",(value.y-1)%2);
		if((value.x-1 == 0 || value.x-1 == 16) && ((value.y-1)%2 == 1)) // si mur bordure
		{
//			printf("\nmur borudre");
			if(plateau[value.y-1][value.x-1].zone1 == -1)
			{
//				printf("\nMur non affect�");
				var ++;
			}			
		}
		else if((value.y-1 == 0 || value.y-1 == 10)&& ((value.x-1)%2 == 1)) // si mur bordure
		{
//			printf("\nmur bordure");
			if(plateau[value.y-1][value.x-1].zone1 == -1)
			{
				var ++;
			}	
		}
		//si mur dans le plateau
		else if(	(plateau[value.y-1][value.x-1].zone1) == -1 || (plateau[value.y-1][value.x-1].zone2 == -1)	)
		{
//			printf("\nmur plateau");
			var ++;
		}
	}
//	printf("\nvar = %d",var);
	if(plateau[value.y][value.x-2].mur == 1&& (value.x-2>= 0)) // Mur pr�sent � l'horizontal � gauche?
	{
		if((value.x-2 == 0 || value.x-2 == 16) && (value.y%2 == 1))
		{
			if(plateau[value.y][value.x-2].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y == 0 || value.y == 10)&& ((value.x-2)%2 == 1))
		{
			if(plateau[value.y][value.x-2].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y][value.x-2].zone1) == -1 || (plateau[value.y][value.x-2].zone2 == -1)	)
		{
			var ++;
		}
	}
//	printf("\nvar = %d",var);
	if(plateau[value.y+1][value.x-1].mur == 1&& (value.y+1<=10 && value.x-1>= 0)) // Mur pr�sent en bas � gauche?
	{
//		printf("\nICI");
		if((value.x-1 == 0 || value.x-1 == 16) && ((value.y+1)%2 == 1))
		{
//			printf("\nmur bordure x");
			if(plateau[value.y+1][value.x-1].zone1 == -1)
			{
//				printf("\nvar++");
				var ++;
			}			
		}
		else if((value.y+1 == 0 || value.y+1 == 10)&& ((value.x-1)%2 == 1))
		{
//			printf("\nmur bordure y");
			if(plateau[value.y+1][value.x-1].zone1 == -1)
			{
//				printf("\nvar++");
				var ++;
			}	
		}
		else if(	(plateau[value.y+1][value.x-1].zone1) == -1 || (plateau[value.y+1][value.x-1].zone2 == -1)	)
		{
			var ++;
		}
	}
//	printf("\nvar = %d",var);
	return var;
}

/*
*	Ajustement d'une fonction deja existante, pour l'adapter � la recherche interieur de zone.
*	Cette fonction compte le nombre de choix possible � droite
*/
// Page 20
int CompteNbChoixPossDroiteZoneInte(void) // verifie si simple mur ou si carrefour, sur un mur horizontal
{
	int var;
	var = 0;
	
	if(plateau[value.y-1][value.x+1].mur == 1&& (value.y-1>=0 && value.x+1<= 16)) // Mur pr�sent en haut � droite?
	{
		if((value.x+1 == 0 || value.x+1 == 16) && ((value.y-1)%2 == 1))
		{
			if(plateau[value.y-1][value.x+1].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y-1 == 0 || value.y-1 == 10)&& ((value.x+1)%2 == 1))
		{
			if(plateau[value.y-1][value.x+1].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y-1][value.x+1].zone1) == -1 || (plateau[value.y-1][value.x+1].zone2 == -1)	)
		{
			var ++;
		}
	}
	if(plateau[value.y][value.x+2].mur == 1&& (value.x+2<= 16)) // Mur pr�sent � l'horizontal � droite?
	{
		if((value.x+2 == 0 || value.x+2 == 16) && (value.y%2 == 1))
		{
			if(plateau[value.y][value.x+2].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y == 0 || value.y == 10)&& ((value.x+2)%2 == 1))
		{
			if(plateau[value.y][value.x+2].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y][value.x+2].zone1) == -1 || (plateau[value.y][value.x+2].zone2 == -1)	)
		{
			var ++;
		}
	}
	if(plateau[value.y+1][value.x+1].mur == 1&& (value.y+1<=10 && value.x+1<= 16)) // Mur pr�sent en bas � droite?
	{
		if((value.x+1 == 0 || value.x+1 == 16) && ((value.y+1)%2 == 1))
		{
			if(plateau[value.y+1][value.x+1].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y+1 == 0 || value.y+1 == 10)&& ((value.x+1)%2 == 1))
		{
			if(plateau[value.y+1][value.x+1].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y+1][value.x+1].zone1) == -1 || (plateau[value.y+1][value.x+1].zone2 == -1)	)
		{
			var ++;
		}
	}
	return var;
}

/*
*	Ajustement d'une fonction deja existante, pour l'adapter � la recherche interieur de zone.
*	Cette fonction compte le nombre de choix possible en bas
*/
// Page 21
int CompteNbChoixPossBasZoneInte(void) // verifie si simple mur ou si carrefour, sur un mur vertical
{
	int var;
	var = 0;
	
	if(plateau[value.y+1][value.x-1].mur == 1&& (value.y+1<=10 && value.x-1>= 0)) // Mur pr�sent � gauche en bas?
	{
		if((value.x-1 == 0 || value.x-1 == 16) && (value.y+1%2 == 1))
		{
			if(plateau[value.y+1][value.x-1].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y+1 == 0 || value.y+1 == 10)&& (value.x-1%2 == 1))
		{
			if(plateau[value.y+1][value.x-1].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y+1][value.x-1].zone1) == -1 || (plateau[value.y+1][value.x-1].zone2 == -1)	)
		{
			var ++;
		}
	}
	if(plateau[value.y+2][value.x].mur == 1&& (value.x+2<= 16)) // Mur pr�sent � la vertical en bas?
	{
		if((value.x == 0 || value.x == 16) && (value.y+2%2 == 1))
		{
			if(plateau[value.y+2][value.x].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y+2 == 0 || value.y+2 == 10)&& (value.x%2 == 1))
		{
			if(plateau[value.y+2][value.x].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y+2][value.x].zone1) == -1 || (plateau[value.y+2][value.x].zone2 == -1)	)
		{
			var ++;
		}
	}
	if(plateau[value.y+1][value.x+1].mur == 1&& (value.y+1<=10 && value.x+1<= 16)) // Mur pr�sent � droite en bas?
	{
		if((value.x+1 == 0 || value.x+1 == 16) && (value.y+1%2 == 1))
		{
			if(plateau[value.y+1][value.x+1].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y+1 == 0 || value.y+1 == 10)&& (value.x+1%2 == 1))
		{
			if(plateau[value.y+1][value.x+1].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y+1][value.x+1].zone1) == -1 || (plateau[value.y+1][value.x+1].zone2 == -1)	)
		{
			var ++;
		}
	}
	return var;
}


/*
*	Ajustement d'une fonction deja existante, pour l'adapter � la recherche interieur de zone.
*	Cette fonction compte le nombre de choix possible en haut
*/
// Page 22
int CompteNbChoixPossHautZoneInte(void) // verifie si simple mur ou si carrefour, sur un mur vertical
{
	int var;
	var = 0;
	
	if(plateau[value.y-1][value.x-1].mur == 1&& (value.y-1>=0 && value.x-1>= 0)) // Mur pr�sent � gauche en haut?
	{
		if((value.x-1 == 0 || value.x-1 == 16) && (value.y-1%2 == 1))
		{
			if(plateau[value.y-1][value.x-1].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y-1 == 0 || value.y-1 == 10)&& (value.x-1%2 == 1))
		{
			if(plateau[value.y-1][value.x-1].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y-1][value.x-1].zone1) == -1 || (plateau[value.y-1][value.x-1].zone2 == -1)	)
		{
			var ++;
		}
	}
	if(plateau[value.y-2][value.x].mur == 1&& (value.y-2>=0)) // Mur pr�sent � la vertical en haut?
	{
		if((value.x == 0 || value.x == 16) && (value.y-2%2 == 1))
		{
			if(plateau[value.y-2][value.x].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y-2 == 0 || value.y-2 == 10)&& (value.x%2 == 1))
		{
			if(plateau[value.y-2][value.x].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y-2][value.x].zone1) == -1 || (plateau[value.y-2][value.x].zone2 == -1)	)
		{
			var ++;
		}
	}
	if(plateau[value.y-1][value.x+1].mur == 1&& (value.y-1>=0 && value.x+1<= 16)) // Mur pr�sent � droite en haut?
	{
		if((value.x+1 == 0 || value.x+1 == 16) && (value.y-1%2 == 1))
		{
			if(plateau[value.y-1][value.x+1].zone1 == -1)
			{
				var ++;
			}			
		}
		else if((value.y-1 == 0 || value.y-1 == 10)&& (value.x+1%2 == 1))
		{
			if(plateau[value.y-1][value.x+1].zone1 == -1)
			{
				var ++;
			}	
		}
		else if(	(plateau[value.y-1][value.x+1].zone1) == -1 || (plateau[value.y-1][value.x+1].zone2 == -1)	)
		{
			var ++;
		}
	}
	return var;
}

/*
*	Distingue si les coordonn�es �tudi�es correspondes � un mur horizontal ou vertical
*	Puis, compte le nombre de choix possible pour distingu� un simple chemin d'un carrefour
*/
int ControleNbChoixPossibleZoneInte(void)
{
	int var;
	if((value.x%2 == 1) && (value.y%2 == 0)) // Si mur horizontal
	{
		if(chemin[indiceChemin-1].direction == 4) // Direction gauche
		{
			var = CompteNbChoixPossGaucheZoneInte(); // Page 19
		}
		else // Direction droite
		{
			var = CompteNbChoixPossDroiteZoneInte(); // Page 20
		}
	}
	else // Mur vertical
	{
		if(chemin[indiceChemin-1].direction == 3) // Direction bas
		{
			var = CompteNbChoixPossBasZoneInte(); // Page 21
		}
		else // Direction droite
		{
			var = CompteNbChoixPossHautZoneInte(); // Page 22
		}
	}
	return var;
}

/*
*	Cette fonction appel toute les fonctions pour recherch� une zone.
*	C'est une boucle qui ne s'arrete que quand elle trouve un zone ou qu'un chemin ne mene � rien.
*/
void RechercheZone(int x,int y) // Page 9
{
	int sortie = 0;
	int val;
	int var = 1;
	int i;
	
	while(sortie == 0)
	{
		if((value.x == chemin[0].x) && (value.y == chemin[0].y))
		{
			printf("\n\nTamponne Zone");
			TamponneZone(); // Page 32
			InitMemoireChemin();
			InitMemoireCarrefour();	
			sortie = 1;
		}
		else
		{
//			printf("\n\nRechercheNbChoix");
			val = RechercheNbChoix(value.x,value.y); // Page 10
			if(val > 1)
			{
//				printf("\n\nChoixChemin");
				ChoixChemin(value.x,value.y); // Page 11
			}
			else
			{
				if(val == 1)
				{
					//indiceChemin --; // Pour adapt avec la fonction de controle.c
//					printf("\n\nRecherche Coord Choix");
					var = RechercheCoordChoixZoneInte(); //Rutilisation de la fonction RechercheCoordChoix P.26 Dans le programme "controle.c" )
//					printf("\nNouvelle coord -> %d",var);
//					printf("\nvalue.x = %d\nvalue.y = %d",value.x,value.y);
					chemin[indiceChemin].x = value.x;
					chemin[indiceChemin].y = value.y;
					MemoireDirection(); // Page 36 dans controle.c
					indiceChemin ++;				
				}
				else
				{
//					printf("\n\nRecherche Memoire");
					if(!RechercheMemoire())// rutilisation de le fonciton recherche memoire P.31 dans le programme "controle.c" )
					{
						for(i=indiceMemo;i<indiceChemin;i++)
						{
							chemin[i].x = -1;
							chemin[i].y = -1;
							chemin[i].direction = -1;
						}
						indiceChemin = indiceMemo;
//						printf("\nnouvelle valeur de indiceChemin = %d",indiceChemin);
						sortie = 1;
					}
				}
			  /*if(var == 0)
				{
					if(indiceCarrefour == 0)
					{
						printf("\n\nVide Memoire Chemin");
						VideMemoirChemin(); // Page 33
						sortie = 1;	
					}
					else
					{
						//Recherche Memoire
					}
				}*/
			}
		}
/*		printf("\nChemin[%d].x=%d",indiceChemin-1,chemin[indiceChemin-1].x);
		printf("\nChemin[%d].y=%d",indiceChemin-1,chemin[indiceChemin-1].y);
		printf("\nChemin[%d].direction=%d",indiceChemin-1,chemin[indiceChemin-1].direction);
		printf("\nIndice Chemin = %d",indiceChemin);
*/	}
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 4
//-------------------------------------------------------------------------------------------------------------

/*
*	La premiere des techniques consistes par commencer � faire le tour des bordures, et de chercher
*	une zone lors d'une rencontre de carrefour.
*	Cette fontion controle si tout les murs de bordure sont associ� � une zone.
*	Si oui, la suite sera une recherche interieur de zone.
*/
_Bool RechercheSiResteMurBordure(void)// Page 8
{
	int x,y;
	_Bool var = false;
	
	for(y=0;y<11;y++)
	{
		for(x=0;x<17;x++)
		{
			if((x == 0 || x == 16) && (y%2 == 1))
			{
				if(plateau[y][x].zone1 == -1)
				{
					var = true;		
				}		
			}
			else if((y == 0 || y == 10)&& (x%2 == 1))
			{
				if(plateau[y][x].zone1 == -1)
				{
					var = true;		
				}	
			}	
		}
	}
	
	return var;
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 5
//-------------------------------------------------------------------------------------------------------------

/*
*	initialisation du tableau de structure des donn�es de zone trouv�.
*/
void InitDonneesZone() // Page 39 // Non utile
{
/*	int i;
	
	for(i=0;i<indiceChemin;i++)
	{
		datazone[zone].coordzone[i][0] = chemin[i].x; //x y
		datazone[zone].coordzone[i][1] = chemin[i].y;
		printf("\ndatazone[%d].coordzone[%d][0]=%d",zone,i,chemin[i].x);
		printf("\ndatazone[%d].coordzone[%d][1]=%d",zone,i,chemin[i].y);
	}*/
}

/*
*	Cette fonction se sert des coordonn�es des zones, distingue si la case rencontr� est dans la zone,
*	prends en consideration les mines rencontr� dans la zone, r�cupere la valeur des jetons.
*	Elle gere donc toutes les informations dans une zone. 
*/
void MinMaxMineZone(int nbzone) // Page 40
{
	int i,x,y;
	int xMin, xMax, yMin, yMax;
//	printf("\nnb zone = %d",nbzone);
	xMin = datazone[nbzone].coordzone[0][0];
	xMax = xMin;
	yMin = datazone[nbzone].coordzone[0][1];
	yMax = yMin;
	
	for(i=0;i<50;i++)
	{
		if(datazone[nbzone].coordzone[i][0] == -1)
		{
			break;
		}
		
		if(datazone[nbzone].coordzone[i][0] > xMax)
		{
			xMax = datazone[nbzone].coordzone[i][0];
		}
		if(datazone[nbzone].coordzone[i][0] < xMin)
		{
			xMin = datazone[nbzone].coordzone[i][0];
		}
		
		if(datazone[nbzone].coordzone[i][1] > yMax)
		{
			yMax = datazone[nbzone].coordzone[i][1];
		}
		if(datazone[nbzone].coordzone[i][1] < yMin)
		{
			yMin = datazone[nbzone].coordzone[i][1];
		}
	}
//	printf("\nxMax = %d",xMax);
//	printf("\nxMin = %d",xMin);
//	printf("\nyMax = %d",yMax);
//	printf("\nyMin = %d",yMin);
	int var1;
	int var2;
	
	/*
	*	Technique : 
	*	Ici, la fonction a les informations suivantes;
	*		-le min max des coordonn�es x et y de la zone
	*	Cette fonction ne traite que les murs verticals.
	*	De la sorte, la fonction connait la parit� des murs pr�sents � gauche et � droite.
	*	Si la case �tudi� � des murs de parit�s impaires, alors elle est dans la zone.
	*	Si la case etidi� � des murs de parit�s paires, alors elle est en dehors de la zone.
	*	Idem si l'un des comptes est egale � 0, elle est considere comme en dehors de la zone.
	*/
	for(y=yMin;y<=yMax;y++)
	{		
		for(x=xMin;x<xMax;x++)
		{
//			printf("\nx : %d\ny : %d",x,y);
			if((x%2 == 0) && (y%2 == 1)) // Si mur vertical
			{
//				printf("\n\nCompteDroite");
				var1 = CompteDroite(x,xMax,y,nbzone); // Page 43
//				printf("\nvar1 = %d",var1);
//				printf("\n\nCompteGauche");
				var2 = CompteGauche(x,xMin,y,nbzone); // Page 44
	//			printf("\nvar2 = %d",var2);
			}
				
			if((var1%2 == 1) && (var2%2 == 1))
			{	
/*				printf("\nOn est dans la zone");
				printf("\nx = %d",x);
				printf("\ny = %d",y);
*/
				if(plateau[y][x].mine == 1)
				{
		//			printf("\nMine PRESENTE");
					datazone[nbzone].mine = 1;
					datazone[nbzone].orZone += plateau[y][x].orMine;
	//				printf("\nvaleur du tresor = %d",datazone[nbzone].orZone);
				}
			
				if(plateau[y][x].joueur == 1)
				{
   //				printf("\nPresence heros 1");
//					printf("\nvaleur du jeton = %d",plateau[y][x].valeurJeton);
					datazone[nbzone].joueur1 += plateau[y][x].valeurJeton;
				}
				if(plateau[y][x].joueur == 2)
				{
	//				printf("\nPresence heros 2");
//					printf("\nvaleur du jeton = %d",plateau[y][x].valeurJeton);
					datazone[nbzone].joueur2 += plateau[y][x].valeurJeton;
				}
				if(plateau[y][x].joueur == 3)
				{
	//				printf("\nPresence heros 3");
//					printf("\nvaleur du jeton = %d",plateau[y][x].valeurJeton);
					datazone[nbzone].joueur3 += plateau[y][x].valeurJeton;
				}
				if(plateau[y][x].joueur == 4)
				{
	//				printf("\nPresence heros 4");
//					printf("\nvaleur du jeton = %d",plateau[y][x].valeurJeton);
					datazone[nbzone].joueur4 += plateau[y][x].valeurJeton;
				}
			}	
		}
			
	}
}

/*
*	compte le nombre de mur pr�sent � droite, pour savoir si la case �tudi� est en dehors de la zone �tudi�
*/
int CompteDroite(int x,int Max,int y,int nbzone) // Page 43
{
	int i;
	int var = 0;
	
	for(i=x+1;i<=Max;i++)
	{
		if((plateau[y][i].mur == 1) && (plateau[y][i].zone1 == nbzone))
		{
			var++;
		}
		else if((plateau[y][i].mur == 1) && (plateau[y][i].zone2 == nbzone))
		{
			var++;
		}
	}
	return var;
}

/*
*	compte le nombre de mur pr�sent � gauche, pour savoir si la case �tudi� est en dehors de la zone �tudi�
*/
int CompteGauche(int x,int Min,int y,int nbzone) // Page 44
{
	int i;
	int var = 0;
//	printf("\nMin : %d",Min);
//	printf("\nx = %d",x);
//	printf("\ny = %d",y);
	
	for(i=Min;i<=x;i++)
	{
	//	printf("\nBoucle : %d",i);
		if((plateau[y][i].mur == 1) && (plateau[y][i].zone1 == nbzone))
		{
			var++;
		}
		else if((plateau[y][i].mur == 1) && (plateau[y][i].zone2 == nbzone))
		{
			var++;
		}
	}
	
	return var;
}

/*
*	Cette fonction d�termine le vainqueur de chaque zone
*/
void VainqueurZone(int nbzone) // Page 41
{
	if(nombreDeJoueur == 4)
	{
		if(joueur[1].equipe == 1)
		{
			datazone[nbzone].equipe1 += datazone[nbzone].joueur1;
		}
		if(joueur[1].equipe == 2)
		{
			datazone[nbzone].equipe2 += datazone[nbzone].joueur1;
		}
		if(joueur[2].equipe == 1)
		{
			datazone[nbzone].equipe1 += datazone[nbzone].joueur2;
		}
		if(joueur[2].equipe == 2)
		{
			datazone[nbzone].equipe2 += datazone[nbzone].joueur2;
		}
		if(joueur[3].equipe == 1)
		{
			datazone[nbzone].equipe1 += datazone[nbzone].joueur3;
		}
		if(joueur[3].equipe == 2)
		{
			datazone[nbzone].equipe2 += datazone[nbzone].joueur3;
		}
		if(joueur[4].equipe == 1)
		{
			datazone[nbzone].equipe1 += datazone[nbzone].joueur4;
		}
		if(joueur[4].equipe == 2)
		{
			datazone[nbzone].equipe2 += datazone[nbzone].joueur4;
		}
		
		if(datazone[nbzone].equipe1 > datazone[nbzone].equipe2)
		{
			datazone[nbzone].vainqueur = 1;
		}
		else
		{
			datazone[nbzone].vainqueur = 2;
		}
	}
	else
	{
		if(datazone[nbzone].joueur1 > datazone[nbzone].joueur2)
		{
			if(datazone[nbzone].joueur1 > datazone[nbzone].joueur3)
			{
				datazone[nbzone].vainqueur = 1;
			}
		}
		if(datazone[nbzone].joueur2 > datazone[nbzone].joueur1)
		{
			if(datazone[nbzone].joueur2 > datazone[nbzone].joueur3)
			{
				datazone[nbzone].vainqueur = 2;
			}
		}
		if(nombreDeJoueur == 3)
		{
			if(datazone[nbzone].joueur3 > datazone[nbzone].joueur1)
			{
				if(datazone[nbzone].joueur3 > datazone[nbzone].joueur2)
				{
					datazone[nbzone].vainqueur = 3;
				}
			}
		}
	}
	printf("\nVainqueur zone %d : %d",nbzone,datazone[nbzone].vainqueur);
	fflush(stdout);
}


/*
*	Cette fonction sert � trouver les zones, et � trouver le vainqueur de chacune d'elle.
*	Si pas de mine d�t�cter dans la zone, alors elle n'appelle pas la fonction qui d�termine le vainqueur.
*/
void ComptabilisePoints(void) // Page 38 
{
	int i;
	
	for(i=1;i<zone;i++)
	{
		MinMaxMineZone(i); // Page 40
		
		if(datazone[i].mine == 1)
		{
			VainqueurZone(i); // Page 41
		}
	}
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 6
//-------------------------------------------------------------------------------------------------------------

/*
*	Lorsque la m�morisation d'un carrefour se fait, si les murs son deja pris par 2 zones, elle ne le prend pas en concideration.
*	Cette fonction controle que les murs enregistr�s sont bien tous disponibles pour une nouvelles zones.
*/
void ControleMurImpossible(void) // Page 37
{
	int x,y;
	if(carrefour[indiceCarrefour-1].flagchoix1 == 1)
	{
		x = carrefour[indiceCarrefour-1].xchoix1;
		y = carrefour[indiceCarrefour-1].ychoix1;
		
		if((plateau[y][x].zone1 != -1) && (plateau[y][x].zone2 != -1))
		{
			carrefour[indiceCarrefour-1].flagchoix1 = 0;
		}
	}
	if(carrefour[indiceCarrefour].flagchoix2 == 1)
	{
		x = carrefour[indiceCarrefour-1].xchoix2;
		y = carrefour[indiceCarrefour-1].ychoix2;
		
		if((plateau[y][x].zone1 != -1) && (plateau[y][x].zone2 != -1))
		{
			carrefour[indiceCarrefour-1].flagchoix2 = 0;
		}
	}
	if(carrefour[indiceCarrefour-1].flagchoix3 == 1)
	{
		x = carrefour[indiceCarrefour-1].xchoix3;
		y = carrefour[indiceCarrefour-1].ychoix3;
		
		if((plateau[y][x].zone1 != -1) && (plateau[y][x].zone2 != -1))
		{
			carrefour[indiceCarrefour-1].flagchoix3 = 0;
		}
	}
}

/*
*	Cette fonction est une version adapt� pour la recherche interieur.
*	Il y a Controle Mur Impossible qui est rajouter par rapport � ChoixChemin.
*	La necessit� de controler si un mur est deja pris ou non par 2 zones doit etre effectu�.
*/
void ChoixCheminInte(void) // Page 36
{
//	printf("\n\nMemoireCarrefour");
	MemoireCarrefour(); //rutilisation fonction Memoire carrefour p.35 dans le programme "controle.c" )
	CorrectionMemoire();
//	printf("\n\nControleMurImpossible");
	ControleMurImpossible();// Page 37
//	printf("\n\nRechercheStrategique");
	RechercheStrategique(); // Page 12
}

/*
*	Recherche de nouvelle coordonn� � gauche
*/
int RechercheCoordGaucheZoneInte(void) // Pour un mur horizontal
{
	if(	 plateau[value.y - 1][value.x - 1].mur == 1&& (value.y-1>=0 && value.x-1>= 0)) // Si mur pr�sent en haut � gauche
	{
//		printf("\nmur haut gauche");
		if((value.x-1 == 0 || value.x-1 == 16) && ((value.y-1)%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y - 1][value.x - 1].zone1 == -1)
			{
				value.y -= 1;
				value.x -= 1;
				return 1;
			}
//			printf("\nmur bordure deja pris");			
		}
		else if((value.y-1 == 0 || value.y-1 == 10)&& ((value.x-1)%2 == 1))// Si mur bordure
		{
			if(plateau[value.y - 1][value.x - 1].zone1 == -1)
			{
				value.y -= 1;
				value.x -= 1;
				return 1;
			}
//			printf("\nmur bordure deja pris");	
		}
		
		else if(plateau[value.y - 1][value.x - 1].zone1 == -1 || plateau[value.y - 1][value.x - 1].zone2 == -1)
		{
//			printf("\nmur zone non affect�");
			value.y -= 1;
			value.x -= 1;
			return 1;		
		}
	}
	
	if( plateau[value.y + 1][value.x - 1].mur == 1&& (value.y+1<=10 && value.x-1>= 0)) // Si mur pr�sent en bas � gauche
	{
//		printf("\nmur bas gauche");
		if((value.x-1 == 0 || value.x-1 == 16) && ((value.y+1)%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y + 1][value.x - 1].zone1 == -1)
			{
				value.y += 1;
				value.x -= 1;
				return 1;
			}			
		}
		else if((value.y+1 == 0 || value.y+1 == 10)&& ((value.x-1)%2 == 1))// Si mur bordure
		{
			if(plateau[value.y + 1][value.x - 1].zone1 == -1)
			{
				value.y += 1;
				value.x -= 1;
				return 1;
			}	
		}
		else if	(plateau[value.y + 1][value.x - 1].zone1 == -1 || plateau[value.y + 1][value.x - 1].zone2 == -1) // Mur disponible pour un nouvelle zone
		{
			value.y += 1;
			value.x -= 1;
			return 1;
		}
	}
	
	if(	plateau[value.y][value.x - 2].mur == 1 && (value.x-2>= 0))// Si mur horizontal � gauche
	{
//		printf("\nmur gauche");
		if((value.x-2 == 0 || value.x-2 == 16) && ((value.y)%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y][value.x - 2].zone1 == -1)
			{
				value.x -= 2;
				return 1;
			}			
		}
		else if((value.y == 0 || value.y == 10)&& ((value.x-2)%2 == 1))// Si mur bordure
		{
			if(plateau[value.y][value.x - 2].zone1 == -1)
			{
				value.x -= 2;
				return 1;
			}	
		}
		else if(plateau[value.y][value.x - 2].zone1 == -1 || plateau[value.y][value.x - 2].zone2 == -1)
		{
			value.x -= 2;
			return 1;	
		}
	}
	
	return 0;
}

/*
*	Recherche de nouvelle coordonn� � droite
*/
// Page 28
int RechercheCoordDroitZoneInte(void) // Pour un mur horizontal
{
	if(	plateau[value.y - 1][value.x + 1].mur == 1&& (value.y-1>=0 && value.x+1 <= 16)) // Si mur pr�sent en haut � droite
	{
		if((value.x+1 == 0 || value.x+1== 16) && ((value.y-1)%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y - 1][value.x + 1].zone1 == -1)
			{
				value.y -= 1;
				value.x += 1;
				return 1;
			}			
		}
		else if((value.y-1 == 0 || value.y-1 == 10)&& ((value.x+1)%2 == 1))// Si mur bordure
		{
			if(plateau[value.y - 1][value.x + 1].zone1 == -1)
			{
				value.y -= 1;
				value.x += 1;
				return 1;
			}	
		}
		
		else if(plateau[value.y - 1][value.x + 1].zone1 == -1 || plateau[value.y - 1][value.x + 1].zone2 == -1)
		{
			value.y -= 1;
			value.x += 1;
			return 1;
		}
	}
	
	if(	plateau[value.y + 1][value.x + 1].mur == 1&& (value.y+1<=10 && value.x+1 <= 16)) // Si mur pr�sent en bas � droite
	{
		if((value.x+1 == 0 || value.x+1 == 16) && ((value.y+1)%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y + 1][value.x + 1].zone1 == -1)
			{
				value.y += 1;
				value.x += 1;
				return 1;
			}			
		}
		else if((value.y+1 == 0 || value.y+1 == 10)&& ((value.x+1)%2 == 1))// Si mur bordure
		{
			if(plateau[value.y + 1][value.x + 1].zone1 == -1)
			{
				value.y += 1;
				value.x += 1;
				return 1;
			}	
		}
		
		else if(plateau[value.y + 1][value.x + 1].zone1 == -1 || plateau[value.y + 1][value.x + 1].zone2 == -1) 
		{
			value.y += 1;
			value.x += 1;
			return 1;	
		}
	}
	
	if(plateau[value.y][value.x + 2].mur == 1&& (value.x+2<= 16)) // Si mur horizontal � droite
	{
		if((value.x+2 == 0 || value.x+2 == 16) && (value.y%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y][value.x + 2].zone1 == -1)
			{
				value.x += 2;
				return 1;
			}			
		}
		else if((value.y == 0 || value.y == 10)&& ((value.x+2)%2 == 1))// Si mur bordure
		{
			if(plateau[value.y][value.x + 2].zone1 == -1)
			{
				value.x += 2;
				return 1;
			}	
		}
		else if	(plateau[value.y][value.x + 2].zone1 == -1 || plateau[value.y][value.x + 2].zone2 == -1)
		{	
			value.x += 2;
			return 1;	
		}
		
	}
	
	return 0;
}

/*
*	Recherche de nouvelle coordonn� en bas
*/
// Page 29
int RechercheCoordBasZoneInte(void) // Pour un mur vertical
{
//	printf("\nmur gauche bas = %d",plateau[value.y+1][value.x - 1].mur);
	if(plateau[value.y+1][value.x - 1].mur == 1 && (value.y+1<=10 && value.x-1>= 0)) // Simurpr�sent � gauche en bas
	{	
		if((value.x-1 == 0 || value.x-1 == 16) && ((value.y+1)%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y+1][value.x - 1].zone1 == -1)
			{
				value.y += 1;
				value.x -= 1;
				return 1;
			}			
		}
		else if((value.y+1 == 0 || value.y+1 == 10)&& ((value.x-1)%2 == 1))// Si mur bordure
		{
			if(plateau[value.y+1][value.x - 1].zone1 == -1)
			{
				value.y += 1;
				value.x -= 1;
				return 1;
			}	
		}
		else if(plateau[value.y+1][value.x - 1].zone1 == -1 || plateau[value.y+1][value.x - 1].zone2 == -1)
		{		
//			printf("\ngauche en bas");
			value.y += 1;
			value.x -= 1;
			return 1;
		}
	}

//	printf("\nmur droite bas = %d",plateau[value.y+1][value.x + 1].mur);
	if(plateau[value.y+1][value.x + 1].mur == 1&& (value.y+1<=10 && value.x+1<= 16)) // Si mur pr�sent � droite en bas
	{
		if((value.x+1 == 0 || value.x+1 == 16) && ((value.y+1)%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y+1][value.x + 1].zone1 == -1)
			{
				value.y += 1;
				value.x += 1;
				return 1;
			}			
		}
		else if((value.y+1 == 0 || value.y+1 == 10)&& ((value.x+1)%2 == 1))// Si mur bordure
		{
			if(plateau[value.y+1][value.x + 1].zone1 == -1)
			{
				value.y += 1;
				value.x += 1;
				return 1;
			}	
		}
//		printf("\nzone 1 = %d",plateau[value.y+1][value.x + 1].zone1);
	//	printf("\nzone 2 = %d",plateau[value.y+1][value.x + 1].zone2);
		else if(plateau[value.y+1][value.x + 1].zone1 == -1 || plateau[value.y+1][value.x + 1].zone2 == -1) 
		{
//			printf("\ndroite en bas");
			value.y += 1;
			value.x += 1;
			return 1;
		}	
	}
	
//	printf("\nmur bas = %d",plateau[value.y + 2][value.x].mur);
	if(plateau[value.y + 2][value.x].mur == 1&& (value.y+2<=10)) // Si mur vertical en bas
	{
		if((value.x == 0 || value.x == 16) && ((value.y+2)%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y + 2][value.x].zone1 == -1)
			{
				value.y += 2;
				return 1;
			}			
		}
		else if((value.y+2 == 0 || value.y+2 == 10)&& (value.x%2 == 1))// Si mur bordure
		{
			if(plateau[value.y + 2][value.x].zone1 == -1)
			{
				value.y += 2;
				return 1;
			}	
		}
		else if(plateau[value.y + 2][value.x].zone1 == -1 || plateau[value.y + 2][value.x].zone2 == -1)
		{
//			printf("\nen bas");
			value.y += 2;
			return 1;
		}
		
	}
	
	return 0;
}

/*
*	Recherche de nouvelle coordonn� en haut
*/
// Page 30
int RechercheCoordHautZoneInte(void) // Pour un mur vertical
{
	if(	 plateau[value.y - 1][value.x - 1].mur == 1&& (value.y-1>=0 && value.x-1>= 0)) // Simurpr�sent � gauche en haut
	{
//		printf("\ngauche en haut");
		if((value.x-1 == 0 || value.x-1 == 16) && ((value.y-1)%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y - 1][value.x - 1].zone1 == -1)
			{
				value.y -= 1;
				value.x -= 1;
				return 1;
			}			
		}
		else if((value.y-1 == 0 || value.y-1 == 10)&& ((value.x-1)%2 == 1))// Si mur bordure
		{
			if(plateau[value.y - 1][value.x - 1].zone1 == -1)
			{
				value.y -= 1;
				value.x -= 1;
				return 1;
			}	
		}
		else if(plateau[value.y - 1][value.x - 1].zone1 == -1 || plateau[value.y - 1][value.x - 1].zone2 == -1)
		{
			value.y -= 1;
			value.x -= 1;
			return 1;
		}
	}
	if(	 plateau[value.y - 1][value.x + 1].mur == 1&& (value.y-1>=0 && value.x+1<= 16)) // Si mur pr�sent � droite en haut
	{
//		printf("\nhaut droit");
		if((value.x+1 == 0 || value.x+1 == 16) && ((value.y-1)%2 == 1)) // Si mur bordure
		{
//			printf("\nhaut droit bordure");
			if(plateau[value.y - 1][value.x + 1].zone1 == -1)
			{
				value.y -= 1;
				value.x += 1;
				return 1;
			}			
		}
		else if((value.y-1 == 0 || value.y-1 == 10)&& ((value.x+1)%2 == 1))// Si mur bordure
		{
//			printf("\nhaut droit bordure");
			if(plateau[value.y - 1][value.x + 1].zone1 == -1)
			{
				value.y -= 1;
				value.x += 1;
				return 1;
			}	
		}
		else if(plateau[value.y - 1][value.x + 1].zone1 == -1 || plateau[value.y - 1][value.x + 1].zone2 == -1)
		{
//			printf("\nhaut droit plateau");
			value.y -= 1;
			value.x += 1;
			return 1;	
		}
	}
	if( plateau[value.y - 2][value.x].mur == 1&& (value.y-2>=0)) // Si mur vertical en haut
	{
//		printf("\nHaut");
		if((value.x == 0 || value.x == 16) && (value.y-2%2 == 1)) // Si mur bordure
		{
			if(plateau[value.y - 2][value.x].zone1 == -1)
			{
				value.y -= 2;
				return 1;
			}			
		}
		else if((value.y-2 == 0 || value.y-2 == 10)&& (value.x%2 == 1))// Si mur bordure
		{
			if(plateau[value.y-2][value.x].zone1 == -1)
			{
				value.y -= 2;
				return 1;
			}	
		}
		else if	(plateau[value.y - 2][value.x].zone1 == -1 || plateau[value.y - 2][value.x].zone2 == -1)
		{
			value.y -= 2;
			return 1;
		}
	}
	
	return 0;
}

/*
*	Recherche de nouvelle coordonn�. Fait la difference en un mur horizontal et verticale pour adapt� la recherche
*/
// Page 26
int RechercheCoordChoixZoneInte(void)
{
	int var;
	if((chemin[indiceChemin-1].y % 2 == 0) && (chemin[indiceChemin-1].x % 2 == 1)) // Si mur horizontal
	{
		if(chemin[indiceChemin-1].direction == 4) // Si direction gauche
		{
			var =RechercheCoordGaucheZoneInte(); // Page 27
		}
		else // Si direction droite
		{
			var =RechercheCoordDroitZoneInte(); // Page 28
		}
	}
	else // Si mur vertical
	{
		if(chemin[indiceChemin-1].direction == 3) // Si direction bas
		{
			var = RechercheCoordBasZoneInte(); // Page 29
		}
		else // Si direction haut
		{
			var = RechercheCoordHautZoneInte(); // Page 30
		}
	}
	return var;
}

/*
*	Cette fonction recherche les zones interieur. C'est � dire les zones qui ne touche pas les bordures.
*	C'est une boucle qui ne finit que lorsqu'elle trouve une zone, ou qu'un chemin ne mene � rien.
*/
void  RechercheZoneInte(int x,int y) // Page 35
{
	int sortie = 0;
	int val;
	int var;
	int depart = 1; // bloc pour value x et y au premier tour, sinon, il considere deja une zone
	int memoire;
	
	while(sortie == 0)
	{
		if((value.x == chemin[0].x) && (value.y == chemin[0].y) && (depart == 0))
		{
			printf("\n\nTamponne la zone interieur");
			TamponneZone(); // Page 32
			InitMemoireChemin();
			InitMemoireCarrefour();	
			sortie = 1;
		}
		else
		{
//			printf("\n\nRechercheNbChoix");
			val = RechercheNbChoix(value.x,value.y); // Page 10
//			printf("\nnombre de choix = %d",val);
			if(val > 1)
			{
//				printf("\n\nChoixCheminInte");
				ChoixCheminInte(); // Page 36
			}
			else
			{
				if(val == 1)
				{
					//indiceChemin --; // Pour adapt avec la fonction de controle.c
//					printf("\n\nRecherche Coord Choix");
					var = RechercheCoordChoixZoneInte(); //Rutilisation de la fonction RechercheCoordChoix P.26 Dans le programme "controle.c" )
//					printf("\nNouvelle coord -> %d",var);
//					printf("\nvalue.x = %d\nvalue.y = %d",value.x,value.y);
					chemin[indiceChemin].x = value.x;
					chemin[indiceChemin].y = value.y;
					MemoireDirection(); // Page 36 dans controle.c
					indiceChemin ++;				
				}
				else
				{
//					printf("\n\nRecherche Memoire");
//					printf("\nindiceCarrefour = %d",indiceCarrefour);
					
					memoire = RechercheMemoire();
//					printf("\nmemoire = %d",memoire);
					if(memoire == 0)// rutilisation de le fonciton recherche memoire P.31 dans le programme "controle.c" )
					{
						sortie = 1;
					}	
				}
			}
		}
		depart = 0;
/*		printf("\nChemin[%d].x=%d",indiceChemin-1,chemin[indiceChemin-1].x);
		printf("\nChemin[%d].y=%d",indiceChemin-1,chemin[indiceChemin-1].y);
		printf("\nChemin[%d].direction=%d",indiceChemin-1,chemin[indiceChemin-1].direction);
		printf("\nIndice Chemin = %d",indiceChemin);
*/	}
}

/*
*	Cette fonction envoie les coordonn�es des murs � l'interieur qui n'ont pas 2 zones d'enregistr�
*	pour determin� si ils peuvent former une nouvelle zone.
*/
void RechercheInterieur(void) // Page 34
{
	int x,y;
	
	for(x = 1; x <= 15;x++)
	{
		for(y = 1; y <= 9;y++)
		{
			if(plateau[y][x].mur == 1)
			{
				if((plateau[y][x].zone1 == -1) || (plateau[y][x].zone2 == -1))
				{
					value.x = x;
					value.y = y;
//					printf("\nplateau[%d][%d]",y,x);
//					printf("\n\nVide memoire chemin");
					VideMemoirChemin(); // Page 33
					InitMemoireCarrefour();
					
					chemin[indiceChemin].x = value.x;
					chemin[indiceChemin].y = value.y;
					
					if((value.x%2 == 1) && (value.y%2 == 0)) // Si mur horizontal
					{
						chemin[indiceChemin].direction = 4;
					}
					else
					{
						chemin[indiceChemin].direction = 3;
					}
					indiceChemin ++;
					
/*					printf("\nChemin[%d].x=%d",indiceChemin-1,chemin[indiceChemin-1].x);
					printf("\nChemin[%d].y=%d",indiceChemin-1,chemin[indiceChemin-1].y);
					printf("\nChemin[%d].direction=%d",indiceChemin-1,chemin[indiceChemin-1].direction);
					printf("\nIndice Chemin = %d",indiceChemin);
					
					printf("\n\nRechercheZoneInte");
*/					RechercheZoneInte(x,y); // Page 35
				}
			}
		}
	}
}

/*
*	Une fois les zones determin�, puis les vainqueurs par zone trouv�, cette fonction determine le
*	grand vainqueur puis appel la fonction d'affichage graphique pour une visualisation du vainqueur
*/
void AfficheFinPartie(void) // Page 42
{
	int i;
	int E1,E2,J1,J2,J3;
	E1 = 0;
	E2 = 0;
	J1 = 0;
	J2 = 0;
	J3 = 0;
	int vainqueurFinal;

	if(nombreDeJoueur == 4)
	{
		for(i=0;i<zone;i++)
		{
			if(datazone[i].vainqueur == 1)
			{
				E1 += datazone[i].orZone;
			}
			if(datazone[i].vainqueur == 2)
			{
				E2 += datazone[i].orZone;
			}
		}
		
		if(E1 < E2)
		{
			vainqueurFinal = 2;
		}
		else
		{
			vainqueurFinal = 1;
		}
		printf("\nScore");
		printf("\nE1 : %d",E1);
		printf("\nE2 : %d",E2);
	}
	else
	{
		for(i=0;i<zone;i++)
		{
			if(datazone[i].vainqueur == 1)
			{
				J1 += datazone[i].orZone;
			}
			if(datazone[i].vainqueur == 2)
			{
				J2 += datazone[i].orZone;
			}
			if(datazone[i].vainqueur == 3)
			{
				J3 += datazone[i].orZone;
			}
		}
		
		if((J1 > J2) && (J1 > J3))
		{
			vainqueurFinal = 1;
		}
		if((J2 > J1) && (J2 > J3))
		{
			vainqueurFinal = 2;
		}
		if((J3 > J1) && (J3 > J2))
		{
			vainqueurFinal = 3;
		}
		printf("\nScore");
		printf("\nJ1 : %d",J1);
		printf("\nJ2 : %d",J2);
		printf("\nJ3 : %d",J3);
	}
	
	AffichageGraphique(vainqueurFinal);
}

/*
*	Cette fonction corrige l'appel de fonction MemoireCarrefour, qui est une r�utilisation de controle.c.
*	Memoire carrefour enregistre tout les choix possible sans se soucier si un mur est deja pris par 2 zones.
*	Si c'est le cas, CorrestionMemoire met le flag � 0 sur le mur correspondant.
*/
void CorrectionMemoire(void)
{
	indiceChemin --;
	chemin[indiceChemin].x=-1;
	chemin[indiceChemin].y=-1;
	chemin[indiceChemin].direction=-1;
	
	if(carrefour[indiceCarrefour-1].flagchoix1 == 0)
	{
		carrefour[indiceCarrefour-1].flagchoix1 = 1;
	}
	if(carrefour[indiceCarrefour-1].flagchoix2 == 0)
	{
		carrefour[indiceCarrefour-1].flagchoix2 = 1;
	}
	if(carrefour[indiceCarrefour-1].flagchoix3 == 0)
	{
		carrefour[indiceCarrefour-1].flagchoix3 = 1;
	}
}

/*
*	Une fois qu'une zone est trouv�, il faut pouvoir repartir sur le bon mur de bordure pour cherche une nouvelle zone.
*	Cette fonction se charge de trouv� ces coordonn�es.
*/
void RechercheDepartNouvZone(int x,int y,int direction)
{
/*	printf("\nmemo.x = %d",memo.x);
	printf("\nmemo.y = %d",memo.y);
	printf("\nmemo.direction = %d",memo.direction);
*/	if(direction == 3)
	{
		if(y == 9)
		{
			value.x = 1;
			value.y = 10;
			memo.direction = 2;
		}
		else
		{
			value.x = memo.x;
			value.y = memo.y+2;
			memo.direction = 3;
		}
	}
	if(direction == 1)
	{
		if(y == 1)
		{
			value.x = 15;
			value.y = 0;
			memo.direction = 4;
		}
		else
		{
			value.x = memo.x;
			value.y = memo.y-2;
			memo.direction = 1;
		}
	}
	if(direction == 2)
	{
		if(x == 15)
		{
			value.x = 16;
			value.y = 9;
			memo.direction = 1;
		}
		else
		{
			value.x = memo.x+2;
			value.y = memo.y;
			memo.direction = 2;
		}
	}
	if(direction == 4)
	{
		if(x == 1)
		{
			printf("\nERROR : Nouvelle affectation de zone impossible\n");
		}
		else
		{
			value.x = memo.x-2;
			value.y = memo.y;
			memo.direction = 4;
		}
	}
//	printf("\nvalue.x = %d",value.x);
//	printf("\nvalue.y = %d",value.y);
	//printf("\nmemo.direction = %d",memo.direction);
}

/*
*	C'est cette fonction qui est appel� dans le main.c. A la fin de la partie, cette fonction
*	fait les appelles de toutes les fonctions necessaire � la determination du grand gagnant de la partie.
*/
int ControleDesScores(void) // Page 1
{
	InitMemoireChemin();
	InitMemoireCarrefour();
	Initialisation(); // Page 2
	int etat = 0;
	int sortie = 0;
	_Bool carrefour;
	_Bool mur;
	
	while(sortie == 0)
	{
		switch(etat)
		{
			case 0:
//				printf("\n\n******************RECHERCHE NOUVELLE COORD******************");
				carrefour=RechercheNouvelleCoord(value.x,value.y);// Page 3
				if(carrefour)
				{
					etat = 1;
				}
				else
				{
					etat = 2;
				}
				break;
			case 1:
//				printf("\n\n******************RECHERCHE ZONE******************");
				memo.x = chemin[indiceChemin-2].x;
				memo.y = chemin[indiceChemin-2].y;
				memo.direction = chemin[indiceChemin-2].direction;
/*				printf("\nmemo.x = %d",memo.x);
				printf("\nmemo.y = %d",memo.y);
				printf("\nmemo.direction = %d",memo.direction);
				
				printf("\n\nRechercheZone");
*/				RechercheZone(value.x,value.y); // Page9
//				printf("\n\nRechercheDepartNouvZone");
				RechercheDepartNouvZone(memo.x,memo.y,memo.direction);
				
				chemin[indiceChemin].x=value.x;
				chemin[indiceChemin].y=value.y;
				chemin[indiceChemin].direction=memo.direction;
				indiceChemin++;
/*				printf("\nChemin[%d].x=%d",indiceChemin-1,chemin[indiceChemin-1].x);
				printf("\nChemin[%d].y=%d",indiceChemin-1,chemin[indiceChemin-1].y);
				printf("\nChemin[%d].direction=%d",indiceChemin-1,chemin[indiceChemin-1].direction);
				printf("\nIndice Chemin = %d",indiceChemin);
*/			//	value.x=memo.x;
			//	value.y=memo.y;
				etat = 2;
				break;
			case 2:
//				printf("\n\n******************RECHERCHE SI RESTE MUR BORDURE******************");
//				printf("\n\nRechercheSiResteMurBordure");
				mur = RechercheSiResteMurBordure(); // Page 8
//				printf("\nmur = %d",mur);
				if(mur)
				{
					etat = 0;
				}
				else
				{
					sortie = 1;
				}
				break;
		}
	}
//	printf("\n\n******************RECHERCHE INTERIEUR******************");
//	printf("\n\nInitMemoireChemin");
	InitMemoireChemin();
//	printf("\n\nInitMemoireCarrefour");
	InitMemoireCarrefour();	
//	printf("\n\nRechercheInterieur");
	RechercheInterieur(); // Page 34
//    printf("\n\n******************COMPTABILISE LES POINTS******************");
    ComptabilisePoints(); // Page 38
//
    VisualisationPoints();
//    printf("\n\n******************AFFICHE FIN DE PARTIE******************");
    printf("\n\n");
    AfficheFinPartie(); // Page 42
    return 1;
}
