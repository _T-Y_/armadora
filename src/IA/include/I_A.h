#ifndef HEADER_IA
#define HEADER_IA

	void I_A(int numJoueur);
	void Murs_I_A(void);
	void Jeton_I_A(int numJoueur);
	void Valeur_Jeton_I_A(int numJoueur, int x, int y);

#endif
