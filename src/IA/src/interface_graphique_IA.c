#include <stdio.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h" /* Inclusion du header de SDL_image (adapter le dossier au besoin) */
#include "variables_globales.h"
#include "interface_graphique_plateau.h"

void Image(int numJoueur)
{
	// Ecran de bienvenu
	SDL_Surface *ecran = NULL, *Fond = NULL; // Pointeur sur image

	SDL_Rect positionFond; // Position de l'image
    positionFond.x = 0;
    positionFond.y = 0;

    SDL_Init(SDL_INIT_VIDEO);// Init de la SDL
    ecran = SDL_SetVideoMode(900, 684, 32, SDL_HWSURFACE | SDL_DOUBLEBUF); // Cr�� une fenetre

    SDL_WM_SetCaption("Armadora", NULL);// Titre de la fenetre
    SDL_WM_SetIcon(IMG_Load("src/interface/image/image_menu/arma.bmp"), NULL); // Icon

    // 1=Mage, 2=Ork, 3=Troll, 4=Elf

    if(joueur[numJoueur].hero == 1)
    {
    	Fond = IMG_Load("src/interface/image/plateau/Mage.bmp");// Image de fond
    	SDL_BlitSurface(Fond, NULL, ecran, &positionFond);// Copie de l'image sur la fenetre
    	SDL_Flip(ecran);// restaure la fentre avec les copies d'images
    }
    if(joueur[numJoueur].hero == 2)
    {
    	Fond = IMG_Load("src/interface/image/plateau/Ork.bmp");// Image de fond
    	SDL_BlitSurface(Fond, NULL, ecran, &positionFond);// Copie de l'image sur la fenetre
    	SDL_Flip(ecran);// restaure la fentre avec les copies d'images
    }
    if(joueur[numJoueur].hero == 3)
    {
    	Fond = IMG_Load("src/interface/image/plateau/Troll.bmp");// Image de fond
    	SDL_BlitSurface(Fond, NULL, ecran, &positionFond);// Copie de l'image sur la fenetre
    	SDL_Flip(ecran);// restaure la fentre avec les copies d'images
    }
    if(joueur[numJoueur].hero == 4)
    {
    	Fond = IMG_Load("src/interface/image/plateau/Elf.bmp");// Image de fond
    	SDL_BlitSurface(Fond, NULL, ecran, &positionFond);// Copie de l'image sur la fenetre
    	SDL_Flip(ecran);// restaure la fentre avec les copies d'images
    }
    SDL_Delay(1000);
	SDL_FreeSurface(Fond); /* On lib�re la surface */
	SDL_FreeSurface(ecran); /* On lib�re la surface */
}

void Visu_IA(void)
{
	Affichage(0);
	SDL_Delay(2000);
}
