#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "I_A.h"
#include "variables_globales.h"
#include "interface_graphique_IA.h"
#include "controle.h"

void I_A(int numJoueur)
{
	printf("\n***************************\n\tIA.%d",numJoueur);
	int choix;
	int x;
	int y;

	// Image du hero IA
	Image(numJoueur);

	srand (time (NULL));
	choix = rand()%101;

	/*
	 * 40% de chance pour poser deux murs
	 * 60% de chance de poser un jeton
	 * La pose de murs et de jetons se fait de façon aléatoire
	 */
	if(choix <= 40) // Pose de mur
	{
		printf("\nIA : Poser 2 murs");
		Murs_I_A();
	}
	if(choix > 40) // Pose de jeton
	{
		printf("\nIA : Poser un jeton");
		Jeton_I_A(numJoueur);
	}
	printf("\n***************************\n");
}

void Jeton_I_A(int numJoueur)
{
	// if(x%2 == 1 && y%2 == 1) // coord posible pour les jetons
	// de x = 1 à x = 15, et de y = 1 à y = 9

	int pose = 0;
	int i;
	int pass = 1;

	for(i=0;i<6;i++) // Regarde si il reste des ressources
	{
		if(joueur[numJoueur].source[i] > 0)
		{
			pass = 0;
		}
	}

	if(pass == 0) // Si il reste au moins un jeton
	{
		printf("\nIA : Recherche d'une case libre ...");
		fflush(stdout);
		while(pose == 0)
		{
			srand (time (NULL));
			int x = rand()%16;
			int y = rand()%10;

			if(x%2 == 1 && y%2 == 1)
			{
				if(plateau[y][x].hero == 0)
				{
					if(plateau[y][x].mine == 0)
					{
						Valeur_Jeton_I_A(numJoueur,x,y);
						printf("\nIA : Jeton, x = %d / y = %d",x,y);
						pose = 1;
					}
				}
			}
		}
	}

	else
	{
		printf("\nIA : n'a plus de jeton\n");
	}
}

void Valeur_Jeton_I_A(int numJoueur, int x, int y)
{
	int i;

	srand (time (NULL));
	i = rand () % 7;

	while(joueur[numJoueur].source[i] == 0){i = rand () % 7;}

	joueur[numJoueur].source[i] --;
	plateau[y][x].hero = joueur[numJoueur].hero;
	plateau[y][x].valeurJeton = i;
	Visu_IA();
}

void Murs_I_A(void)
{
	int pose = 0;
	int verif;
	int i = 0;
	int x;
	int y;
	// Verif
	for(y=0;y<11;y++)
	{
		for(x=0;x<17;x++)
		{
			if(	(y%2 == 1 && x%2 == 0) || (y%2 == 0 && x%2 == 1)	)
			{
				if(plateau[y][x].mur == 0 && plateau[y][x].flag_interdit == 0)
				{
					i ++;

				}
			}
		}
	}
	if(	i	>=	4	)
	{
		verif = 1;
	}
	else
	{
		verif = 0;
	}

	if(verif == 1)
	{
		for(i=0;i<2;i++)
		{
			printf("\nIA : Recherche les coordonnées du mur %d ...",i+1);
			fflush(stdout);
			Surbrillance();//nous permet de mettre en surbrillance ou pas les murs du plateau selon leurs disponibilité
			while(pose == 0)
			{
				srand (time (NULL));
				int x = rand()%16;
				int y = rand()%10;

				if(	(y%2 == 0 && x%2 == 1) || (y%2 == 1 && x%2 == 0)	)
				{
					if(plateau[y][x].mur == 0)
					{
						if(plateau[y][x].flag_interdit == 0)
						{
							printf("\nIA : Mur %d en x = %d / y = %d",i,x,y);
							plateau[y][x].mur = 1;
							pose = 1;
						}
					}
				}
			}
			pose = 0;
			Visu_IA();
		}
	}
	else
	{
		printf("\nIA : Moins de 2 murs disponibles\n");
		printf("\nIA : Poser 2 murs");
		Murs_I_A();
	}
}
