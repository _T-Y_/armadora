#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "SDL/SDL.h"
#include <SDL/SDL_ttf.h>
#include "variables_globales.h"
#include "initialisation.h"
#include "menu.h"
#include "controle.h"
#include "fin_de_partie.h"
#include "Gestiontour.h"
#include "I_A.h"


int main(void)
{
	int tour;
	int finDuJeu = 0;
	zapper = 0;
	TEMPS = 1;
	/*---------Affichage du menu de depart--------------*/
	Menu();

	/*----------GESTION DU TOUR-------------------------*/
	while(finDuJeu==0){
	    for(tour=1;tour<=nombreDeJoueur;tour++){
	    	if(joueur[tour].type == 1) // Joueur
			{
				finDuJeu=Gestiontour(tour);
				if(finDuJeu == 1)
				{
					break;
				}
			}
	    	else if(joueur[tour].type == 2) // I.A
			{
				I_A(tour);
			}
			else
			{
				printf("\nERROR : Joueur non déterminé\n");
				exit(1);
			}

	    }
	}
	
	/*------------Affichage de la fin du jeu------------------*/
	ControleDesScores();	
	
	TTF_Quit();
	SDL_Quit();
	return 1;
}
