#include "menu.h"
#include "interface_graphique_menu.h"
#include "variables_globales.h"
#include "initialisation.h"

/*
*	Cette fonction est appel� qu'au lancement du jeux.
*	Elle permet de collecter toutes les informations des joueurs et de g�n�rer les
*	donn�es pour la partie.
*/
void Menu(void)
{
	AffichageStart(); // Simple affichage de d�but de jeux.
	nombreDeJoueur = AffichageChoixNbJoueur();// nombreDeJoueur est une variable globale. Elle recois le nombre choisi par l'utilisateur
	AffichageDataJoueur(nombreDeJoueur);// On choisit le heros, le type, et l'�quipe en fonction du nombre de joueur
	InitialisationDuPlateau();// Genere les donn�es du plateau pour le d�but de partie
	InitialisationDesMines();// Genere une attribution al�atoire du nombre d'or pour les mines
	
	int i;
	// Genere le nombre d'arm� en fonction du nombre de joueur
	if (nombreDeJoueur==2){
        for(i=1;i<=2;i++)
        {
            joueur[i].source[0] = 0;
            joueur[i].source[1] = 11;
            joueur[i].source[2] = 2;
            joueur[i].source[3] = 1;
            joueur[i].source[4] = 1;
            joueur[i].source[5] = 1;

        }
    }
    if(nombreDeJoueur==3)
    {
        for(i=1;i<=3;i++)
        {
            joueur[i].source[0] = 0;
            joueur[i].source[1] = 7;
            joueur[i].source[2] = 2;
            joueur[i].source[3] = 1;
            joueur[i].source[4] = 1;
            joueur[i].source[5] = 0;
        }
    }
    if(nombreDeJoueur==4)
    {
        for(i=1;i<=4;i++)
        {
            joueur[i].source[0] = 0;
            joueur[i].source[1] = 5;
            joueur[i].source[2] = 1;
            joueur[i].source[3] = 1;
            joueur[i].source[4] = 0;
            joueur[i].source[5] = 0;
        }
    }
}
