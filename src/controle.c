/*
*	R�alis� par Th�ophile Yvars
*/


/*

Descriptif du programme;

Ce programme � pour but d'analyse les murs non d�j� positionn�.
Une regle impose qu'on ne peut former une zone de moins de 4 cases.
Donc, lorsque le joueur d�cidera de poser un mur, cet algorithme de contr�le s'executera.
Il selectionnera ces murs non d�ja positionn�, est v�rifira si il forme potentiellement une zone interdite.
Si c'est le cas, il n'apparaitera pas en surbrillance au joueur.

Dans une premiere partie, le programme d�finira si 2 murs apparaisses de part et d'autre le mur non deja positionn�.
Si ce n'est pas le cas, aucune chance qu'il forme une zone interdire.
Si c'est le cas, il parcoura les murs pour trouver si il forme une zone, puis il l'analysera pour en d�finir si la pose 
de ce ur est autoris� ou non.

*/
#include <stdio.h>
#include <stdbool.h>
#include "variables_globales.h"
#include "initialisation.h"
#include "controle.h"
/*
PARTIE 1 = Recherche si mur des 2 cot�s d'une proposition de mur pour savoir si un zone est potentiellement existante
PARTIE 2 = Recherche si plusieurs chois possible
PARTIE 3 = Avance et enregistre les donn�es 
PARTIE 4 = Gestion des carrefours
PARTIE 5 = liaison entre PARTIE 4, PARTIE 3 et PARTIE 2
PARTIE 6 = Initialisation et fin
PARTIE 7 = liaison entre PARTIE 6 et PARTIE 5
PARTIE 8 = liaison entre PARTIE 7 et PARTIE 1
PARTIE 9 = Envoie des coords, definit si mur en surbrillance ou non
*/

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 1
//-------------------------------------------------------------------------------------------------------------

/*
*	Regarde si il y a un mur present � droite, sur un mur horizonal
*/
// Page 12
_Bool MurPresentDroit(int x, int y) // sur un mur horizontal
{
	if(plateau[y-1][x+1].mur == 1) // Mur pr�sent en haut � droite 
	{
		return true;
	}
	else if(plateau[y+1][x+1].mur == 1) // Mur pr�sent en bas � droite  
	{
		return true;
	}
	else if(plateau[y][x+2].mur == 1) // Mur pr�sent � l'horizontal � droite 
	{
		return true;
	}
	else // Pas de mur trouv�
	{
		return false;
	}
}

/*
*	Regarde si il y a un mur present � gauche, sur un mur horizonal
*/
// Page 13
_Bool MurPresentGauche(int x, int y) // sur un mur horizontal
{
	if(plateau[y-1][x-1].mur == 1) // Mur pr�sent en haut � gauche 
	{
		return true;
	}
	else if(plateau[y+1][x-1].mur == 1) // Mur pr�sent en bas � gauche
	{
		return true;
	}
	else if(plateau[y][x-2].mur == 1) // Mur pr�sent � l'horizontal � gauche 
	{
		return true;
	}
	else // Pas de mur trouv�
	{
		return false;
	}
}

/*
*	Regarde si il y a un mur present en haut, sur un mur vertical
*/
// Page 14
_Bool MurPresentHaut(int x, int y) // sur un mur vertical
{
	if(plateau[y-1][x-1].mur == 1) // Mur pr�sent � gauche en haut
	{
		return true;
	}
	else if(plateau[y-1][x+1].mur == 1) // Mur pr�sent � droite en haut
	{
		return true;
	}
	else if(plateau[y-2][x].mur == 1) // Mur pr�sent verticale en haut
	{
		return true;
	}
	else // Pas de mur trouv�
	{
		return false;
	}
}

/*
*	Regarde si il y a un mur present en bas, sur un mur vertical
*/
// Page 15
_Bool MurPresentBas(int x, int y) // sur un mur vertical
{
	if(plateau[y+1][x-1].mur == 1) // Mur pr�sent � gauche en bas
	{
		return true;
	}
	else if(plateau[y+1][x+1].mur == 1) // Mur pr�sent � droite en bas
	{
		return true;
	}
	else if(plateau[y+2][x].mur == 1) // Mur pr�sent verticale en bas
	{
		return true;
	}
	else // Pas de mur trouv�
	{
		return false;
	}
}

/*
*	Fonction qui controle la presence de mur � droite. 
*	le mur suivant et un mur de bordure, il y forcement un mur present
*	Sinon, l'appel d'une fontion se chargera de controler
*/
// Page 8
_Bool MurADroite(int x, int y)
{
	_Bool var;
	if(x+1 == 15)// Si mur � droite bordure
	{
		return true;
	}
	else
	{
		var = MurPresentDroit(x,y);// cherche un mur � droite // Page 12
		return var;
	}
}

/*
*	Fonction qui controle la presence de mur � gauche. 
*	le mur suivant et un mur de bordure, il y forcement un mur present
*	Sinon, l'appel d'une fontion se chargera de controler
*/
// Page 9
_Bool MurAGauche(int x, int y)
{
	_Bool var;
	if(x-1 == 0)// Si mur � gauche bordure
	{
		return true;
	}
	else
	{
		var = MurPresentGauche(x,y);//cherche un mur � gauche // Page 13
		return var;
	}
}

/*
*	Fonction qui controle la presence de mur en haut. 
*	le mur suivant et un mur de bordure, il y forcement un mur present
*	Sinon, l'appel d'une fontion se chargera de controler
*/
// Page 10
_Bool MurEnHaut(int x, int y)
{
	_Bool var;
	if(y-1 == 0)// Si mur en haut bordure
	{
		return true;
	}
	else
	{
		var = MurPresentHaut(x,y);// cherche un mur en haut // Page 14
		return var;
	}
}

/*
*	Fonction qui controle la presence de mur en bas. 
*	le mur suivant et un mur de bordure, il y forcement un mur present
*	Sinon, l'appel d'une fontion se chargera de controler
*/
// Page 11
_Bool MurEnBas(int x, int y)
{
	_Bool var;
	if(y+1 == 10)// Si mur en bas bordure
	{
		return true;
	}
	else
	{
		var = MurPresentBas(x,y);// cherche un mur en bas // Page 15
		return var;
	}
}

/*
*	Pour un mur horizontal, controle sur les 2 cot�s du mur, si un mur present
*/
// Page 6
_Bool Cont6PossHorizontal(int x, int y) // Regarde si mur un mur pr�sent des 2 cotes, sur un mur Horizontal
{
	if(MurADroite(x,y)) // Page 8
	{
		if(MurAGauche(x,y)) // Page 9
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

/*
*	Pour un mur vertical, controle sur les 2 cot�s du mur, si un mur present
*/
// Page 7
_Bool Cont6PossVertical(int x, int y) // Regarde si mur un mur pr�sent des 2 cotes, sur un mur Vertical
{
	if(MurEnHaut(x,y)) // Page 10
	{
		if(MurEnBas(x,y)) // Page 11
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

/*
*	Distingue si c'est un mur verticale ou horizontal, et controle la presence de mur en fonction.
*/
// Page 5
_Bool Controle6Poss(int x, int y)
{
	if((x%2 == 1) && (y%2 == 0)) // Si mur horizontal
	{
		if(Cont6PossHorizontal(x,y)) // Page 6
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else // si mur vertical
	{
		if(Cont6PossVertical(x,y)) // Page 7
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

/*
*	Petite redondance avec la fonction precedante.
*	C'est cette fonction qui est appel� pour savoir si il y a des murs de chaque cot� des
*	coordonn� control�
*/
// Page 4
_Bool RechercheMur2Cotes(int x, int y)
{
	if(Controle6Poss(x,y)) // Page 5
	{
		return true;
	}
	else
	{
		return false;
	}
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 2
//-------------------------------------------------------------------------------------------------------------
/*
*	Compte le nombre de mur disponible � gauche d'un mur horizonal
*	Pour d�termin� plus tard si il s'agit d'un carrefour, ou d'un simple chemin
*/
// Page 19
int CompteNbChoixPossGauche(void) // verifie si simple mur ou si carrefour, sur un mur horizontal
{
	int var;
	var = 0;
	
	if(plateau[value.y-1][value.x-1].mur == 1 && (value.y-1>=0 && value.x-1>= 0)) // Mur pr�sent en haut � gauche?
	{
		var ++;
	}
	if(plateau[value.y][value.x-2].mur == 1&& (value.x-2>= 0)) // Mur pr�sent � l'horizontal � gauche?
	{
		var ++;
	}
	if(plateau[value.y+1][value.x-1].mur == 1&& (value.y+1<=10 && value.x-1>= 0)) // Mur pr�sent en bas � gauche?
	{
		var ++;
	}
	return var;
}

/*
*	Compte le nombre de mur disponible � droite d'un mur horizonal
*	Pour d�termin� plus tard si il s'agit d'un carrefour, ou d'un simple chemin
*/
// Page 20
int CompteNbChoixPossDroite(void) // verifie si simple mur ou si carrefour, sur un mur horizontal
{
	int var;
	var = 0;
	
	if(plateau[value.y-1][value.x+1].mur == 1&& (value.y-1>=0 && value.x+1<= 16)) // Mur pr�sent en haut � droite?
	{
		var ++;
	}
	if(plateau[value.y][value.x+2].mur == 1&& (value.x+2<= 16)) // Mur pr�sent � l'horizontal � droite?
	{
		var ++;
	}
	if(plateau[value.y+1][value.x+1].mur == 1&& (value.y+1<=10 && value.x+1<= 16)) // Mur pr�sent en bas � droite?
	{
		var ++;
	}
	return var;
}

/*
*	Compte le nombre de mur disponible en bas d'un mur horizonal
*	Pour d�termin� plus tard si il s'agit d'un carrefour, ou d'un simple chemin
*/
// Page 21
int CompteNbChoixPossBas(void) // verifie si simple mur ou si carrefour, sur un mur vertical
{
	int var;
	var = 0;
	
	if(plateau[value.y+1][value.x-1].mur == 1&& (value.y+1<=10 && value.x-1>= 0)) // Mur pr�sent � gauche en bas?
	{
		var ++;
	}
	if(plateau[value.y+2][value.x].mur == 1&& (value.x+2<= 16)) // Mur pr�sent � la vertical en bas?
	{
		var ++;
	}
	if(plateau[value.y+1][value.x+1].mur == 1&& (value.y+1<=10 && value.x+1<= 16)) // Mur pr�sent � droite en bas?
	{
		var ++;
	}
	return var;
}

/*
*	Compte le nombre de mur disponible en haut d'un mur horizonal
*	Pour d�termin� plus tard si il s'agit d'un carrefour, ou d'un simple chemin
*/
// Page 22
int CompteNbChoixPossHaut(void) // verifie si simple mur ou si carrefour, sur un mur vertical
{
	int var;
	var = 0;
	
	if(plateau[value.y-1][value.x-1].mur == 1&& (value.y-1>=0 && value.x-1>= 0)) // Mur pr�sent � gauche en haut?
	{
		var ++;
	}
	if(plateau[value.y-2][value.x].mur == 1&& (value.y-2>=0)) // Mur pr�sent � la vertical en haut?
	{
		var ++;
	}
	if(plateau[value.y-1][value.x+1].mur == 1&& (value.y-1>=0 && value.x+1<= 16)) // Mur pr�sent � droite en haut?
	{
		var ++;
	}
	return var;
}

/*
*	Determine si les coordonn�es analys� corespondante � un murhorizontal ou vertical
*	Puis, compte le nombre de mur pr�sent de chaque cot�
*/
// Page 23
int ControleNbChoixPossible(void)
{
	int var;
	if((value.x%2 == 1) && (value.y%2 == 0)) // Si mur horizontal
	{
		if(chemin[indiceChemin-1].direction == 4) // Direction gauche
		{
			var = CompteNbChoixPossGauche(); // Page 19
		}
		else // Direction droite
		{
			var = CompteNbChoixPossDroite(); // Page 20
		}
	}
	else // Mur vertical
	{
		if(chemin[indiceChemin-1].direction == 3) // Direction bas
		{
			var = CompteNbChoixPossBas(); // Page 21
		}
		else // Direction droite
		{
			var = CompteNbChoixPossHaut(); // Page 22
		}
	}
	return var;
}

/*
*	Controle le nombre de choix possible � l'origine.
*	Permet de savoir si il est necessaire de controler la pr�sence de zone ou non
*/
// Page 18
int ControleNbChoixPossOrigine(void)
{
	int var;
	if((value.x%2 == 1) && (value.y%2 == 0)) // Si mur horizontal
	{
		var = CompteNbChoixPossGauche();// Page 19
	}
	else
	{
		var = CompteNbChoixPossBas(); // Page 21
	}
	return var;
}

/*
*	Fonction qui est appeler pour le comptage de nombre de mur disponible de chaque cot�
*/
// Page 17
int RechercheSiPlusieurChoixPossible(void)
{
	if(indiceChemin == 1)
	{
		return ControleNbChoixPossOrigine(); // Page 18
	}
	else
	{
		return ControleNbChoixPossible(); // Page 23
	}
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 3
//-------------------------------------------------------------------------------------------------------------

/*
*	Si un seul mur pr�sent � gauche, recherche les coords du mur
*/
// Page 27
int RechercheCoordGauche(void) // Pour un mur horizontal
{
	if(plateau[value.y - 1][value.x - 1].mur == 1&& (value.y-1>=0 && value.x-1>= 0)) // Si mur pr�sent en haut � gauche
	{
		value.y -= 1;
		value.x -= 1;
		return 1;
	}
	else if(plateau[value.y + 1][value.x - 1].mur == 1&& (value.y+1<=10 && value.x-1>= 0)) // Si mur pr�sent en bas � gauche
	{
		value.y += 1;
		value.x -= 1;
		return 1;
	}
	else if(plateau[value.y][value.x - 2].mur == 1&& (value.x-2>= 0))// Si mur horizontal � gauche
	{
		value.x -= 2;
		return 1;
	}
	else
	{
		return 0;
	}
}

/*
*	Si un seul mur pr�sent � droite, recherche les coords du mur
*/
// Page 28
int RechercheCoordDroit(void) // Pour un mur horizontal
{
	if(plateau[value.y - 1][value.x + 1].mur == 1&& (value.y-1>=0 && value.x+1<= 16)) // Si mur pr�sent en haut � droite
	{
		value.y -= 1;
		value.x += 1;
		return 1;
	}
	else if(plateau[value.y + 1][value.x + 1].mur == 1&& (value.y+1<=10 && value.x+1<= 16)) // Si mur pr�sent en bas � droite
	{
		value.y += 1;
		value.x += 1;
		return 1;
	}
	else if(plateau[value.y][value.x + 2].mur == 1&& (value.x+2<= 16)) // Si mur horizontal � droite
	{
		value.x += 2;
		return 1;
	}
	else
	{
		return 0;
	}
}

/*
*	Si un seul mur pr�sent en bas, recherche les coords du mur
*/
// Page 29
int RechercheCoordBas(void) // Pour un mur vertical
{
	if(plateau[value.y+1][value.x - 1].mur == 1&& (value.y+1<=10 && value.x-1>= 0)) // Simurpr�sent � gauche en bas
	{
		value.y += 1;
		value.x -= 1;
		return 1;
	}
	else if(plateau[value.y+1][value.x + 1].mur == 1&& (value.y+1<=10 && value.x+1<= 16)) // Si mur pr�sent � droite en bas
	{
		value.y += 1;
		value.x += 1;
		return 1;
	}
	else if(plateau[value.y + 2][value.x].mur == 1&& (value.y+2<=10)) // Si mur vertical en bas
	{
		value.y += 2;
		return 1;
	}
	else
	{
		return 0;
	}
}

/*
*	Si un seul mur pr�sent � gauche, recherche les coords du mur
*/
// Page 30
int RechercheCoordHaut(void) // Pour un mur vertical
{
	if(plateau[value.y - 1][value.x - 1].mur == 1&& (value.y-1>=0 && value.x-1>= 0)) // Simurpr�sent � gauche en haut
	{
		value.y -= 1;
		value.x -= 1;
		return 1;
	}
	else if(plateau[value.y - 1][value.x + 1].mur == 1&& (value.y-1>=0 && value.x+1<= 16)) // Si mur pr�sent � droite en haut
	{
		value.y -= 1;
		value.x += 1;
		return 1;
	}
	else if(plateau[value.y - 2][value.x].mur == 1&& (value.y-2>=0)) // Si mur vertical en haut
	{
		value.y -= 2;
		return 1;
	}
	else
	{
		return 0;
	}
}

/*
*	Distingue si c'est un mur horizontal, ou vertical, puis recherche la nouvelle coordonn�e
*	Ces fonctions sont appel� uniquement si un seul mur a �t� d�t�cter
*/
// Page 26
int RechercheCoordChoix(void)
{
	int var;
	if((chemin[indiceChemin-1].y % 2 == 0) && (chemin[indiceChemin-1].x % 2 == 1)) // Si mur horizontal
	{
		if(chemin[indiceChemin-1].direction == 4) // Si direction gauche
		{
			var =RechercheCoordGauche(); // Page 27
		}
		else // Si direction droite
		{
			var =RechercheCoordDroit(); // Page 28
		}
	}
	else // Si mur vertical
	{
		if(chemin[indiceChemin-1].direction == 3) // Si direction bas
		{
			var = RechercheCoordBas(); // Page 29
		}
		else // Si direction haut
		{
			var = RechercheCoordHaut(); // Page 30
		}
	}
	return var;
}

/*
*	Lorsqu'une coordonn�e est rrouv�e, il faut lui attribuer une direction.
*	En fonction de l'orientation du mur, et des coordonn�es precedante, une direction est attribu�
*	au nouveau mur
*/
// Page 36
void MemoireDirection(void)
{
	if((chemin[indiceChemin-1].y % 2 == 0) && (chemin[indiceChemin-1].x % 2 == 1)) // Si mur horizontal
	{
		if(chemin[indiceChemin - 1].x <  value.x)// Si Mur pr�c�dant en m�moire pr�sent sur la gauche
		{
			
			if(chemin[indiceChemin-1].y >  value.y)
			{
				chemin[indiceChemin].direction = 1;
			}
			else if(chemin[indiceChemin-1].y <  value.y)
			{
				chemin[indiceChemin].direction = 3;
			}
			else if(chemin[indiceChemin-1].y ==  value.y)
			{
				chemin[indiceChemin].direction = 2;
			}
			else
			{
				printf("\nERREUR : enregistrement direction\n");
			}
		}
		else // Si Mur pr�c�dant en m�moire pr�sent sur la droite
		{
			if(chemin[indiceChemin-1].y >  value.y)
			{
				chemin[indiceChemin].direction = 1;
			}
			else if(chemin[indiceChemin-1].y <  value.y)
			{
				chemin[indiceChemin].direction = 3;
			}
			else if(chemin[indiceChemin-1].y ==  value.y)
			{
				chemin[indiceChemin].direction = 4;
			}
			else
			{
				printf("\nERREUR : enregistrement direction\n");
			}
		}
	}
	else // Mur vertical
	{
		if(chemin[indiceChemin - 1].y >  value.y)// Si Mur pr�c�dant en m�moire pr�sent sur le bas
		{
			if(chemin[indiceChemin-1].x >  value.x)
			{
				chemin[indiceChemin].direction = 4;
			}
			else if(chemin[indiceChemin-1].x <  value.x)
			{
				chemin[indiceChemin].direction = 2;
			}
			else if(chemin[indiceChemin-1].x ==  value.x)
			{
				chemin[indiceChemin].direction = 1;
			}
			else
			{
				printf("\nERREUR : enregistrement direction\n");
			}
		}
		else// Si Mur pr�c�dant en m�moire pr�sent sur le haut
		{
			if(chemin[indiceChemin-1].x >  value.x)
			{
				chemin[indiceChemin].direction = 4;
			}
			else if(chemin[indiceChemin-1].x <  value.x)
			{
				chemin[indiceChemin].direction = 2;
			}
			else if(chemin[indiceChemin-1].x ==  value.x)
			{
				chemin[indiceChemin].direction = 3;
			}
			else
			{
				printf("\nERREUR : enregistrement direction\n");
			}
		}
	}
}

/*
*	Sauvegarde les nouvelles coordonn�es dans le tableau de structure chemin
*	Appel de la fonction direction pour sauvegarder la direction
*/
// Page 33
void MemoireChemin(void)
{
	chemin[indiceChemin].x = value.x;
	chemin[indiceChemin].y = value.y;
	MemoireDirection(); // Page 36
	indiceChemin ++; 
}

/*
*	Si plus aucune coordonn�e trouv�, cette fonction verifie si il ne reste pas des
*	coordonn�es sauvegard� d'un carrefour rencontr� encore pas explor�.
*/
// Page 32
int GestionMemoireCarrefour(void)
{
	int memoire;
	int i;
	int direction;
//	printf("\n\nFonction GestionMemoireCarrefour\n");
//	printf("\nflagChoix 1 = %d",carrefour[indiceCarrefour-1].flagchoix1);
//	printf("\nflagChoix 2 = %d",carrefour[indiceCarrefour-1].flagchoix2);
//	printf("\nflagChoix 3 = %d",carrefour[indiceCarrefour-1].flagchoix3);
	
	// Si toutes les coordonn�es sont explor� � un indice carrefour, la boucle while descand 
	// l'indice jusqu'a qu'une coordonn�e non explor� en memoire soit trouv�
	while( (carrefour[indiceCarrefour-1].flagchoix1 <= 0)&&(carrefour[indiceCarrefour-1].flagchoix2 <= 0)&&(carrefour[indiceCarrefour-1].flagchoix3 <= 0) )
	{
		indiceCarrefour--;
		if(indiceCarrefour == 0)
		{
			break;
		}
	}
//	printf("\nindice carrefoure = %d",indiceCarrefour);
	// Prends les coordonn�es d'un flag encore egale � 1. Puis le passe � 0.
	if(indiceCarrefour > 0)
	{
//		printf("\nTEST OK");
		if(carrefour[indiceCarrefour-1].flagchoix1 == 1)
		{
			carrefour[indiceCarrefour-1].flagchoix1 = 0;
			value.x = carrefour[indiceCarrefour-1].xchoix1;
			value.y = carrefour[indiceCarrefour-1].ychoix1;
			direction = carrefour[indiceCarrefour-1].directionChoix1;
			memoire = 1;
		}
		else if(carrefour[indiceCarrefour-1].flagchoix2 == 1)
		{
			carrefour[indiceCarrefour-1].flagchoix2 = 0;
			value.x = carrefour[indiceCarrefour-1].xchoix2;
			value.y = carrefour[indiceCarrefour-1].ychoix2;
			direction = carrefour[indiceCarrefour-1].directionChoix2;
			memoire = 1;
		}
		else
		{
			carrefour[indiceCarrefour-1].flagchoix3 = 0;
			value.x = carrefour[indiceCarrefour-1].xchoix3;
			value.y = carrefour[indiceCarrefour-1].ychoix3;
			direction = carrefour[indiceCarrefour-1].directionChoix3;
			memoire = 1;
		}
		
		indiceMemo = indiceChemin;
		indiceChemin = carrefour[indiceCarrefour-1].marqueurCarrefour;// r�cupere le num�ro de 
		// l'indice chemin sauvegard� � ce carrefour
//		printf("\nmarqueur Carrefour = %d",carrefour[indiceCarrefour-1].marqueurCarrefour);
		
		// R�initialise les coordonn�es ne formant pas une zone � -1 pour �vit� les confusions
		for(i = indiceChemin ; i < indiceMemo; i++)
		{
			chemin[i].x= -1;
			chemin[i].y= -1;
			chemin[i].direction= -1;
		}
		// Implante les nouvelles coordonn�es dans chemin
		chemin[indiceChemin].direction = direction;	
		chemin[indiceChemin].x = value.x;
		chemin[indiceChemin].y = value.y;
		indiceChemin ++;
//		printf("\nChemin[%d].x = %d",indiceChemin-1,chemin[indiceChemin-1].x);
//		printf("\nChemin[%d].y = %d",indiceChemin-1,chemin[indiceChemin-1].y);
//		printf("\nChemin[%d].direction = %d",indiceChemin-1,chemin[indiceChemin-1].direction);
//		printf("\nIndice chemin = %d",indiceChemin);
//		printf("\nValue x = %d\nValue y =%d",value.x,value.y);
//		printf("\n\n");
	}
	else
	{
		memoire = 0; /// retrourne que la m�moire est vide, donc plus de possibilit� de form� un zone.
	}
	return memoire;	
}

/*
*	Fonction qui est appel� si plus de coordonn�es trouv� sur le chemin
*	Si l'indice memoire est egale � 0, alors plus de chemin possible
*/
// Page 31
int RechercheMemoire(void)
{
	int memoire;
	if(indiceCarrefour == 0)
	{
		value.x = -1;
		value.y = -1;
		memoire = 0;
	}
	else
	{
		memoire = GestionMemoireCarrefour();// Page 32
	}
	return memoire;
}

/*
*	Cette fonction est appel� qu'en une seule coordonn�e est possible pour avanc� dans le chemin.
*	Elle les coordonn�es, puis les enregistres.
*/
// Page 25
void Avance(void)
{
	int var;
	var = RechercheCoordChoix();// Page 26
	
	if(var == 1)
	{
		MemoireChemin();// Page 33
	}
	else
	{
		value.x = -1;
		value.y = -1;
	}
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 4
//-------------------------------------------------------------------------------------------------------------
/*
*	Ces 4 fonctions ci dessous sauvegarde tout les murs presents � la rencontre d'un carrefour.
*/

/*
*	 Envoie des coordonn�es pour suivre un chemin, suite � une rencontre avec un carrefour
*/
// Page 42
void CoordChoixEnvoie(void){ //
	if(carrefour[indiceCarrefour].flagchoix1 == 1){
		carrefour[indiceCarrefour].flagchoix1 =0;
		value.x = carrefour[indiceCarrefour].xchoix1;
		value.y = carrefour[indiceCarrefour].ychoix1;
		chemin[indiceChemin].direction = carrefour[indiceCarrefour].directionChoix1;
	}
	else if(carrefour[indiceCarrefour].flagchoix2 == 1){
		carrefour[indiceCarrefour].flagchoix2 =0;
		value.x = carrefour[indiceCarrefour].xchoix2;
		value.y = carrefour[indiceCarrefour].ychoix2;
		chemin[indiceChemin].direction = carrefour[indiceCarrefour].directionChoix2;
	}
	else if(carrefour[indiceCarrefour].flagchoix3 == 1){
		carrefour[indiceCarrefour].flagchoix3 =0;
		value.x = carrefour[indiceCarrefour].xchoix3;
		value.y = carrefour[indiceCarrefour].ychoix3;
		chemin[indiceChemin].direction = carrefour[indiceCarrefour].directionChoix3;
	}
//	printf("\nindice carrefour -> %d",indiceCarrefour);
//	printf("\ndirection -> %d",chemin[indiceChemin].direction);
}

/*
*	Les donn�es de chaque orientation possible,selon la direction et le sens, au carrefour sont enregistr�es.
*/
// Page 37
void EnregistrementChoixHaut(void){
 	if(plateau[value.y-1][value.x+1].mur == 1 && (value.y-1>=0 && value.x+1<= 16)){ // Si mur droite haut existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix1 = value.x+1;
 		carrefour[indiceCarrefour].ychoix1 = value.y-1;
 		carrefour[indiceCarrefour].directionChoix1 = 2;
 		carrefour[indiceCarrefour].flagchoix1 = 1;
	}
	if(plateau[value.y-2][value.x].mur == 1 && (value.y-2>=0)){// Si mur verticale haut existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix2 = value.x;
 		carrefour[indiceCarrefour].ychoix2 = value.y-2;
 		carrefour[indiceCarrefour].directionChoix2 = 1;
 		carrefour[indiceCarrefour].flagchoix2 = 1;
	}
	if(plateau[value.y-1][value.x-1].mur == 1&& (value.y-1>=0 && value.x-1>= 0)){// Si mur gauche haut existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix3 = value.x-1;
 		carrefour[indiceCarrefour].ychoix3 = value.y-1;
 		carrefour[indiceCarrefour].directionChoix3 = 4;
 		carrefour[indiceCarrefour].flagchoix3 = 1;
	}
	CoordChoixEnvoie(); // Page42
	indiceCarrefour ++;
}

/*
*	Les donn�es de chaque orientation possible,selon la direction et le sens, au carrefour sont enregistr�es.
*/
//Page 38
void EnregistrementChoixBas(void){
 	if(plateau[value.y+1][value.x+1].mur == 1 && (value.y+1<=10 && value.x+1<= 16)){ // Si mur droite bas existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix1 = value.x+1;
 		carrefour[indiceCarrefour].ychoix1 = value.y+1;
 		carrefour[indiceCarrefour].directionChoix1 = 2;
 		carrefour[indiceCarrefour].flagchoix1 = 1;
	}
	if(plateau[value.y+2][value.x].mur == 1&& (value.y+2 <=10)){// Si mur verticale bas existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix2 = value.x;
 		carrefour[indiceCarrefour].ychoix2 = value.y+2;
 		carrefour[indiceCarrefour].directionChoix2 = 3;
 		carrefour[indiceCarrefour].flagchoix2 = 1;
	}
	if(plateau[value.y+1][value.x-1].mur == 1&& (value.y+1<=10 && value.x-1>= 0)){// Si mur gauche bas existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix3 = value.x-1;
 		carrefour[indiceCarrefour].ychoix3 = value.y+1;
 		carrefour[indiceCarrefour].directionChoix3 = 4;
 		carrefour[indiceCarrefour].flagchoix3 = 1;
	}
	CoordChoixEnvoie(); // Page42
	indiceCarrefour ++;
}

/*
*	Les donn�es de chaque orientation possible,selon la direction et le sens, au carrefour sont enregistr�es.
*/
//Page 39
void EnregistrementChoixDroit(void){
 	if(plateau[value.y+1][value.x+1].mur == 1&& (value.y+1<=10 && value.x+1<= 16)){ // Si mur bas droite existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix1 = value.x+1;
 		carrefour[indiceCarrefour].ychoix1 = value.y+1;
 		carrefour[indiceCarrefour].directionChoix1 = 3;
 		carrefour[indiceCarrefour].flagchoix1 = 1;
	}
	if(plateau[value.y][value.x+2].mur == 1 && (value.x+2<17)){// Si mur horizontal droite existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix2 = value.x+2;
 		carrefour[indiceCarrefour].ychoix2 = value.y;
 		carrefour[indiceCarrefour].directionChoix2 = 2;
 		carrefour[indiceCarrefour].flagchoix2 = 1;
	}
	if(plateau[value.y-1][value.x+1].mur == 1&& (value.y-1>=0 && value.x+1<= 16)){// Si mur haut droite existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix3 = value.x+1;
 		carrefour[indiceCarrefour].ychoix3 = value.y-1;
 		carrefour[indiceCarrefour].directionChoix3 = 1;
 		carrefour[indiceCarrefour].flagchoix3 = 1;
	}
	CoordChoixEnvoie(); // Page42
	indiceCarrefour ++;
} 

/*
*	Les donn�es de chaque orientation possible,selon la direction et le sens, au carrefour sont enregistr�es.
*/
// Page 40
void EnregistrementChoixGauche(void){
 	if(plateau[value.y+1][value.x-1].mur == 1&& (value.y+1<=10 && value.x-1>= 0)){ // Si mur bas gauche existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix1 = value.x-1;
 		carrefour[indiceCarrefour].ychoix1 = value.y+1;
 		carrefour[indiceCarrefour].directionChoix1 = 3;
 		carrefour[indiceCarrefour].flagchoix1 = 1;
	}
	if(plateau[value.y][value.x - 2].mur == 1&& (value.x-2>= 0)){// Si mur horizontal gauche existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix2 = value.x-2;
 		carrefour[indiceCarrefour].ychoix2 = value.y;
 		carrefour[indiceCarrefour].directionChoix2 = 4;
 		carrefour[indiceCarrefour].flagchoix2 = 1;
	}
	if(plateau[value.y-1][value.x-1].mur == 1&& (value.y-1>=0 && value.x-1>= 0)){// Si mur haut gauche existe, on enregistre les donn�es
 		carrefour[indiceCarrefour].xchoix3 = value.x-1;
 		carrefour[indiceCarrefour].ychoix3 = value.y-1;
 		carrefour[indiceCarrefour].directionChoix3 = 1;
 		carrefour[indiceCarrefour].flagchoix3 = 1;
	}
	
	CoordChoixEnvoie(); // Page42
	indiceCarrefour ++;
}

/*
*	Fonction qui est appel� suite � la rencontre d'un carrefour, pour sauvegarder toutes les possibilit�es
*	de formation de zone.
*/
// Page 35
void MemoireCarrefour(void){
	carrefour[indiceCarrefour].marqueurCarrefour = indiceChemin;
	if((value.x%2 == 1) && (value.y%2 == 0)) // Si mur horizontal
	{
		if(chemin[indiceChemin-1].direction ==4){ // Si direction gauche
			EnregistrementChoixGauche(); // Page 40
		}
		else{ // Si direction droite
			EnregistrementChoixDroit(); // Page 39
		}
	}
	else{ // Si mur verticale
		if(chemin[indiceChemin-1].direction == 3){ // Si direction bas
			EnregistrementChoixBas(); // Page 38
		}
		else{
			EnregistrementChoixHaut(); // Page 37
		}
	}
	chemin[indiceChemin].x=value.x;
	chemin[indiceChemin].y=value.y;
	indiceChemin++;
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 5
//-------------------------------------------------------------------------------------------------------------

/*
*	fonction qui controle si plusieurs murs sont disponibles.
*	Si plusieurs mur, elle sauvegarde de chaque mur est en choisi un au hazard.
*	Sinon, elle avance simple au coordonn�e disponible
*/
// Page 24
void RechercheNouvCoord(void){
	int var;
	var = RechercheSiPlusieurChoixPossible(); // Page 17, PARTIE 2
//	printf("\nvar = %d",var);
	if(var > 1){
		MemoireCarrefour(); // Page 35, PARTiE 3
//		printf("\nflag choix 1 = %d",carrefour[indiceCarrefour-1].flagchoix1);
//		printf("\nx choix 1 = %d",carrefour[indiceCarrefour-1].xchoix1);
//		printf("\ny choix 1 = %d",carrefour[indiceCarrefour-1].ychoix1);
//		printf("\ndirection 1 = %d",carrefour[indiceCarrefour-1].directionChoix1);
//		printf("\nflag choix 2 = %d",carrefour[indiceCarrefour-1].flagchoix2);
//		printf("\nx choix 2 = %d",carrefour[indiceCarrefour-1].xchoix2);
//		printf("\ny choix 2 = %d",carrefour[indiceCarrefour-1].ychoix2);
//		printf("\ndirection 2 = %d",carrefour[indiceCarrefour-1].directionChoix2);
//		printf("\nflag choix 3 = %d",carrefour[indiceCarrefour-1].flagchoix3);
//		printf("\nx choix 3 = %d",carrefour[indiceCarrefour-1].xchoix3);
//		printf("\ny choix 3 = %d",carrefour[indiceCarrefour-1].ychoix3);
//		printf("\ndirection 3 = %d",carrefour[indiceCarrefour-1].directionChoix3);
//		printf("\nIndice Carrrefour = %d",indiceCarrefour);
//		printf("\nValue x = %d\nValue y =%d",value.x,value.y);
//		printf("\nChemin[%d].x = %d",indiceChemin-1,chemin[indiceChemin-1].x);
//		printf("\nChemin[%d].y = %d",indiceChemin-1,chemin[indiceChemin-1].y);
//		printf("\nChemin[%d].direction = %d",indiceChemin-1,chemin[indiceChemin-1].direction);
//		printf("\nIndice chemin = %d",indiceChemin);
//		printf("\nValue x = %d\nValue y =%d",value.x,value.y);
//		printf("\n\n");
	}
	else
	{
		Avance(); // Page 25, PARTIE 4
				
//		printf("\nChemin[%d].x = %d",indiceChemin-1,chemin[indiceChemin-1].x);
//		printf("\nChemin[%d].y = %d",indiceChemin-1,chemin[indiceChemin-1].y);
//		printf("\nChemin[%d].direction = %d",indiceChemin-1,chemin[indiceChemin-1].direction);
//		printf("\nIndice chemin = %d",indiceChemin);
//		printf("\nValue x = %d\nValue y =%d",value.x,value.y);
//		printf("\n\n");
	}
} 

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 6
//-------------------------------------------------------------------------------------------------------------

/*
*	Fonction d'initialisation des coordonn�es de l'origine.
*/
// Page 34
void InitMemoirCheminOrigine (void){
	if((value.x%2 == 1) && (value.y%2 == 0)){ // Si mur horizontal
		chemin[indiceChemin].direction = 4; // direction = gauche
	}
	else{	
		chemin[indiceChemin].direction = 3; // direction = bas
	}
	chemin[indiceChemin].x = value.x;
	chemin[indiceChemin].y = value.y;	
	indiceChemin ++;
///	printf("\nORIGINE");
//	printf("\nChemin[%d].x = %d",indiceChemin-1,chemin[indiceChemin-1].x);
//	printf("\nChemin[%d].y = %d",indiceChemin-1,chemin[indiceChemin-1].y);
//	printf("\nChemin[%d].direction = %d",indiceChemin-1,chemin[indiceChemin-1].direction);
//	printf("\nIndice chemin = %d",indiceChemin);
//	printf("\nValue x = %d\nValue y =%d",value.x,value.y);
//	printf("\n\n");
} 

/*
*	Fonction qui contr�le si la zone de 4 cases est autoris�
*/
// Page 42
_Bool ZoneOK(){
	// pour une zone de 4 cases, si 2 coord se suit, alors il y a 2 fois la coord x ou y.
	// Pour former une zone autoris�, il faut 4 fois une r�p�tition de coord.
	// Si ce n'est pas le cas, la zone n'est pas autoris�.
	int compteur = 0;
	int i;
	int xMin,xMax,yMin,yMax;
	
	xMin=chemin[0].x;
	xMax=chemin[0].x;
	yMin=chemin[0].y;
	yMax=chemin[0].y;
	
//	printf("\nTest Zone OK\n");
	// touve le min max des coordonn�es x et y
	for(i=0;i<indiceChemin;i++)
	{
		if(chemin[i].x<xMin)
		{
			xMin = chemin[i].x;
		}
		if(chemin[i].y<yMin)
		{
			yMin = chemin[i].y;
		}
		if(chemin[i].x>xMax)
		{
			xMax = chemin[i].x;
		}
		if(chemin[i].y>yMax)
		{
			yMax = chemin[i].y;
		}
	}
	
//	printf("\nxMax = %d\nxMin = %d",xMax,xMin);
//	printf("\nyMax = %d\nyMin = %d",yMax,yMin);
	
	/*
	*	Si la zone est carr�
	*	les coordonn�es de chaque mur doivent se repet� au moins 2 fois
	*	Si c'est le cas, le carr� est parfait
	*	Sinon, la zone a moins de 4 cases
	*/
	if(	(xMax-xMin == yMax-yMin)	) // Carre
	{
//		printf("\nZone carre");
		for(i = 0 ; i < indiceChemin ; i++){
			if(chemin[i].x == chemin[i+1].x){
				compteur++;
			}
			if(chemin[i].y == chemin[i+1].y){
				compteur++;
			}
		}
	
		if(chemin[indiceChemin].x == chemin[0].x){
			compteur++;
		}
		if(chemin[indiceChemin].y == chemin[0].y){
			compteur++;
		}
		
		if(compteur == 4)
		{
//			printf("\nMur valide");
			return true;
		}
		else{
//			printf("\nMur non valide");
			return false;
		}
	}
	
	/*
	*	Zone rectangle
	*	Meme systeme que pour la zone carr�e
	*	Si la r�p�tission de 6 murs sont detect�
	*	Alors la zone est valide
	*	Sinon, zone de moins de 4 cases
	*/
	else //if(	(xMax-xMin == 2) || (yMax-yMin == 2)	) // Carre
	{
//		printf("\nZone rectangle");
		
		
		for(i = 0 ; i < indiceChemin ; i++){
			if(chemin[i].x == chemin[i+1].x){
				compteur++;
			}
			if(chemin[i].y == chemin[i+1].y){
				compteur++;
			}
		}
	
		if(chemin[indiceChemin].x == chemin[0].x){
			compteur++;
		}
		if(chemin[indiceChemin].y == chemin[0].y){
			compteur++;
		}
		
		
		if(compteur == 6)
		{
//			printf("\nMur valide");
			return true;
		}
		else{
//			printf("\nMur non valide");
			return false;
		}
	}
/*	else
	{
		printf("\nErreur : forme de zone non d�termin�\n");
	}*/
	


} 

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 7
//-------------------------------------------------------------------------------------------------------------

/*
*	Cette fonction est une boucle pour chercher si un zone interdite peut etre form� avec 
*	les coordon�es pass� en parametre
*	Elle appel toutes les fonctions pour gerer les carrefours, la memoire, la recherche de coordonn�es
*	Puis, le controle de la formation potentielle de zone valide ou non
*/
// Page 16
_Bool ControlePotInvalide(int x, int y){
	value.x = x;
	value.y = y;
	int etat = 0;
	int zone;
	int memoire;
	_Bool ret;
	
	do{
		switch(etat){
			case 0: // Initialisation
				etat = 1;
				zone = -1;
				InitMemoireCarrefour();
				InitMemoireChemin();
				InitMemoirCheminOrigine(); // Page 34
				break;
			case 1: // Recherche nouvelles coordonnees
				RechercheNouvCoord();
				if(value.x != -1 && value.y != -1){ // Si nouvelle coord
					etat = 2;
				}
				else{
//					printf("\nPlus de coord");
					etat = 3;
				}
				break;
			case 2: // Verifie si une zone est form�e, sinon recherche de nouvelles coordonn�es
				if( (chemin[indiceChemin-1].x == chemin[0].x) && (chemin[indiceChemin-1].y == chemin[0].y) ){
//					printf("\nRencontre avec origine");
					if(ZoneOK()){ // Page 42
						etat = 3;
					}
					else{
						ret = true;
						zone = 1;
					}
				}
				else{
					if(indiceChemin > 9){// >9 car au dela, la formation d'une zone interdite est impossible
						etat = 3;
					}
					else{
						etat = 1;
					}
				}
				break;
			case 3: // Recherche si il reste des coordonn�es en memoire
//				printf("\nRecheche Memoire\n");
				memoire = RechercheMemoire(); // Page 31
				if(memoire == 1)
				{
					etat = 2;
				}
				else
				{
					zone = 0;
					ret = false; // variable de retour
				}
				break;
		}
	}while(zone == -1);
	
	return ret;
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 8
//-------------------------------------------------------------------------------------------------------------

/*
*	Fonction qui controle si au moins un mur est present de chaque cot� des coordonn�es contr�l�
*	Si oui, alors la fonction appel le cont�le de formation de zone potentielle
*	Sinon, elle renvoie directement qu'aucun zone est potentiellement pr�sente, est dans ce cas,
*	le programme passe aux coordonn�es suivantes
*/
// Page 3
_Bool ControlePotZone(int x, int y){
	if(RechercheMur2Cotes(x,y)){ // Page 4 // Si mur pr�sent des 2 cot�s
		if(ControlePotInvalide(x,y)){ // Page 16 // si zone invalide
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 9
//-------------------------------------------------------------------------------------------------------------

/*
*	Fonction qui est appel� avant chaque mur pos� par le joueur
*	Elle met les flag interdits � 1 si les coordonn�es d'un mur peuvent former une zone interdite
*	Des lors, le joueur ne pourra pas selectionner ce mur pour le poser
*/
// Page 2
void Surbrillance(void)
{
	int x,y;
	_Bool var;
	
	for(y=0;y<11;y++)
	{
		for(x=0;x<17;x++)
		{
			if(	((y%2 == 0) && (x%2 == 1)) || ((y%2 == 1) && (x%2 == 0))	) // Pour chaque coordonn�e correspondante � un mur
			{
				if(plateau[y][x].mur == 0 && plateau[y][x].flag_interdit == 0)// Si mur non poser et mur pas encore controle interdit
				{
//					printf("\n\n********************************\n\tNouveau tour\n");
					plateau[y][x].mur = 1;
					var = ControlePotZone(x,y); // Page 3
					if(var == true){
						plateau[y][x].flag_interdit = 1;
					}
					plateau[y][x].mur = 0;
//					printf("flag = %d\n\n\n\n",	plateau[y][x].flag_interdit);
				}
			}
		}
	}
} 
