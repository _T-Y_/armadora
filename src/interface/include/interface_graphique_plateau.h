#ifndef HEADER_INTERFACE_GRAPHIQUE_PLATEAU
#define HEADER_INTERFACE_GRAPHIQUE_PLATEAU

	int AffichageChoix(int hero, int numero);
	int SelectionnerPion(int numero,int hero);
	void poser_mur(void);
	void Affichage(int surbrillance);
	int poser_jeton(int val, int hero);
	
#endif
