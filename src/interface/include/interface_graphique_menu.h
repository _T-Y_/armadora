#ifndef HEADER_INTERFACE_GRAPHIQUE_MENU
#define HEADER_INTERFACE_GRAPHIQUE_MENU
	
	void AffichageStart(void);
	int AffichageChoixNbJoueur(void);
	void AffichageDataJoueur(int nbJoueur);
	
#endif
