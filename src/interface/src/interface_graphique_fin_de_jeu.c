// Commande pour compiler
// gcc -o out main.c -lSDL
// ou
// gcc -I/usr/include/SDL/ main.c -o out -L/usr/lib -lSDL
// gcc -o out Image.c -lSDL_image -lSDL pour compiler avec des images autres que bmp

#include <stdlib.h>
#include <stdio.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h" /* Inclusion du header de SDL_image (adapter le dossier au besoin) */
//#include<SDL/SDL_getenv.h>
#include <SDL/SDL_ttf.h> //  Son r�le est de cr�er uneSDL_Surface� partir du texte que vous lui envoyez.
//#include "../include/interface_graphique_fin_de_jeu.h"
#include "interface_graphique_fin_de_jeu.h"
#include "variables_globales.h"
#include "interface_graphique_plateau.h"

//____________________________________________________
// Pour Test
/*struct donnees
 {
 int hero;
 int equipe;
 };
 struct donnees joueur[5];*/
//____________________________________________________
void AffichageGraphique(int gagnant) {
	// SDL

	SDL_Surface *ecran = NULL, *Fond = NULL, *texte = NULL, *Elf = NULL, *Mage =
			NULL, *Ork = NULL, *Troll = NULL;
	SDL_Surface *texteJ1 = NULL, *texteJ2 = NULL, *texteJ3 = NULL, *texteE1 =
			NULL, *texteE2 = NULL;

	SDL_Rect positionHero1;
	SDL_Rect positionHero2;

	SDL_Rect positionGagnant;
	positionGagnant.x = 40;
	positionGagnant.y = 400;

	SDL_Rect positionFond;
	positionFond.x = 0;
	positionFond.y = 0;

	SDL_Rect positionVictoire;
	positionVictoire.x = 260;
	positionVictoire.y = 50;

	SDL_Init(SDL_INIT_VIDEO);
	ecran = SDL_SetVideoMode(750, 500, 32, SDL_HWSURFACE | SDL_DOUBLEBUF); /* Double Buffering */

	SDL_WM_SetCaption("Armadora : Fin de partie", NULL);
	SDL_WM_SetIcon(IMG_Load("src/interface/image/image_fin_de_jeu/icone.bmp"),
			NULL); // Icon

	Fond = IMG_Load("src/interface/image/image_fin_de_jeu/Fond.bmp");
	Elf = IMG_Load("src/interface/image/image_fin_de_jeu/Elf.png");
	Mage = IMG_Load("src/interface/image/image_fin_de_jeu/Mage.png");
	Ork = IMG_Load("src/interface/image/image_fin_de_jeu/Ork.png");
	Troll = IMG_Load("src/interface/image/image_fin_de_jeu/Troll.png");
	SDL_BlitSurface(Fond, NULL, ecran, &positionFond);

	// TTF

	if (TTF_Init() == -1) {
		fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n",
				TTF_GetError());
		exit(EXIT_FAILURE);
	}

	TTF_Font *police = NULL, *J1 = NULL, *J2 = NULL, *J3 = NULL, *E1 = NULL,
			*E2 = NULL;
	/* Chargement de la police */
	police = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 65);
	J1 = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 200);
	J2 = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 25);
	J3 = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 25);
	E1 = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 25);
	E2 = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 25);
	if (!police) {
		printf("\nERREUR : chargement police\n");
	}
	/* Couleur du texte  */
	SDL_Color couleurNoire = { 0, 0, 0 };
	/* �criture du texte dans la SDL_Surface texte en mode Blended (optimal) */
	texte = TTF_RenderText_Solid(police, "Victoire", couleurNoire);
	texteJ1 = TTF_RenderText_Solid(police, "Joueur 1 gagne la partie",
			couleurNoire);
	texteJ2 = TTF_RenderText_Solid(police, "Joueur 2 gagne la partie",
			couleurNoire);
	texteJ3 = TTF_RenderText_Solid(police, "Joueur 3 gagne la partie",
			couleurNoire);
	texteE1 = TTF_RenderText_Solid(police, "Equipe 1 gagne la partie",
			couleurNoire);
	texteE2 = TTF_RenderText_Solid(police, "Equipe 2 gagne la partie",
			couleurNoire);

	SDL_BlitSurface(texte, NULL, ecran, &positionVictoire); /* Blit du texte */

	//____________________________________________________

	// Pour Test
	/*
	 joueur[1].hero = 1;
	 joueur[1].equipe = 1;
	 joueur[2].hero = 2;
	 joueur[2].equipe = 1;
	 joueur[3].hero = 3;
	 joueur[3].equipe = 2;
	 joueur[4].hero = 4;
	 joueur[4].equipe = 2;
	 // 1=Mage, 2=Ork, 3=Troll, 4=Elf
	 int nombreDeJoueur = 3;
	 */
	//____________________________________________________
	int transparance = 160;

	if (nombreDeJoueur == 4) {
		positionHero1.x = 100;
		positionHero1.y = 150;
		positionHero2.x = 450;
		positionHero2.y = 150;

		int bloc = 0;
		int i;

		for (i = 1; i < 5; i++) {
			if (joueur[i].equipe == gagnant) {
				if (joueur[i].hero == 1 && bloc == 0) {
					SDL_SetAlpha(Mage, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(Mage, NULL, ecran, &positionHero1);
					//	SDL_Flip(ecran);
					bloc = 1;
				}
				if (joueur[i].hero == 2 && bloc == 0) {
					SDL_SetAlpha(Ork, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(Ork, NULL, ecran, &positionHero1);
					bloc = 1;
				}
				if (joueur[i].hero == 3 && bloc == 0) {
					SDL_SetAlpha(Troll, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(Troll, NULL, ecran, &positionHero1);
					bloc = 1;
				}
				if (joueur[i].hero == 4 && bloc == 0) {
					SDL_SetAlpha(Elf, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(Elf, NULL, ecran, &positionHero1);
					bloc = 1;
				}

				if (joueur[i].hero == 1 && bloc == 1) {
					SDL_SetAlpha(Mage, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(Mage, NULL, ecran, &positionHero2);
					bloc = 1;
				}
				if (joueur[i].hero == 2 && bloc == 1) {
					SDL_SetAlpha(Ork, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(Ork, NULL, ecran, &positionHero2);
					bloc = 1;
				}
				if (joueur[i].hero == 3 && bloc == 1) {
					SDL_SetAlpha(Troll, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(Troll, NULL, ecran, &positionHero2);
					bloc = 1;
				}
				if (joueur[i].hero == 4 && bloc == 1) {
					SDL_SetAlpha(Elf, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(Elf, NULL, ecran, &positionHero2);
					bloc = 1;
				}
			}
		}
		if (gagnant == 1) {
			SDL_BlitSurface(texteE1, NULL, ecran, &positionGagnant); /* Blit du texte */
		}
		if (gagnant == 2) {
			SDL_BlitSurface(texteE2, NULL, ecran, &positionGagnant); /* Blit du texte */
		}
	} else {
		positionHero1.x = 270;
		positionHero1.y = 150;

		if (gagnant == 1) // Joueur 1
				{
			if (joueur[1].hero == 1) {
				SDL_SetAlpha(Mage, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Mage, NULL, ecran, &positionHero1);
			}
			if (joueur[1].hero == 2) {
				SDL_SetAlpha(Ork, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Ork, NULL, ecran, &positionHero1);
			}
			if (joueur[1].hero == 3) {
				SDL_SetAlpha(Troll, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Troll, NULL, ecran, &positionHero1);
			}
			if (joueur[1].hero == 4) {
				SDL_SetAlpha(Elf, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Elf, NULL, ecran, &positionHero1);
			}
			SDL_BlitSurface(texteJ1, NULL, ecran, &positionGagnant); /* Blit du texte */
		}
		if (gagnant == 2) // Joueur 2
				{
			if (joueur[2].hero == 1) {
				SDL_SetAlpha(Mage, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Mage, NULL, ecran, &positionHero1);
			}
			if (joueur[2].hero == 2) {
				SDL_SetAlpha(Ork, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Ork, NULL, ecran, &positionHero1);
			}
			if (joueur[2].hero == 3) {
				SDL_SetAlpha(Troll, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Troll, NULL, ecran, &positionHero1);
			}
			if (joueur[2].hero == 4) {
				SDL_SetAlpha(Elf, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Elf, NULL, ecran, &positionHero1);
			}
			SDL_BlitSurface(texteJ2, NULL, ecran, &positionGagnant); /* Blit du texte */
		}
		if (gagnant == 3) // Joueur 3
				{
			if (joueur[3].hero == 1) {
				SDL_SetAlpha(Mage, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Mage, NULL, ecran, &positionHero1);
			}
			if (joueur[3].hero == 2) {
				SDL_SetAlpha(Ork, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Ork, NULL, ecran, &positionHero1);
			}
			if (joueur[3].hero == 3) {
				SDL_SetAlpha(Troll, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Troll, NULL, ecran, &positionHero1);
			}
			if (joueur[3].hero == 4) {
				SDL_SetAlpha(Elf, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(Elf, NULL, ecran, &positionHero1);
			}
			SDL_BlitSurface(texteJ3, NULL, ecran, &positionGagnant); /* Blit du texte */
		}
	}
	SDL_Flip(ecran);
	pause();

	TTF_CloseFont(police); /* Doit �tre avant TTF_Quit() */
	TTF_Quit();

	SDL_FreeSurface(Fond); /* On lib�re la surface */
	SDL_FreeSurface(Mage);
	SDL_Quit();
}

void VisualisationPoints(void) {
	SDL_Surface *ecran = NULL, *un = NULL, *deux = NULL, *trois = NULL,
			*quatre = NULL, *cinq = NULL, *fond = NULL, *suite = NULL;

	SDL_Event event;

	SDL_Rect position;
	SDL_Rect position1;
	SDL_Rect position2;
	SDL_Rect position3;
	SDL_Rect position4;
	SDL_Rect position5;
	SDL_Rect positionSuite;

	TTF_Font *police = NULL, *policeTemps = NULL;
	SDL_Color couleurNoire = { 0, 0, 0 };
	int continuer = 1;

	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();

	ecran = SDL_SetVideoMode(900, 684, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
	SDL_WM_SetCaption("Armadora", NULL);
	police = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 65);
	/*initialisation des affichages ttf */
	un = TTF_RenderText_Solid(police, "1", couleurNoire);
	deux = TTF_RenderText_Solid(police, "2", couleurNoire);
	trois = TTF_RenderText_Solid(police, "3", couleurNoire);
	quatre = TTF_RenderText_Solid(police, "4", couleurNoire);
	cinq = TTF_RenderText_Solid(police, "5", couleurNoire);
	suite = TTF_RenderText_Solid(police, "Suite", couleurNoire);

	Affichage(0);
	int i = 0;
	int x;
	int y;
	position.y = 145;

	for (y = 0; y < 10; y++) {
		position.x = 30;
		for (x = 0; x < 16; x++) {
			if (x % 2 == 1 && y % 2 == 1) {
				i++;
				if (plateau[y][x].valeurJeton != 0) {
					if (plateau[y][x].valeurJeton == 1) {
						SDL_BlitSurface(un, NULL, ecran, &position); /* Blit du texte */
						SDL_Flip(ecran);
					}
					if (plateau[y][x].valeurJeton == 2) {
						SDL_BlitSurface(deux, NULL, ecran, &position); /* Blit du texte */
						SDL_Flip(ecran);
					}
					if (plateau[y][x].valeurJeton == 3) {
						SDL_BlitSurface(trois, NULL, ecran, &position); /* Blit du texte */
						SDL_Flip(ecran);
					}
					if (plateau[y][x].valeurJeton == 4) {
						SDL_BlitSurface(quatre, NULL, ecran, &position); /* Blit du texte */
						SDL_Flip(ecran);
					}
					if (plateau[y][x].valeurJeton == 5) {
						SDL_BlitSurface(cinq, NULL, ecran, &position); /* Blit du texte */
						SDL_Flip(ecran);
					}
				}
			}
			if (x % 2 == 1 && y % 2 == 1) {
				position.x += 113;
			}

		}
		if (y % 2 == 1) {
			position.y += 110;
		}
	}
	positionSuite.x = 0;
	positionSuite.y = 0;
	SDL_BlitSurface(suite, NULL, ecran, &positionSuite); /* Blit du texte */
	SDL_Flip(ecran);
	int bloc = 1;

	while (continuer) {

		SDL_WaitEvent(&event);

		if (bloc == 0) {
			police = TTF_OpenFont("src/interface/fonts/Subscribe.ttf", 65);
			suite = TTF_RenderText_Solid(police, "Suite", couleurNoire);
			SDL_BlitSurface(suite, NULL, ecran, &positionSuite); /* Blit du texte */
			SDL_Flip(ecran);
			continuer = 0;
			SDL_Delay(1000);
		}

		switch (event.type) {
		case SDL_QUIT:
			continuer = 0;
			SDL_Quit();
			exit(1);
			break;
			//posage de jeton sur les cases choisi en fontion des cliques
		case SDL_MOUSEBUTTONUP:
			if (event.button.button == SDL_BUTTON_LEFT) {
				if ((event.button.x > positionSuite.x)
						&& (event.button.x < positionSuite.x + 200)
						&& (event.button.y > positionSuite.y)
						&& (event.button.y < positionSuite.y + 50)) {
					bloc = 0;
					break;
				}
			}
		}
	}

}

void pause() {
	int continuer = 1;
	SDL_Event event;

	while (continuer) {
		SDL_WaitEvent(&event);
		switch (event.type) {
		case SDL_QUIT:
			continuer = 0;
			break;
		}
	}
}
