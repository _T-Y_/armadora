#include "interface_graphique_plateau.h"
#include <stdio.h>
#include <stdlib.h>
#include<SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "SDL/SDL_image.h"
#include "variables_globales.h"

int AffichageChoix(int hero, int numero) {
	SDL_Surface *ecran = NULL, *texte1 = NULL, *texte2 = NULL, *texte3 = NULL,
			*fond = NULL;
	SDL_Rect positiontexte1, positiontexte2, positiontexte3, position;
	SDL_Event event;
	int choix, i;
	TTF_Font *police = NULL, *policeChoix = NULL;
	SDL_Color couleurNoire = { 0, 0, 0 };
	int continuer = 1;
	int ressource = 0;
	for (i = 1; i <= 6; i++) {
		if (joueur[numero].source[i] > 0) {
			ressource++;
		}
	}

	// Timer

	SDL_Surface *texteTimer = NULL;
	SDL_Rect positionTimer;
	int tempsActuel = 0, tempsPrecedent = 0, compteur = 60;
	TEMPS = compteur; // variable global temps
	char temps[40] = ""; /* Tableau de char suffisamment grand */

	// Init

	SDL_Init(SDL_INIT_VIDEO);

	// Affichage

	if (TTF_Init() == -1) {
		fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n",
				TTF_GetError());
		exit(EXIT_FAILURE);
	}

	ecran = SDL_SetVideoMode(900, 684, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
	SDL_WM_SetCaption("Armadora", NULL);
	if (hero == 1) {
		fond = IMG_Load("src/interface/image/plateau/Mage.bmp");
	}
	if (hero == 2) {
		fond = IMG_Load("src/interface/image/plateau/Ork.bmp");
	}

	if (hero == 3) {
		fond = IMG_Load("src/interface/image/plateau/Troll.bmp");
	}

	if (hero == 4) {
		fond = IMG_Load("src/interface/image/plateau/Elf.bmp");
	}

	/* Chargement de la police */
	police = TTF_OpenFont("src/interface/fonts/Subscribe.ttf", 65);
	if (!police) {
		printf("\nERREUR : chargement police\n\tAffichageChoix.c\n");
	}
	policeChoix = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 65);
	if (!policeChoix) {
		printf("\nERREUR : chargement police\n\tAffichageChoix.c\n");
	}

	/* Écriture du texte dans la SDL_Surface texte en mode Blended (optimal) */
	texte1 = TTF_RenderText_Blended(police, "Choisissez une action",
			couleurNoire);
	texte2 = TTF_RenderText_Blended(policeChoix, "Poser deux murs",
			couleurNoire);
	texte3 = TTF_RenderText_Blended(policeChoix, "Poser un pion ",
			couleurNoire);

	position.x = 0;
	position.y = 0;
	positiontexte1.x = 10;
	positiontexte1.y = 200;
	positiontexte2.x = 20;
	positiontexte2.y = 350;
	positiontexte3.x = 20;
	positiontexte3.y = 450;

	int bloc = 1;
	int affichage = 1;

	tempsActuel = SDL_GetTicks();
	int bloctemps = 0;

	while (continuer) {

		// TIMER

		tempsActuel = SDL_GetTicks();
		if (tempsActuel - tempsPrecedent >= 1000) /* Si 1000 ms au moins se sont coules */
		{
			affichage = 1;
			tempsPrecedent = tempsActuel;
			bloctemps = 1;
		}
		if (compteur == 0) {
			continuer = 0;
			choix = 0;
			bloctemps = 1;
			//  compteur = 20;
		}

		// AFFICHAGE

		if (affichage == 1) {
			SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
			SDL_BlitSurface(fond, NULL, ecran, &position); /* Blit du fond */

			if (bloc == 1) {
				SDL_BlitSurface(texte2, NULL, ecran, &positiontexte2); /* Blit du texte */
				SDL_Flip(ecran);
				if (ressource > 0) {
					SDL_BlitSurface(texte3, NULL, ecran, &positiontexte3); /* Blit du texte */
					SDL_Flip(ecran);
				}
			} else {
				if (choix == 1) {
					texte2 = TTF_RenderText_Blended(police, "Poser deux murs",
							couleurNoire);
					SDL_BlitSurface(texte2, NULL, ecran, &positiontexte2); /* Blit du texte */
					SDL_Flip(ecran);
					if (ressource > 0) {
						SDL_BlitSurface(texte3, NULL, ecran, &positiontexte3); /* Blit du texte */
						SDL_Flip(ecran);
					}
					continuer = 0;
					SDL_Delay(1000);
				} else {
					texte3 = TTF_RenderText_Blended(police, "Poser un pion ",
							couleurNoire);
					SDL_BlitSurface(texte2, NULL, ecran, &positiontexte2); /* Blit du texte */
					SDL_Flip(ecran);
					SDL_BlitSurface(texte3, NULL, ecran, &positiontexte3); /* Blit du texte */
					SDL_Flip(ecran);
					continuer = 0;
					SDL_Delay(1000);
				}
			}
			if (bloctemps == 1) {
				TEMPS = compteur;
				sprintf(temps, "Temps : %d s", compteur); /* On crit dans la chane "temps" le nouveau temps */
				SDL_FreeSurface(texteTimer); /* On supprime la surface prcdente */
				texteTimer = TTF_RenderText_Blended(police, temps,
						couleurNoire); /* On crit la chane temps dans la SDL_Surface */
				tempsPrecedent = tempsActuel; /* On met  jour le tempsPrecedent */
				positionTimer.x = 20;
				positionTimer.y = 620;
				SDL_BlitSurface(texteTimer, NULL, ecran, &positionTimer); /* Blit du texte */
				SDL_Flip(ecran);
				bloctemps = 0;
				compteur -= 1; /* On rajoute 100 ms au compteur */
			}
			affichage = 0;
		}

		SDL_PollEvent(&event);
		switch (event.type) {
		case SDL_QUIT:
			TTF_CloseFont(police);
			TTF_Quit();
			SDL_FreeSurface(texte1);
			SDL_FreeSurface(texte2);
			SDL_FreeSurface(texte3);
			SDL_Quit();

			continuer = 0;
			exit(1);
			break;

// definition du choix de jeu
		case SDL_MOUSEBUTTONUP:
			if (event.button.button == SDL_BUTTON_LEFT) {
				if ((event.button.x > positiontexte2.x)
						&& (event.button.x < positiontexte2.x + 500)
						&& (event.button.y > positiontexte2.y)
						&& (event.button.y < positiontexte2.y + 70)) {
					choix = 1;
					bloc = 0;
					affichage = 1;
					break;

				}
				if ((event.button.x > positiontexte3.x)
						&& (event.button.x < positiontexte3.x + 500)
						&& (event.button.y > positiontexte3.y)
						&& (event.button.y < positiontexte3.y + 70)
						&& (ressource > 0)) {
					choix = 2;
					bloc = 0;
					affichage = 1;
					break;
				}
			}
		}
	}

	TTF_CloseFont(police);
	//   TTF_Quit();

	SDL_FreeSurface(texte1);
	SDL_FreeSurface(texte2);
	SDL_FreeSurface(texte3);
	//   SDL_Quit();

	return choix;

}

int SelectionnerPion(int numero, int hero) {
	SDL_Surface *ecran = NULL, *un = NULL, *deux = NULL, *trois = NULL,
			*quatre = NULL, *cinq = NULL, *six = NULL, *fond = NULL;
	SDL_Surface *message1 = NULL, *message2 = NULL, *message3 = NULL,
			*message4 = NULL, *message5 = NULL, *message6 = NULL;
	SDL_Rect position;
	SDL_Rect position1;
	SDL_Rect position2;
	SDL_Rect position3;
	SDL_Rect position4;
	SDL_Rect position5;
	SDL_Rect position6;
	SDL_Rect positionMessage;
	SDL_Event event;
	int val;
	TTF_Font *police = NULL, *policeTemps = NULL;
	SDL_Color couleurNoire = { 0, 0, 0 };
	int continuer = 1;
	char texte1[100], texte2[100], texte3[100], texte4[100], texte5[100],
			texte6[100];

	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();

	ecran = SDL_SetVideoMode(900, 684, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
	SDL_WM_SetCaption("Armadora", NULL);

	//fond = SDL_LoadBMP("src/interface/image/plateau/choix.bmp");
	if (hero == 1) {
		fond = IMG_Load("src/interface/image/plateau/Mage.bmp");
	}
	if (hero == 2) {
		fond = IMG_Load("src/interface/image/plateau/Ork.bmp");
	}

	if (hero == 3) {
		fond = IMG_Load("src/interface/image/plateau/Troll.bmp");
	}

	if (hero == 4) {
		fond = IMG_Load("src/interface/image/plateau/Elf.bmp");
	}
	// Timer

	SDL_Surface *texteTimer = NULL;
	SDL_Rect positionTimer;
	int tempsActuel = 0, tempsPrecedent = 0;
	int compteur = TEMPS; // variable global temps
	char temps[40] = ""; /* Tableau de char suffisamment grand */

	/* Chargement de la police */
	police = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 65);
	policeTemps = TTF_OpenFont("src/interface/fonts/Subscribe.ttf", 65);
	/*initialisation des affichages ttf */
	un = TTF_RenderText_Solid(police, "1", couleurNoire);
	deux = TTF_RenderText_Solid(police, "2", couleurNoire);
	trois = TTF_RenderText_Solid(police, "3", couleurNoire);
	quatre = TTF_RenderText_Solid(police, "4", couleurNoire);
	cinq = TTF_RenderText_Solid(police, "5", couleurNoire);
	six = TTF_RenderText_Solid(police, "6", couleurNoire);

	if (joueur[numero].source[1] != 0) {
		sprintf(texte1, "Reste %d pions", joueur[numero].source[1]);
	} else {
		sprintf(texte1, "Vide");
	}
	if (joueur[numero].source[2] != 0) {
		sprintf(texte2, "Reste %d pions", joueur[numero].source[2]);
	} else {
		sprintf(texte2, "Vide");
	}
	if (joueur[numero].source[3] != 0) {
		sprintf(texte3, "Reste %d pions", joueur[numero].source[3]);
	} else {
		sprintf(texte3, "Vide");
	}
	if (joueur[numero].source[4] != 0) {
		sprintf(texte4, "Reste %d pions", joueur[numero].source[4]);
	} else {
		sprintf(texte4, "Vide");
	}
	if (joueur[numero].source[5] != 0) {
		sprintf(texte5, "Reste %d pions", joueur[numero].source[5]);
	} else {
		sprintf(texte5, "Vide");
	}

	message1 = TTF_RenderText_Solid(police, texte1, couleurNoire);
	message2 = TTF_RenderText_Solid(police, texte2, couleurNoire);
	message3 = TTF_RenderText_Solid(police, texte3, couleurNoire);
	message4 = TTF_RenderText_Solid(police, texte4, couleurNoire);
	message5 = TTF_RenderText_Solid(police, texte5, couleurNoire);
	/* Écriture du texte dans la SDL_Surface texte en mode Blended (optimal) */
	// texte1 = TTF_RenderText_Blended(police, "que voulez vous faire?", couleurNoire);
	int bloc1 = 0;
	int bloc2 = 0;
	int bloc3 = 0;
	int bloc4 = 0;
	int bloc5 = 0;
	int bloc6 = 0;
	// Initialisation des positions
	positionMessage.x = 100;
	positionMessage.y = 500;

	int bloctemps = 1;
	int affichage = 1;
	int memo;

	position.x = 0;
	position.y = 0;
	SDL_BlitSurface(fond, NULL, ecran, &position); /* Blit du fond */
	SDL_Flip(ecran);

	position1.x = 100;
	position1.y = 300;
	position2.x = 200;
	position2.y = 300;
	position3.x = 300;
	position3.y = 300;
	position4.x = 100;
	position4.y = 400;
	position5.x = 200;
	position5.y = 400;
	position6.x = 300;
	position6.y = 400;

	SDL_BlitSurface(un, NULL, ecran, &position1); /* Blit du texte */
	SDL_Flip(ecran);

	SDL_BlitSurface(deux, NULL, ecran, &position2); /* Blit du texte */
	SDL_Flip(ecran);

	SDL_BlitSurface(trois, NULL, ecran, &position3); /* Blit du texte */
	SDL_Flip(ecran);

	SDL_BlitSurface(quatre, NULL, ecran, &position4); /* Blit du texte */
	SDL_Flip(ecran);

	SDL_BlitSurface(cinq, NULL, ecran, &position5); /* Blit du texte */
	SDL_Flip(ecran);

	finPoseJeton = 0;
	int clic = 0;

	while (continuer) {   // TIMER

		tempsActuel = SDL_GetTicks();
		if (tempsActuel - tempsPrecedent >= 1000) /* Si 1000 ms au moins se sont coules */
		{
			bloctemps = 1;
			affichage = 1;
		}
		if (compteur == 0) {
			continuer = 0;
			finPoseJeton = 1;
			bloctemps = 1;
			affichage = 1;
			//  compteur = 20;
		}

		if (affichage == 1) {
			SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
			SDL_BlitSurface(fond, NULL, ecran, &position); /* Blit du fond */
			SDL_Flip(ecran);

			if (clic == 1 && bloc1 == 1) {
				un = TTF_RenderText_Solid(policeTemps, "1", couleurNoire);
				SDL_BlitSurface(un, NULL, ecran, &position1); /* Blit du texte */
				SDL_Flip(ecran);
			} else {
				SDL_BlitSurface(un, NULL, ecran, &position1); /* Blit du texte */
				SDL_Flip(ecran);
			}
			if (clic == 1 && bloc2 == 1) {
				deux = TTF_RenderText_Solid(policeTemps, "2", couleurNoire);
				SDL_BlitSurface(deux, NULL, ecran, &position2); /* Blit du texte */
				SDL_Flip(ecran);
			} else {
				SDL_BlitSurface(deux, NULL, ecran, &position2); /* Blit du texte */
				SDL_Flip(ecran);
			}
			if (clic == 1 && bloc3 == 1) {
				trois = TTF_RenderText_Solid(policeTemps, "3", couleurNoire);
				SDL_BlitSurface(trois, NULL, ecran, &position3); /* Blit du texte */
				SDL_Flip(ecran);
			} else {
				SDL_BlitSurface(trois, NULL, ecran, &position3); /* Blit du texte */
				SDL_Flip(ecran);
			}
			if (clic == 1 && bloc4 == 1) {
				quatre = TTF_RenderText_Solid(policeTemps, "4", couleurNoire);
				SDL_BlitSurface(quatre, NULL, ecran, &position4); /* Blit du texte */
				SDL_Flip(ecran);
			} else {
				SDL_BlitSurface(quatre, NULL, ecran, &position4); /* Blit du texte */
				SDL_Flip(ecran);
			}
			if (clic == 1 && bloc5 == 1) {
				cinq = TTF_RenderText_Solid(policeTemps, "5", couleurNoire);
				SDL_BlitSurface(cinq, NULL, ecran, &position5); /* Blit du texte */
				SDL_Flip(ecran);
			} else {
				SDL_BlitSurface(cinq, NULL, ecran, &position5); /* Blit du texte */
				SDL_Flip(ecran);
			}

			if (bloctemps == 0) {
				sprintf(temps, "Temps : %d s", compteur); /* On crit dans la chane "temps" le nouveau temps */
				SDL_BlitSurface(texteTimer, NULL, ecran, &positionTimer); /* Blit du texte */
			}

			if (bloc1 == 1) {
				SDL_BlitSurface(message1, NULL, ecran, &positionMessage); /* Blit du texte */
				SDL_Flip(ecran);
			}
			if (bloc2 == 1) {
				SDL_BlitSurface(message2, NULL, ecran, &positionMessage); /* Blit du texte */
				SDL_Flip(ecran);
			}
			if (bloc3 == 1) {
				SDL_BlitSurface(message3, NULL, ecran, &positionMessage); /* Blit du texte */
				SDL_Flip(ecran);
			}
			if (bloc4 == 1) {
				SDL_BlitSurface(message4, NULL, ecran, &positionMessage); /* Blit du texte */
				SDL_Flip(ecran);
			}
			if (bloc5 == 1) {
				SDL_BlitSurface(message5, NULL, ecran, &positionMessage); /* Blit du texte */
				SDL_Flip(ecran);
			}

			if (bloctemps == 1) {
				TEMPS = compteur;
				sprintf(temps, "Temps : %d s", compteur); /* On crit dans la chane "temps" le nouveau temps */
				SDL_FreeSurface(texteTimer); /* On supprime la surface prcdente */
				texteTimer = TTF_RenderText_Blended(policeTemps, temps,
						couleurNoire); /* On crit la chane temps dans la SDL_Surface */
				tempsPrecedent = tempsActuel; /* On met  jour le tempsPrecedent */
				positionTimer.x = 20;
				positionTimer.y = 620;
				SDL_BlitSurface(texteTimer, NULL, ecran, &positionTimer); /* Blit du texte */
				SDL_Flip(ecran);
				bloctemps = 0;
				compteur -= 1; /* On rajoute 100 ms au compteur */
			}

			if (clic == 1) {
				continuer = 0;
				SDL_Delay(1000);
			}

			affichage = 0;
		}

		SDL_PollEvent(&event);
		switch (event.type) {
		case SDL_QUIT:
			continuer = 0;
			TTF_CloseFont(police);
			TTF_Quit();

			SDL_FreeSurface(un);
			SDL_FreeSurface(deux);
			SDL_FreeSurface(trois);
			SDL_FreeSurface(quatre);
			SDL_FreeSurface(cinq);
			SDL_FreeSurface(six);
			SDL_Quit();
			exit(1);
			break;
			// affichage d'un petit message informatif  on survolant la souris sur les messages affichés
		case SDL_MOUSEMOTION:
			if ((event.motion.x > position1.x)
					&& (event.motion.x < position1.x + 50)
					&& (event.motion.y > position1.y)
					&& (event.motion.y < position1.y + 50)) {
				memo = bloc1;
				bloc1 = 1;
				if (memo == 0) {
					affichage = 1;
				}
			} else {
				bloc1 = 0;
			}
			if ((event.motion.x > position2.x)
					&& (event.motion.x < position2.x + 50)
					&& (event.motion.y > position2.y)
					&& (event.motion.y < position2.y + 50)) {
				memo = bloc2;
				bloc2 = 1;
				if (memo == 0) {
					affichage = 1;
				}
			} else {
				bloc2 = 0;
			}
			if ((event.motion.x > position3.x)
					&& (event.motion.x < position3.x + 50)
					&& (event.motion.y > position3.y)
					&& (event.motion.y < position3.y + 50)) {
				memo = bloc3;
				bloc3 = 1;
				if (memo == 0) {
					affichage = 1;
				}
			} else {
				bloc3 = 0;
			}
			if ((event.motion.x > position4.x)
					&& (event.motion.x < position4.x + 50)
					&& (event.motion.y > position4.y)
					&& (event.motion.y < position4.y + 50)) {
				memo = bloc4;
				bloc4 = 1;
				if (memo == 0) {
					affichage = 1;
				}
			} else {
				bloc4 = 0;
			}
			if ((event.motion.x > position5.x)
					&& (event.motion.x < position5.x + 50)
					&& (event.motion.y > position5.y)
					&& (event.motion.y < position5.y + 50)) {
				memo = bloc5;
				bloc5 = 1;
				if (memo == 0) {
					affichage = 1;
				}
			} else {
				bloc5 = 0;
			}

			// affectation de la valeur choisi du pion grace au clique
		case SDL_MOUSEBUTTONUP:
			if (event.button.button == SDL_BUTTON_LEFT && clic == 0) {
				if ((event.motion.x > position1.x)
						&& (event.motion.x < position1.x + 50)
						&& (event.motion.y > position1.y)
						&& (event.motion.y < position1.y + 50)
						&& (joueur[numero].source[1] > 0)) {
					val = 1;
					joueur[numero].source[1]--;
					clic = 1;
					break;
				}
				if ((event.motion.x > position2.x)
						&& (event.motion.x < position2.x + 50)
						&& (event.motion.y > position2.y)
						&& (event.motion.y < position2.y + 50)
						&& (joueur[numero].source[2] > 0)) {
					val = 2;
					joueur[numero].source[2]--;
					clic = 1;
					break;
				}
				if ((event.motion.x > position3.x)
						&& (event.motion.x < position3.x + 50)
						&& (event.motion.y > position3.y)
						&& (event.motion.y < position3.y + 50)
						&& (joueur[numero].source[3] > 0)) {
					val = 3;
					joueur[numero].source[3]--;
					clic = 1;
					break;
				}
				if ((event.motion.x > position4.x)
						&& (event.motion.x < position4.x + 50)
						&& (event.motion.y > position4.y)
						&& (event.motion.y < position4.y + 50)
						&& (joueur[numero].source[4] > 0)) {
					val = 4;
					joueur[numero].source[4]--;
					clic = 1;
					break;
				}
				if ((event.motion.x > position5.x)
						&& (event.motion.x < position5.x + 50)
						&& (event.motion.y > position5.y)
						&& (event.motion.y < position5.y + 50)
						&& (joueur[numero].source[5] > 0)) {
					val = 5;
					joueur[numero].source[5]--;
					clic = 1;
					break;
				}
				if ((event.motion.x > position6.x)
						&& (event.motion.x < position6.x + 50)
						&& (event.motion.y > position6.y)
						&& (event.motion.y < position6.y + 50)
						&& (joueur[numero].source[6] > 0)) {
					val = 6;
					joueur[numero].source[6]--;
					clic = 1;
					break;
				}
			}

		}

	}

	SDL_Delay(500);

	TTF_CloseFont(police);
	SDL_FreeSurface(un);
	SDL_FreeSurface(deux);
	SDL_FreeSurface(trois);
	SDL_FreeSurface(quatre);
	SDL_FreeSurface(cinq);
	SDL_FreeSurface(six);

	return val;
}

void poser_mur(void) {
	SDL_Surface *ecran = NULL, *imageDeFond = NULL, *zozor = NULL,
			*passe = NULL;
	SDL_Rect positionFond, positionZozor, positionpasse;
	int continuer = 1;
	SDL_Event event;

	positionFond.x = 0;
	positionFond.y = 0;
	positionZozor.x = 16;
	positionZozor.y = 110;
	positionpasse.x = 0;
	positionpasse.y = 0;
	SDL_Init(SDL_INIT_VIDEO);

	SDL_WM_SetIcon(SDL_LoadBMP("src/interface/image/plateau/icone.bmp"), NULL);
	ecran = SDL_SetVideoMode(900, 684, 32, SDL_HWSURFACE);
	SDL_WM_SetCaption("Armadora", NULL);

	imageDeFond = SDL_LoadBMP("src/interface/image/plateau/Plateau.bmp");
	SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);

	//TIMER
	/* Chargement de la police */
	TTF_Init();
	TTF_Font *police = NULL, *policeChoix = NULL;
	SDL_Color couleurNoire = { 0, 0, 0 };
	police = TTF_OpenFont("src/interface/fonts/Subscribe.ttf", 65);
	policeChoix = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 65);
	SDL_Surface *texteTimer = NULL;
	SDL_Rect positionTimer;
	int tempsActuel = 0, tempsPrecedent = 0, compteur = TEMPS;
	char temps[40] = ""; /* Tableau de char suffisamment grand */

	SDL_Flip(ecran);

	Affichage(1);

	passe = TTF_RenderText_Solid(policeChoix, "Passer", couleurNoire);
	SDL_BlitSurface(passe, NULL, ecran, &positionpasse); /* Blit du texte */
	SDL_Flip(ecran);

	int bloctemps = 0;
	finPoseMur = 0;
	int bloc = 1;

	while (continuer) {
		// TIMER

		tempsActuel = SDL_GetTicks();
		if (tempsActuel - tempsPrecedent >= 1000) /* Si 1000 ms au moins se sont coules */
		{
			bloctemps = 1;
		}
		if (compteur == 0) {
			continuer = 0;
			finPoseMur = 1;
			bloctemps = 1;
			//  compteur = 20;
		}

		if (bloctemps == 1) {
			Affichage(1);

			passe = TTF_RenderText_Solid(policeChoix, "Passer", couleurNoire);
			SDL_BlitSurface(passe, NULL, ecran, &positionpasse); /* Blit du texte */
			SDL_Flip(ecran);

			TEMPS = compteur;
			sprintf(temps, "%d s", compteur); /* On crit dans la chane "temps" le nouveau temps */
			SDL_FreeSurface(texteTimer); /* On supprime la surface prcdente */
			texteTimer = TTF_RenderText_Blended(police, temps, couleurNoire); /* On crit la chane temps dans la SDL_Surface */
			tempsPrecedent = tempsActuel; /* On met  jour le tempsPrecedent */
			positionTimer.x = 800;
			positionTimer.y = 10;
			SDL_BlitSurface(texteTimer, NULL, ecran, &positionTimer); /* Blit du texte */
			SDL_Flip(ecran);
			bloctemps = 0;
			compteur -= 1; /* On rajoute 100 ms au compteur */
		}

		if (bloc == 0) {
			Affichage(0);

			finPoseMur = 1;

			TEMPS = compteur;
			sprintf(temps, "%d s", compteur); /* On crit dans la chane "temps" le nouveau temps */
			SDL_FreeSurface(texteTimer); /* On supprime la surface prcdente */
			texteTimer = TTF_RenderText_Blended(police, temps, couleurNoire); /* On crit la chane temps dans la SDL_Surface */

			passe = TTF_RenderText_Solid(police, "Passer", couleurNoire);
			SDL_BlitSurface(passe, NULL, ecran, &positionpasse); /* Blit du texte */
			SDL_Flip(ecran);

			continuer = 0;
			SDL_Delay(1000);
		}

		// EVENT

		SDL_PollEvent(&event);
		switch (event.type) {
		case SDL_QUIT:
			continuer = 0;
			SDL_FreeSurface(imageDeFond);
			SDL_FreeSurface(zozor);
			SDL_Quit();
			exit(1);
			break;

		case SDL_MOUSEBUTTONUP:
			if (event.button.button == SDL_BUTTON_LEFT) {

				if ((event.button.x > positionpasse.x)
						&& (event.button.x < positionpasse.x + 200)
						&& (event.button.y > positionpasse.y)
						&& (event.button.y < positionpasse.x + 50)) {
					bloc = 0;
					break;
				}
				/*----------------------------------------HORIZONTAL------------------------------------------------------------------*/

				/*------------------------------------------LIGNE1--------------------------------------------------------------------*/
				if ((event.button.x > 16) && (event.button.x < 123)
						&& (event.button.y > 200) && (event.button.y < 240)
						&& (plateau[2][1].flag_interdit == 0)
						&& (plateau[2][1].mur == 0)) {
					plateau[2][1].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 126) && (event.button.x < 230)
						&& (event.button.y > 180) && (event.button.y < 240)
						&& (plateau[2][3].flag_interdit == 0)
						&& (plateau[2][3].mur == 0)) {
					plateau[2][3].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 236) && (event.button.x < 340)
						&& (event.button.y > 180) && (event.button.y < 240)
						&& (plateau[2][5].flag_interdit == 0)
						&& (plateau[2][5].mur == 0)) {
					plateau[2][5].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 345) && (event.button.x < 450)
						&& (event.button.y > 180) && (event.button.y < 240)
						&& (plateau[2][7].flag_interdit == 0)
						&& (plateau[2][7].mur == 0)) {
					plateau[2][7].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 456) && (event.button.x < 560)
						&& (event.button.y > 180) && (event.button.y < 240)
						&& (plateau[2][9].flag_interdit == 0)
						&& (plateau[2][9].mur == 0)) {
					plateau[2][9].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 566) && (event.button.x < 670)
						&& (event.button.y > 180) && (event.button.y < 240)
						&& (plateau[2][11].flag_interdit == 0)
						&& (plateau[2][11].mur == 0)) {
					plateau[2][11].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 676) && (event.button.x < 780)
						&& (event.button.y > 180) && (event.button.y < 240)
						&& (plateau[2][13].flag_interdit == 0)
						&& (plateau[2][13].mur == 0)) {
					plateau[2][13].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 786) && (event.button.x < 896)
						&& (event.button.y > 180) && (event.button.y < 240)
						&& (plateau[2][15].flag_interdit == 0)
						&& (plateau[2][15].mur == 0)) {
					plateau[2][15].mur = 1;
					continuer = 0;
					break;
				}
				/*------------------------------------------LIGNE2--------------------------------------------------------------------*/
				if ((event.button.x > 16) && (event.button.x < 123)
						&& (event.button.y > 290) && (event.button.y < 350)
						&& (plateau[4][1].flag_interdit == 0)
						&& (plateau[4][1].mur == 0)) {
					plateau[4][1].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 126) && (event.button.x < 230)
						&& (event.button.y > 290) && (event.button.y < 350)
						&& (plateau[4][3].flag_interdit == 0)
						&& (plateau[4][3].mur == 0)) {
					plateau[4][3].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 236) && (event.button.x < 340)
						&& (event.button.y > 290) && (event.button.y < 350)
						&& (plateau[4][5].flag_interdit == 0)
						&& (plateau[4][5].mur == 0)) {
					plateau[4][5].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 345) && (event.button.x < 450)
						&& (event.button.y > 290) && (event.button.y < 350)
						&& (plateau[4][7].flag_interdit == 0)
						&& (plateau[4][7].mur == 0)) {
					plateau[4][7].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 456) && (event.button.x < 560)
						&& (event.button.y > 290) && (event.button.y < 350)
						&& (plateau[4][9].flag_interdit == 0)
						&& (plateau[4][9].mur == 0)) {
					plateau[4][9].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 566) && (event.button.x < 670)
						&& (event.button.y > 290) && (event.button.y < 350)
						&& (plateau[4][11].flag_interdit == 0)
						&& (plateau[4][11].mur == 0)) {
					plateau[4][11].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 676) && (event.button.x < 780)
						&& (event.button.y > 290) && (event.button.y < 350)
						&& (plateau[4][13].flag_interdit == 0)
						&& (plateau[4][13].mur == 0)) {
					plateau[4][13].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 786) && (event.button.x < 896)
						&& (event.button.y > 290) && (event.button.y < 350)
						&& (plateau[4][15].flag_interdit == 0)
						&& (plateau[4][15].mur == 0)) {
					plateau[4][15].mur = 1;
					continuer = 0;
					break;
				}
				/*------------------------------------------LIGNE3--------------------------------------------------------------------*/
				if ((event.button.x > 16) && (event.button.x < 123)
						&& (event.button.y > 400) && (event.button.y < 460)
						&& (plateau[6][1].flag_interdit == 0)
						&& (plateau[6][1].mur == 0)) {
					plateau[6][1].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 126) && (event.button.x < 230)
						&& (event.button.y > 400) && (event.button.y < 460)
						&& (plateau[6][3].flag_interdit == 0)
						&& (plateau[6][3].mur == 0)) {
					plateau[6][3].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 236) && (event.button.x < 340)
						&& (event.button.y > 400) && (event.button.y < 460)
						&& (plateau[6][5].flag_interdit == 0)
						&& (plateau[6][5].mur == 0)) {
					plateau[6][5].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 345) && (event.button.x < 450)
						&& (event.button.y > 400) && (event.button.y < 460)
						&& (plateau[6][7].flag_interdit == 0)
						&& (plateau[6][7].mur == 0)) {
					plateau[6][7].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 456) && (event.button.x < 560)
						&& (event.button.y > 400) && (event.button.y < 460)
						&& (plateau[6][9].flag_interdit == 0)
						&& (plateau[6][9].mur == 0)) {
					plateau[6][9].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 566) && (event.button.x < 670)
						&& (event.button.y > 400) && (event.button.y < 460)
						&& (plateau[6][11].flag_interdit == 0)
						&& (plateau[6][11].mur == 0)) {
					plateau[6][11].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 676) && (event.button.x < 780)
						&& (event.button.y > 400) && (event.button.y < 460)
						&& (plateau[6][13].flag_interdit == 0)
						&& (plateau[6][13].mur == 0)) {
					plateau[6][13].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 786) && (event.button.x < 896)
						&& (event.button.y > 400) && (event.button.y < 460)
						&& (plateau[6][15].flag_interdit == 0)
						&& (plateau[6][15].mur == 0)) {
					plateau[6][15].mur = 1;
					continuer = 0;
					break;
				}
				/*------------------------------------------LIGNE4--------------------------------------------------------------------*/
				if ((event.button.x > 16) && (event.button.x < 123)
						&& (event.button.y > 510) && (event.button.y < 570)
						&& (plateau[8][1].flag_interdit == 0)
						&& (plateau[8][1].mur == 0)) {
					plateau[8][1].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 126) && (event.button.x < 230)
						&& (event.button.y > 510) && (event.button.y < 570)
						&& (plateau[8][3].flag_interdit == 0)
						&& (plateau[8][3].mur == 0)) {
					plateau[8][3].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 236) && (event.button.x < 340)
						&& (event.button.y > 510) && (event.button.y < 570)
						&& (plateau[8][5].flag_interdit == 0)
						&& (plateau[8][5].mur == 0)) {
					plateau[8][5].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 345) && (event.button.x < 450)
						&& (event.button.y > 510) && (event.button.y < 570)
						&& (plateau[8][7].flag_interdit == 0)
						&& (plateau[8][7].mur == 0)) {
					plateau[8][7].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 456) && (event.button.x < 560)
						&& (event.button.y > 510) && (event.button.y < 570)
						&& (plateau[8][9].flag_interdit == 0)
						&& (plateau[8][9].mur == 0)) {
					plateau[8][9].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 566) && (event.button.x < 670)
						&& (event.button.y > 510) && (event.button.y < 570)
						&& (plateau[8][11].flag_interdit == 0)
						&& (plateau[8][11].mur == 0)) {
					plateau[8][11].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 676) && (event.button.x < 780)
						&& (event.button.y > 510) && (event.button.y < 570)
						&& (plateau[8][13].flag_interdit == 0)
						&& (plateau[8][13].mur == 0)) {
					plateau[8][13].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 786) && (event.button.x < 896)
						&& (event.button.y > 510) && (event.button.y < 570)
						&& (plateau[8][15].flag_interdit == 0)
						&& (plateau[8][15].mur == 0)) {
					plateau[8][15].mur = 1;
					continuer = 0;
					break;
				}

				/*-----------------------------------------VERTICAL-------------------------------------------------------------------*/

				/*------------------------------------------LIGNE1--------------------------------------------------------------------*/
				if ((event.button.x > 110) && (event.button.x < 130)
						&& (event.button.y > 140) && (event.button.y < 200)
						&& (plateau[1][2].flag_interdit == 0)
						&& (plateau[1][2].mur == 0)) {
					plateau[1][2].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 210) && (event.button.x < 250)
						&& (event.button.y > 140) && (event.button.y < 200)
						&& (plateau[1][4].flag_interdit == 0)
						&& (plateau[1][4].mur == 0)) {
					plateau[1][4].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 320) && (event.button.x < 360)
						&& (event.button.y > 140) && (event.button.y < 200)
						&& (plateau[1][6].flag_interdit == 0)
						&& (plateau[1][6].mur == 0)) {
					plateau[1][6].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 420) && (event.button.x < 460)
						&& (event.button.y > 140) && (event.button.y < 200)
						&& (plateau[1][8].flag_interdit == 0)
						&& (plateau[1][8].mur == 0)) {
					plateau[1][8].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 530) && (event.button.x < 570)
						&& (event.button.y > 140) && (event.button.y < 200)
						&& (plateau[1][10].flag_interdit == 0)
						&& (plateau[1][10].mur == 0)) {
					plateau[1][10].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 640) && (event.button.x < 680)
						&& (event.button.y > 140) && (event.button.y < 200)
						&& (plateau[1][12].flag_interdit == 0)
						&& (plateau[1][12].mur == 0)) {
					plateau[1][12].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 750) && (event.button.x < 790)
						&& (event.button.y > 140) && (event.button.y < 200)
						&& (plateau[1][14].flag_interdit == 0)
						&& (plateau[1][14].mur == 0)) {
					plateau[1][14].mur = 1;
					continuer = 0;
					break;
				}
				/*------------------------------------------LIGNE2--------------------------------------------------------------------*/
				if ((event.button.x > 110) && (event.button.x < 130)
						&& (event.button.y > 240) && (event.button.y < 300)
						&& (plateau[3][2].flag_interdit == 0)
						&& (plateau[3][2].mur == 0)) {
					plateau[3][2].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 210) && (event.button.x < 250)
						&& (event.button.y > 240) && (event.button.y < 300)
						&& (plateau[3][4].flag_interdit == 0)
						&& (plateau[3][4].mur == 0)) {
					plateau[3][4].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 320) && (event.button.x < 360)
						&& (event.button.y > 240) && (event.button.y < 300)
						&& (plateau[3][6].flag_interdit == 0)
						&& (plateau[3][6].mur == 0)) {
					plateau[3][6].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 420) && (event.button.x < 460)
						&& (event.button.y > 240) && (event.button.y < 300)
						&& (plateau[3][8].flag_interdit == 0)
						&& (plateau[3][8].mur == 0)) {
					plateau[3][8].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 530) && (event.button.x < 570)
						&& (event.button.y > 240) && (event.button.y < 300)
						&& (plateau[3][10].flag_interdit == 0)
						&& (plateau[3][10].mur == 0)) {
					plateau[3][10].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 640) && (event.button.x < 680)
						&& (event.button.y > 240) && (event.button.y < 300)
						&& (plateau[3][12].flag_interdit == 0)
						&& (plateau[3][12].mur == 0)) {
					plateau[3][12].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 750) && (event.button.x < 790)
						&& (event.button.y > 240) && (event.button.y < 300)
						&& (plateau[3][14].flag_interdit == 0)
						&& (plateau[3][14].mur == 0)) {
					plateau[3][14].mur = 1;
					continuer = 0;
					break;
				}
				/*------------------------------------------LIGNE3--------------------------------------------------------------------*/
				if ((event.button.x > 110) && (event.button.x < 130)
						&& (event.button.y > 320) && (event.button.y < 400)
						&& (plateau[5][2].flag_interdit == 0)
						&& (plateau[5][2].mur == 0)) {
					plateau[5][2].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 210) && (event.button.x < 250)
						&& (event.button.y > 320) && (event.button.y < 400)
						&& (plateau[5][4].flag_interdit == 0)
						&& (plateau[5][4].mur == 0)) {
					plateau[5][4].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 320) && (event.button.x < 360)
						&& (event.button.y > 320) && (event.button.y < 400)
						&& (plateau[5][6].flag_interdit == 0)
						&& (plateau[5][6].mur == 0)) {
					plateau[5][6].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 420) && (event.button.x < 460)
						&& (event.button.y > 320) && (event.button.y < 400)
						&& (plateau[5][8].flag_interdit == 0)
						&& (plateau[5][8].mur == 0)) {
					plateau[5][8].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 530) && (event.button.x < 570)
						&& (event.button.y > 320) && (event.button.y < 400)
						&& (plateau[5][10].flag_interdit == 0)
						&& (plateau[5][10].mur == 0)) {
					plateau[5][10].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 640) && (event.button.x < 680)
						&& (event.button.y > 320) && (event.button.y < 400)
						&& (plateau[5][12].flag_interdit == 0)
						&& (plateau[5][12].mur == 0)) {
					plateau[5][12].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 750) && (event.button.x < 790)
						&& (event.button.y > 320) && (event.button.y < 400)
						&& (plateau[5][14].flag_interdit == 0)
						&& (plateau[5][14].mur == 0)) {
					plateau[5][14].mur = 1;
					continuer = 0;
					break;
				}
				/*------------------------------------------LIGNE4--------------------------------------------------------------------*/
				if ((event.button.x > 110) && (event.button.x < 130)
						&& (event.button.y > 420) && (event.button.y < 500)
						&& (plateau[7][2].flag_interdit == 0)
						&& (plateau[7][2].mur == 0)) {
					plateau[7][2].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 210) && (event.button.x < 250)
						&& (event.button.y > 420) && (event.button.y < 500)
						&& (plateau[7][4].flag_interdit == 0)
						&& (plateau[7][4].mur == 0)) {
					plateau[7][4].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 320) && (event.button.x < 360)
						&& (event.button.y > 420) && (event.button.y < 500)
						&& (plateau[7][6].flag_interdit == 0)
						&& (plateau[7][6].mur == 0)) {
					plateau[7][6].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 420) && (event.button.x < 460)
						&& (event.button.y > 420) && (event.button.y < 500)
						&& (plateau[7][8].flag_interdit == 0)
						&& (plateau[7][8].mur == 0)) {
					plateau[7][8].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 530) && (event.button.x < 570)
						&& (event.button.y > 420) && (event.button.y < 500)
						&& (plateau[7][10].flag_interdit == 0)
						&& (plateau[7][10].mur == 0)) {
					plateau[7][10].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 640) && (event.button.x < 680)
						&& (event.button.y > 420) && (event.button.y < 500)
						&& (plateau[7][12].flag_interdit == 0)
						&& (plateau[7][12].mur == 0)) {
					plateau[7][12].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 750) && (event.button.x < 790)
						&& (event.button.y > 420) && (event.button.y < 500)
						&& (plateau[7][14].flag_interdit == 0)
						&& (plateau[7][14].mur == 0)) {
					plateau[7][14].mur = 1;
					continuer = 0;
					break;
				}
				/*------------------------------------------LIGNE5--------------------------------------------------------------------*/
				if ((event.button.x > 110) && (event.button.x < 130)
						&& (event.button.y > 550) && (event.button.y < 700)
						&& (plateau[9][2].flag_interdit == 0)
						&& (plateau[9][2].mur == 0)) {
					plateau[9][2].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 210) && (event.button.x < 250)
						&& (event.button.y > 550) && (event.button.y < 700)
						&& (plateau[9][4].flag_interdit == 0)
						&& (plateau[9][4].mur == 0)) {
					plateau[9][4].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 320) && (event.button.x < 360)
						&& (event.button.y > 550) && (event.button.y < 700)
						&& (plateau[9][6].flag_interdit == 0)
						&& (plateau[9][6].mur == 0)) {
					plateau[9][6].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 420) && (event.button.x < 460)
						&& (event.button.y > 550) && (event.button.y < 700)
						&& (plateau[9][8].flag_interdit == 0)
						&& (plateau[9][8].mur == 0)) {
					plateau[9][8].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 530) && (event.button.x < 570)
						&& (event.button.y > 550) && (event.button.y < 700)
						&& (plateau[9][10].flag_interdit == 0)
						&& (plateau[9][10].mur == 0)) {
					plateau[9][10].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 640) && (event.button.x < 680)
						&& (event.button.y > 550) && (event.button.y < 700)
						&& (plateau[9][12].flag_interdit == 0)
						&& (plateau[9][12].mur == 0)) {
					plateau[9][12].mur = 1;
					continuer = 0;
					break;
				}
				if ((event.button.x > 750) && (event.button.x < 790)
						&& (event.button.y > 550) && (event.button.y < 700)
						&& (plateau[9][14].flag_interdit == 0)
						&& (plateau[9][14].mur == 0)) {
					plateau[9][14].mur = 1;
					continuer = 0;
					break;
				}

				/*-------------------------------------------Fin----------------------------------------------------------------------*/
			}
			break;
		}
	}

	Affichage(0);
	SDL_Delay(1000);
}

int poser_jeton(int val, int numero) {
	int continuer = 1;
	int hero = joueur[numero].hero;
	SDL_Event event;
	Affichage(0);
	TTF_Init();
	SDL_Surface *ecran = NULL, *imageDeFond = NULL, *zozor = NULL,
			*passe = NULL;

	SDL_Init(SDL_INIT_VIDEO);
	SDL_WM_SetIcon(SDL_LoadBMP("src/interface/image/plateau/icone.bmp"), NULL);
	ecran = SDL_SetVideoMode(900, 684, 32, SDL_HWSURFACE);
	SDL_WM_SetCaption("Armadora", NULL);

	//TIMER
	/* Chargement de la police */
	TTF_Init();
	if (TTF_Init() == -1) {
		fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n",
				TTF_GetError());
		exit(EXIT_FAILURE);
	}
	TTF_Font *police = NULL, *policeChoix = NULL;
	SDL_Color couleurNoire = { 0, 0, 0 };
	police = TTF_OpenFont("src/interface/fonts/Subscribe.ttf", 65);
	if (!police) {
		printf("\nERREUR : chargement police\n");
	}
	policeChoix = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 65);
	if (!policeChoix) {
		printf("\nERREUR : chargement police\n");
	}
	SDL_Surface *texteTimer = NULL;
	SDL_Rect positionTimer;
	int tempsActuel = 0, tempsPrecedent = 0, compteur = TEMPS;
	char temps[40] = ""; /* Tableau de char suffisamment grand */
	int bloctemps = 1;

	SDL_Flip(ecran);

	SDL_Rect positionpasse;
	positionpasse.x = 0;
	positionpasse.y = 0;
	policeChoix = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 65);
	passe = TTF_RenderText_Solid(policeChoix, "Passer", couleurNoire);
	SDL_BlitSurface(passe, NULL, ecran, &positionpasse); /* Blit du texte */
	SDL_Flip(ecran);
	int bloc = 1;
	int retour = 0;

	while (continuer) {

		if (bloc == 0) {
			Affichage(0);

			retour = 1;
			TEMPS = compteur;
			sprintf(temps, "%d s", compteur); /* On ecrit dans la chane "temps" le nouveau temps */
			SDL_FreeSurface(texteTimer); /* On supprime la surface prcdente */
			texteTimer = TTF_RenderText_Blended(police, temps, couleurNoire); /* On crit la chane temps dans la SDL_Surface */
			passe = TTF_RenderText_Solid(police, "Passer", couleurNoire);
			SDL_BlitSurface(passe, NULL, ecran, &positionpasse); /* Blit du texte */
			SDL_Flip(ecran);

			continuer = 0;
			SDL_Delay(1000);
		}
		// TIMER

		tempsActuel = SDL_GetTicks();
		if (tempsActuel - tempsPrecedent >= 1000) /* Si 1000 ms au moins se sont coules */
		{
			bloctemps = 1;
		}
		if (compteur == 0) {
			continuer = 0;
			retour = 1;
			bloctemps = 1;
			//  compteur = 20;
		}
		if (bloctemps == 1) {
			Affichage(0);

			passe = TTF_RenderText_Solid(policeChoix, "Passer", couleurNoire);
			SDL_BlitSurface(passe, NULL, ecran, &positionpasse); /* Blit du texte */
			SDL_Flip(ecran);

			TEMPS = compteur;
			sprintf(temps, "%d s", compteur); /* On crit dans la chane "temps" le nouveau temps */
			SDL_FreeSurface(texteTimer); /* On supprime la surface prcdente */
			texteTimer = TTF_RenderText_Blended(police, temps, couleurNoire); /* On crit la chane temps dans la SDL_Surface */
			tempsPrecedent = tempsActuel; /* On met  jour le tempsPrecedent */
			positionTimer.x = 800;
			positionTimer.y = 10;
			SDL_BlitSurface(texteTimer, NULL, ecran, &positionTimer); /* Blit du texte */
			SDL_Flip(ecran);
			bloctemps = 0;
			compteur -= 1; /* On rajoute 100 ms au compteur */
		}

		SDL_PollEvent(&event);

		switch (event.type) {
		case SDL_QUIT:
			continuer = 0;
			SDL_FreeSurface(imageDeFond);
			SDL_FreeSurface(zozor);
			SDL_Quit();
			exit(1);
			break;
			//posage de jeton sur les cases choisi en fontion des cliques
		case SDL_MOUSEBUTTONUP:
			if (event.button.button == SDL_BUTTON_LEFT) {
				if ((event.button.x > positionpasse.x)
						&& (event.button.x < positionpasse.x + 200)
						&& (event.button.y > positionpasse.y)
						&& (event.button.y < positionpasse.y + 50)) {
					bloc = 0;
					break;
				}
				if ((event.button.x > 25) && (event.button.x < 130)
						&& (event.button.y > 145) && (event.button.y < 230)
						&& (plateau[1][1].hero == 0)) {
					plateau[1][1].valeurJeton = val;
					plateau[1][1].hero = hero;
					plateau[1][1].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 115) && (event.button.x < 220)
						&& (event.button.y > 145) && (event.button.y < 230)
						&& (plateau[1][3].hero == 0)) {
					plateau[1][3].valeurJeton = val;
					plateau[1][3].hero = hero;
					plateau[1][3].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 240) && (event.button.x < 310)
						&& (event.button.y > 145) && (event.button.y < 230)
						&& (plateau[1][5].hero == 0)) {
					plateau[1][5].valeurJeton = val;
					plateau[1][5].hero = hero;
					plateau[1][5].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 450) && (event.button.x < 556)
						&& (event.button.y > 145) && (event.button.y < 230)
						&& (plateau[1][9].hero == 0)) {
					plateau[1][9].valeurJeton = val;
					plateau[1][9].hero = hero;
					plateau[1][9].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 560) && (event.button.x < 660)
						&& (event.button.y > 145) && (event.button.y < 230)
						&& (plateau[1][11].hero == 0)) {
					plateau[1][11].valeurJeton = val;
					plateau[1][11].hero = hero;
					plateau[1][11].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 669) && (event.button.x < 770)
						&& (event.button.y > 145) && (event.button.y < 230)
						&& (plateau[1][13].hero == 0)) {
					plateau[1][13].valeurJeton = val;
					plateau[1][13].hero = hero;
					plateau[1][13].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 777) && (event.button.x < 880)
						&& (event.button.y > 145) && (event.button.y < 230)
						&& (plateau[1][15].hero == 0)) {
					plateau[1][15].valeurJeton = val;
					plateau[1][15].hero = hero;
					plateau[1][15].joueur = numero;
					continuer = 0;
					break;
				}
				/*---------------------------------------------ligne2---------------------------------------------------------*/
				if ((event.button.x > 25) && (event.button.x < 105)
						&& (event.button.y > 240) && (event.button.y < 340)
						&& (plateau[3][1].hero == 0)) {
					plateau[3][1].valeurJeton = val;
					plateau[3][1].hero = hero;
					plateau[3][1].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 230) && (event.button.x < 310)
						&& (event.button.y > 240) && (event.button.y < 340)
						&& (plateau[3][5].hero == 0)) {
					plateau[3][5].valeurJeton = val;
					plateau[3][5].hero = hero;
					plateau[3][5].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 330) && (event.button.x < 420)
						&& (event.button.y > 240) && (event.button.y < 340)
						&& (plateau[3][7].hero == 0)) {
					plateau[3][7].valeurJeton = val;
					plateau[3][7].hero = hero;
					plateau[3][7].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 440) && (event.button.x < 530)
						&& (event.button.y > 240) && (event.button.y < 340)
						&& (plateau[3][9].hero == 0)) {
					plateau[3][9].valeurJeton = val;
					plateau[3][9].hero = hero;
					plateau[3][9].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 660) && (event.button.x < 750)
						&& (event.button.y > 240) && (event.button.y < 340)
						&& (plateau[3][13].hero == 0)) {
					plateau[3][13].valeurJeton = val;
					plateau[3][13].hero = hero;
					plateau[3][13].joueur = numero;
					continuer = 0;
					break;
				}
				/*---------------------------------------------linge3--------------------------------------------------------*/
				if ((event.button.x > 25) && (event.button.x < 105)
						&& (event.button.y > 345) && (event.button.y < 445)
						&& (plateau[5][1].hero == 0)) {
					plateau[5][1].valeurJeton = val;
					plateau[5][1].hero = hero;
					plateau[5][1].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 115) && (event.button.x < 200)
						&& (event.button.y > 345) && (event.button.y < 445)
						&& (plateau[5][3].hero == 0)) {
					plateau[5][3].valeurJeton = val;
					plateau[5][3].hero = hero;
					plateau[5][3].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 230) && (event.button.x < 310)
						&& (event.button.y > 345) && (event.button.y < 445)
						&& (plateau[5][5].hero == 0)) {
					plateau[5][5].valeurJeton = val;
					plateau[5][5].hero = hero;
					plateau[5][5].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 330) && (event.button.x < 420)
						&& (event.button.y > 345) && (event.button.y < 445)
						&& (plateau[5][7].hero == 0)) {
					plateau[5][7].valeurJeton = val;
					plateau[5][7].hero = hero;
					plateau[5][7].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 440) && (event.button.x < 530)
						&& (event.button.y > 345) && (event.button.y < 445)
						&& (plateau[5][9].hero == 0)) {
					plateau[5][9].valeurJeton = val;
					plateau[5][9].hero = hero;
					plateau[5][9].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 550) && (event.button.x < 630)
						&& (event.button.y > 345) && (event.button.y < 445)
						&& (plateau[5][11].hero == 0)) {
					plateau[5][11].valeurJeton = val;
					plateau[5][11].hero = hero;
					plateau[5][11].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 660) && (event.button.x < 750)
						&& (event.button.y > 345) && (event.button.y < 445)
						&& (plateau[5][13].hero == 0)) {
					plateau[5][13].valeurJeton = val;
					plateau[5][13].hero = hero;
					plateau[5][13].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 770) && (event.button.x < 850)
						&& (event.button.y > 345) && (event.button.y < 445)
						&& (plateau[5][15].hero == 0)) {
					plateau[5][15].valeurJeton = val;
					plateau[5][15].hero = hero;
					plateau[5][15].joueur = numero;
					continuer = 0;
					break;
				}
				/*---------------------------------------------ligne4--------------------------------------------------------*/
				if ((event.button.x > 115) && (event.button.x < 200)
						&& (event.button.y > 450) && (event.button.y < 555)
						&& (plateau[7][3].hero == 0)) {
					plateau[7][3].valeurJeton = val;
					plateau[7][3].hero = hero;
					plateau[7][3].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 230) && (event.button.x < 310)
						&& (event.button.y > 450) && (event.button.y < 555)
						&& (plateau[7][5].hero == 0)) {
					plateau[7][5].valeurJeton = val;
					plateau[7][5].hero = hero;
					plateau[7][5].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 330) && (event.button.x < 420)
						&& (event.button.y > 450) && (event.button.y < 555)
						&& (plateau[7][7].hero == 0)) {
					plateau[7][7].valeurJeton = val;
					plateau[7][7].hero = hero;
					plateau[7][7].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 550) && (event.button.x < 630)
						&& (event.button.y > 450) && (event.button.y < 555)
						&& (plateau[7][11].hero == 0)) {
					plateau[7][11].valeurJeton = val;
					plateau[7][11].hero = hero;
					plateau[7][11].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 650) && (event.button.x < 750)
						&& (event.button.y > 450) && (event.button.y < 555)
						&& (plateau[7][13].hero == 0)) {
					plateau[7][13].valeurJeton = val;
					plateau[7][13].hero = hero;
					plateau[7][13].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 770) && (event.button.x < 850)
						&& (event.button.y > 450) && (event.button.y < 555)
						&& (plateau[7][15].hero == 0)) {
					plateau[7][15].valeurJeton = val;
					plateau[7][15].hero = hero;
					plateau[7][15].joueur = numero;
					continuer = 0;
					break;
				}
				/*---------------------------------------------ligne5--------------------------------------------------------*/
				if ((event.button.x > 25) && (event.button.x < 105)
						&& (event.button.y > 560) && (event.button.y < 670)
						&& (plateau[9][1].hero == 0)) {
					plateau[9][1].valeurJeton = val;
					plateau[9][1].hero = hero;
					plateau[9][1].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 115) && (event.button.x < 200)
						&& (event.button.y > 560) && (event.button.y < 670)
						&& (plateau[9][3].hero == 0)) {
					plateau[9][3].valeurJeton = val;
					plateau[9][3].hero = hero;
					plateau[9][3].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 330) && (event.button.x < 420)
						&& (event.button.y > 560) && (event.button.y < 670)
						&& (plateau[9][7].hero == 0)) {
					plateau[9][7].valeurJeton = val;
					plateau[9][7].hero = hero;
					plateau[9][7].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 440) && (event.button.x < 530)
						&& (event.button.y > 560) && (event.button.y < 670)
						&& (plateau[9][9].hero == 0)) {
					plateau[9][9].valeurJeton = val;
					plateau[9][9].hero = hero;
					plateau[9][9].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 550) && (event.button.x < 630)
						&& (event.button.y > 560) && (event.button.y < 670)
						&& (plateau[9][11].hero == 0)) {
					plateau[9][11].valeurJeton = val;
					plateau[9][11].hero = hero;
					plateau[9][11].joueur = numero;
					continuer = 0;
					break;
				}
				if ((event.button.x > 770) && (event.button.x < 850)
						&& (event.button.y > 560) && (event.button.y < 670)
						&& (plateau[9][15].hero == 0)) {
					plateau[9][15].valeurJeton = val;
					plateau[9][15].hero = hero;
					plateau[9][15].joueur = numero;
					continuer = 0;
					break;
				}

			}
			break;
		}
	}
	Affichage(0);
	SDL_Delay(1000);

	return retour;
}

void Affichage(int surbrillance) {

	SDL_Surface *ecran = NULL, *imageDeFond = NULL, *zozor = NULL;
	SDL_Rect positionFond, positionZozor;
	positionFond.x = 0;
	positionFond.y = 0;
	positionZozor.x = 16;
	positionZozor.y = 110;

	SDL_Init(SDL_INIT_VIDEO);
	SDL_WM_SetIcon(SDL_LoadBMP("src/interface/image/plateau/Icon.bmp"), NULL);
	ecran = SDL_SetVideoMode(900, 684, 32, SDL_HWSURFACE);
	SDL_WM_SetCaption("Armadora", NULL);

	imageDeFond = SDL_LoadBMP("src/interface/image/plateau/Plateau.bmp");
	SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);

	SDL_Flip(ecran);
	TTF_Init();
	TTF_Font *policeChoix = NULL;
	SDL_Color couleurNoire = { 0, 0, 0 };
	policeChoix = TTF_OpenFont("src/interface/fonts/Subscribe.ttf", 65);
	SDL_Surface *texteOr = NULL;
	SDL_Rect positionOr;
	char mine[2];

	/*---------------------AFFICHAGE-------------------------------------*/

	/* Chargement de la police */

	sprintf(mine, "%d", plateau[9][13].orMine);
	texteOr = TTF_RenderText_Solid(policeChoix, mine, couleurNoire);
	positionOr.x = 680;
	positionOr.y = 580;
	SDL_BlitSurface(texteOr, NULL, ecran, &positionOr); /* Blit du texte */
	SDL_Flip(ecran);

	sprintf(mine, "%d", plateau[9][5].orMine);
	texteOr = TTF_RenderText_Solid(policeChoix, mine, couleurNoire);
	positionOr.x = 250;
	positionOr.y = 580;
	SDL_BlitSurface(texteOr, NULL, ecran, &positionOr); /* Blit du texte */
	SDL_Flip(ecran);

	sprintf(mine, "%d", plateau[7][9].orMine);
	texteOr = TTF_RenderText_Solid(policeChoix, mine, couleurNoire);
	positionOr.x = 460;
	positionOr.y = 470;
	SDL_BlitSurface(texteOr, NULL, ecran, &positionOr); /* Blit du texte */
	SDL_Flip(ecran);

	sprintf(mine, "%d", plateau[7][1].orMine);
	texteOr = TTF_RenderText_Solid(policeChoix, mine, couleurNoire);
	positionOr.x = 85;
	positionOr.y = 490;
	SDL_BlitSurface(texteOr, NULL, ecran, &positionOr); /* Blit du texte */
	SDL_Flip(ecran);

	sprintf(mine, "%d", plateau[3][15].orMine);
	texteOr = TTF_RenderText_Solid(policeChoix, mine, couleurNoire);
	positionOr.x = 790;
	positionOr.y = 260;
	SDL_BlitSurface(texteOr, NULL, ecran, &positionOr); /* Blit du texte */
	SDL_Flip(ecran);

	sprintf(mine, "%d", plateau[3][11].orMine);
	texteOr = TTF_RenderText_Solid(policeChoix, mine, couleurNoire);
	positionOr.x = 570;
	positionOr.y = 260;
	SDL_BlitSurface(texteOr, NULL, ecran, &positionOr); /* Blit du texte */
	SDL_Flip(ecran);

	sprintf(mine, "%d", plateau[3][3].orMine);
	texteOr = TTF_RenderText_Solid(policeChoix, mine, couleurNoire);
	positionOr.x = 195;
	positionOr.y = 290;
	SDL_BlitSurface(texteOr, NULL, ecran, &positionOr); /* Blit du texte */
	SDL_Flip(ecran);

	sprintf(mine, "%d", plateau[1][7].orMine);
	texteOr = TTF_RenderText_Solid(policeChoix, mine, couleurNoire);
	positionOr.x = 365;
	positionOr.y = 174;
	SDL_BlitSurface(texteOr, NULL, ecran, &positionOr); /* Blit du texte */
	SDL_Flip(ecran);

	// Affichage plateau
	/*--------LIGNE1----------*/
	if (plateau[1][1].valeurJeton != 0) {
		switch (plateau[1][1].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 30;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 30;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 30;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 30;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[1][3].valeurJeton != 0) {
		switch (plateau[1][3].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 140;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 140;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 140;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 140;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[1][5].valeurJeton != 0) {
		switch (plateau[1][5].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 248;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 248;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 248;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 248;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[1][7].valeurJeton != 0) {
		switch (plateau[1][7].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 355;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 355;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 355;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 355;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[1][9].valeurJeton != 0) {
		switch (plateau[1][9].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 465;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 465;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 465;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 465;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[1][11].valeurJeton != 0) {
		switch (plateau[1][11].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 572;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 572;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 572;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 572;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[1][13].valeurJeton != 0) {
		switch (plateau[1][13].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 680;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 680;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 680;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 680;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[1][15].valeurJeton != 0) {
		switch (plateau[1][15].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 787;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 787;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 787;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 787;
			positionZozor.y = 145;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	/*----------------Ligne2----------------------*/
	if (plateau[3][1].valeurJeton != 0) {
		switch (plateau[3][1].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 30;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 30;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 30;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 30;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[3][3].valeurJeton != 0) {
		switch (plateau[3][3].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 140;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 140;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 140;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 140;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[3][5].valeurJeton != 0) {
		switch (plateau[3][5].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 248;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 248;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 248;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 248;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[3][7].valeurJeton != 0) {
		switch (plateau[3][7].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 355;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 355;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 355;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 355;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[3][9].valeurJeton != 0) {
		switch (plateau[3][9].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 465;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 465;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 465;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 465;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[3][11].valeurJeton != 0) {
		switch (plateau[3][11].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 572;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 572;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 572;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 572;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[3][13].valeurJeton != 0) {
		switch (plateau[3][13].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 680;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 680;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 680;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 680;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[3][15].valeurJeton != 0) {
		switch (plateau[3][15].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 787;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 787;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 787;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 787;
			positionZozor.y = 250;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	/*---------------------ligne3-------------------------*/
	if (plateau[5][1].valeurJeton != 0) {
		switch (plateau[5][1].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 30;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 30;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 30;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 30;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[5][3].valeurJeton != 0) {
		switch (plateau[5][3].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 140;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 140;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 140;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 140;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[5][5].valeurJeton != 0) {
		switch (plateau[5][5].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 248;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 248;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 248;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 248;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[5][7].valeurJeton != 0) {
		switch (plateau[5][7].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 355;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 355;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 355;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 355;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[5][9].valeurJeton != 0) {
		switch (plateau[5][9].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 465;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 465;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 465;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 465;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[5][11].valeurJeton != 0) {
		switch (plateau[5][11].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 572;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 572;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 572;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 572;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[5][13].valeurJeton != 0) {
		switch (plateau[5][13].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 680;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 680;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 680;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 680;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[5][15].valeurJeton != 0) {
		switch (plateau[5][15].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 787;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 787;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 787;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 787;
			positionZozor.y = 355;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	/*-------------------ligne4---------------*/
	if (plateau[7][1].valeurJeton != 0) {
		switch (plateau[7][1].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 30;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 30;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 30;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 30;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[7][3].valeurJeton != 0) {
		switch (plateau[7][3].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 140;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 140;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 140;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 140;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[7][5].valeurJeton != 0) {
		switch (plateau[7][5].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 248;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 248;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 248;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 248;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[7][7].valeurJeton != 0) {
		switch (plateau[7][7].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 355;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 355;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 355;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 355;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[7][9].valeurJeton != 0) {
		switch (plateau[7][9].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 465;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 465;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 465;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 465;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[7][11].valeurJeton != 0) {
		switch (plateau[7][11].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 572;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 572;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 572;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 572;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[7][13].valeurJeton != 0) {
		switch (plateau[7][13].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 680;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 680;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 680;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 680;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[7][15].valeurJeton != 0) {
		switch (plateau[7][15].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 787;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 787;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 787;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 787;
			positionZozor.y = 465;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	/*------------------ligne5-----------------------------*/
	if (plateau[9][1].valeurJeton != 0) {
		switch (plateau[9][1].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 30;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 30;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 30;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 30;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[9][3].valeurJeton != 0) {
		switch (plateau[9][3].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 140;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 140;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 140;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 140;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[9][5].valeurJeton != 0) {
		switch (plateau[9][5].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 248;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 248;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 248;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 248;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[9][7].valeurJeton != 0) {
		switch (plateau[9][7].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 355;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 355;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 355;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 355;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[9][9].valeurJeton != 0) {
		switch (plateau[9][9].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 465;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 465;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 465;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 465;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[9][11].valeurJeton != 0) {
		switch (plateau[9][11].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 572;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 572;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 572;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 572;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[9][13].valeurJeton != 0) {
		switch (plateau[9][13].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 680;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 680;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 680;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 680;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	if (plateau[9][15].valeurJeton != 0) {
		switch (plateau[9][15].hero) {
		case 1:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero1.bmp");
			positionZozor.x = 787;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 2:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero2.bmp");
			positionZozor.x = 787;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 3:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero3.bmp");
			positionZozor.x = 787;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		case 4:
			zozor = SDL_LoadBMP("src/interface/image/plateau/hero4.bmp");
			positionZozor.x = 787;
			positionZozor.y = 575;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
			break;
		}
	}
	// Affichage mur
	/*------------------------------------LES BORDURES----------------------------------------------------*/
	/*----------------------------------BORDURE SUPERIEURE------------------------------------------------*/
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 15;
	positionZozor.y = 110;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 125;
	positionZozor.y = 110;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 235;
	positionZozor.y = 110;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 341;
	positionZozor.y = 110;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 449;
	positionZozor.y = 110;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 556;
	positionZozor.y = 110;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 665;
	positionZozor.y = 110;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 774;
	positionZozor.y = 110;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	/*----------------------------------BORDURE INFERIEURE------------------------------------------------*/
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 15;
	positionZozor.y = 640;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 125;
	positionZozor.y = 640;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 235;
	positionZozor.y = 640;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 341;
	positionZozor.y = 640;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 449;
	positionZozor.y = 640;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 556;
	positionZozor.y = 640;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 665;
	positionZozor.y = 640;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
	positionZozor.x = 774;
	positionZozor.y = 640;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	/*-----------------------------------BORDURE GAUCHE---------------------------------------------------*/
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 0;
	positionZozor.y = 120;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 0;
	positionZozor.y = 230;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 0;
	positionZozor.y = 340;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 0;
	positionZozor.y = 450;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 0;
	positionZozor.y = 550;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	/*-----------------------------------BORDURE DROITE---------------------------------------------------*/
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 868;
	positionZozor.y = 120;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 868;
	positionZozor.y = 230;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 868;
	positionZozor.y = 340;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 868;
	positionZozor.y = 450;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
	positionZozor.x = 868;
	positionZozor.y = 550;
	SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
			SDL_MapRGB(zozor->format, 255, 255, 255));
	SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
	/*----------------------------------FIN DES BORDURES--------------------------------------------------*/
	/*--------------------------------Horizontal------------------------------------------------*/
	/*----------------------------------ligne1--------------------------------------------------*/
	if (plateau[2][1].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 15;
		positionZozor.y = 210;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[2][3].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 125;
		positionZozor.y = 210;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[2][5].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 235;
		positionZozor.y = 210;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[2][7].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 341;
		positionZozor.y = 210;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[2][9].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 449;
		positionZozor.y = 210;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[2][11].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 556;
		positionZozor.y = 210;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[2][13].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 665;
		positionZozor.y = 210;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[2][15].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 771;
		positionZozor.y = 210;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	/*----------------------------------ligne2--------------------------------------------------*/
	if (plateau[4][1].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 15;
		positionZozor.y = 320;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[4][3].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 125;
		positionZozor.y = 320;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[4][5].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 235;
		positionZozor.y = 320;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[4][7].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 341;
		positionZozor.y = 320;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[4][9].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 449;
		positionZozor.y = 320;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[4][11].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 556;
		positionZozor.y = 320;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[4][13].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 665;
		positionZozor.y = 320;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[4][15].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 771;
		positionZozor.y = 320;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	/*----------------------------------ligne3--------------------------------------------------*/
	if (plateau[6][1].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 15;
		positionZozor.y = 430;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[6][3].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 125;
		positionZozor.y = 430;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[6][5].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 235;
		positionZozor.y = 430;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[6][7].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 341;
		positionZozor.y = 430;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[6][9].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 449;
		positionZozor.y = 430;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[6][11].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 556;
		positionZozor.y = 430;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[6][13].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 665;
		positionZozor.y = 430;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[6][15].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 771;
		positionZozor.y = 430;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	/*----------------------------------ligne4--------------------------------------------------*/
	if (plateau[8][1].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 15;
		positionZozor.y = 540;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[8][3].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 125;
		positionZozor.y = 540;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[8][5].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 235;
		positionZozor.y = 540;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[8][7].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 341;
		positionZozor.y = 540;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[8][9].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 449;
		positionZozor.y = 540;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[8][11].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 556;
		positionZozor.y = 540;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[8][13].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 665;
		positionZozor.y = 540;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[8][15].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
		positionZozor.x = 771;
		positionZozor.y = 540;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	/*---------------------------------Vertical-------------------------------------------------*/
	/*----------------------------------ligne1--------------------------------------------------*/
	if (plateau[1][2].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 115;
		positionZozor.y = 120;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[1][4].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 230;
		positionZozor.y = 120;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[1][6].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 335;
		positionZozor.y = 120;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[1][8].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 440;
		positionZozor.y = 120;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[1][10].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 550;
		positionZozor.y = 120;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[1][12].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 660;
		positionZozor.y = 120;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[1][14].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 770;
		positionZozor.y = 120;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	/*----------------------------------ligne2--------------------------------------------------*/
	if (plateau[3][2].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 115;
		positionZozor.y = 230;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[3][4].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 230;
		positionZozor.y = 230;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[3][6].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 335;
		positionZozor.y = 230;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[3][8].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 440;
		positionZozor.y = 230;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[3][10].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 550;
		positionZozor.y = 230;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[3][12].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 660;
		positionZozor.y = 230;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[3][14].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 770;
		positionZozor.y = 230;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	/*----------------------------------ligne3--------------------------------------------------*/
	if (plateau[5][2].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 115;
		positionZozor.y = 340;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[5][4].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 230;
		positionZozor.y = 340;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[5][6].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 335;
		positionZozor.y = 340;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[5][8].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 440;
		positionZozor.y = 340;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[5][10].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 550;
		positionZozor.y = 340;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[5][12].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 660;
		positionZozor.y = 340;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[5][14].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 770;
		positionZozor.y = 340;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	/*----------------------------------ligne4--------------------------------------------------*/
	if (plateau[7][2].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 115;
		positionZozor.y = 450;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[7][4].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 230;
		positionZozor.y = 450;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[7][6].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 335;
		positionZozor.y = 450;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[7][8].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 440;
		positionZozor.y = 450;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[7][10].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 550;
		positionZozor.y = 450;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[7][12].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 660;
		positionZozor.y = 450;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[7][14].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 770;
		positionZozor.y = 450;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	/*----------------------------------ligne5--------------------------------------------------*/
	if (plateau[9][2].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 115;
		positionZozor.y = 550;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[9][4].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 230;
		positionZozor.y = 550;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[9][6].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 335;
		positionZozor.y = 550;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[9][8].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 440;
		positionZozor.y = 550;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[9][10].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 550;
		positionZozor.y = 550;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[9][12].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 660;
		positionZozor.y = 550;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}
	if (plateau[9][14].mur == 1) {
		zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
		positionZozor.x = 770;
		positionZozor.y = 550;
		SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
				SDL_MapRGB(zozor->format, 255, 255, 255));
		SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
		SDL_Flip(ecran);
	}

	// MUR SURBRILLANCE

	if (surbrillance == 1) {
		int surb = 100;

		/*----------------------------------ligne1--------------------------------------------------*/
		if (plateau[2][1].mur == 0 && plateau[2][1].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 0;
			positionZozor.y = 210;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[2][3].mur == 0 && plateau[2][3].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 110;
			positionZozor.y = 210;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[2][5].mur == 0 && plateau[2][5].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 220;
			positionZozor.y = 210;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[2][7].mur == 0 && plateau[2][7].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 326;
			positionZozor.y = 210;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[2][9].mur == 0 && plateau[2][9].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 434;
			positionZozor.y = 210;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[2][11].mur == 0 && plateau[2][11].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 541;
			positionZozor.y = 210;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[2][13].mur == 0 && plateau[2][13].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 650;
			positionZozor.y = 210;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[2][15].mur == 0 && plateau[2][15].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 759;
			positionZozor.y = 210;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		/*----------------------------------ligne2--------------------------------------------------*/
		if (plateau[4][1].mur == 0 && plateau[4][1].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 0;
			positionZozor.y = 320;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[4][3].mur == 0 && plateau[4][3].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 110;
			positionZozor.y = 320;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[4][5].mur == 0 && plateau[4][5].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 220;
			positionZozor.y = 320;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[4][7].mur == 0 && plateau[4][7].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 326;
			positionZozor.y = 320;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[4][9].mur == 0 && plateau[4][9].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 434;
			positionZozor.y = 320;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[4][11].mur == 0 && plateau[4][11].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 541;
			positionZozor.y = 320;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[4][13].mur == 0 && plateau[4][13].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 650;
			positionZozor.y = 320;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[4][15].mur == 0 && plateau[4][15].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 759;
			positionZozor.y = 320;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		/*----------------------------------ligne3--------------------------------------------------*/
		if (plateau[6][1].mur == 0 && plateau[6][1].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 0;
			positionZozor.y = 430;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[6][3].mur == 0 && plateau[6][3].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 110;
			positionZozor.y = 430;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[6][5].mur == 0 && plateau[6][5].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 220;
			positionZozor.y = 430;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[6][7].mur == 0 && plateau[6][7].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 326;
			positionZozor.y = 430;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[6][9].mur == 0 && plateau[6][9].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 434;
			positionZozor.y = 430;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[6][11].mur == 0 && plateau[6][11].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 541;
			positionZozor.y = 430;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[6][13].mur == 0 && plateau[6][13].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 650;
			positionZozor.y = 430;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[6][15].mur == 0 && plateau[6][15].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 759;
			positionZozor.y = 430;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		/*----------------------------------ligne4--------------------------------------------------*/
		if (plateau[8][1].mur == 0 && plateau[8][1].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 0;
			positionZozor.y = 540;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[8][3].mur == 0 && plateau[8][3].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 110;
			positionZozor.y = 540;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[8][5].mur == 0 && plateau[8][5].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 220;
			positionZozor.y = 540;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[8][7].mur == 0 && plateau[8][7].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 326;
			positionZozor.y = 540;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[8][9].mur == 0 && plateau[8][9].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 434;
			positionZozor.y = 540;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[8][11].mur == 0 && plateau[8][11].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 541;
			positionZozor.y = 540;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[8][13].mur == 0 && plateau[8][13].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 650;
			positionZozor.y = 540;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[8][15].mur == 0 && plateau[8][15].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur3.bmp");
			positionZozor.x = 759;
			positionZozor.y = 540;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		/*---------------------------------Vertical-------------------------------------------------*/
		/*----------------------------------ligne1--------------------------------------------------*/
		if (plateau[1][2].mur == 0 && plateau[1][2].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 115;
			positionZozor.y = 120;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[1][4].mur == 0 && plateau[1][4].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 230;
			positionZozor.y = 120;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[1][6].mur == 0 && plateau[1][6].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 335;
			positionZozor.y = 120;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[1][8].mur == 0 && plateau[1][8].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 440;
			positionZozor.y = 120;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[1][10].mur == 0 && plateau[1][10].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 550;
			positionZozor.y = 120;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[1][12].mur == 0 && plateau[1][12].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 660;
			positionZozor.y = 120;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[1][14].mur == 0 && plateau[1][14].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 770;
			positionZozor.y = 120;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		/*----------------------------------ligne2--------------------------------------------------*/
		if (plateau[3][2].mur == 0 && plateau[3][2].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 115;
			positionZozor.y = 230;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[3][4].mur == 0 && plateau[3][4].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 230;
			positionZozor.y = 230;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[3][6].mur == 0 && plateau[3][6].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 335;
			positionZozor.y = 230;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[3][8].mur == 0 && plateau[3][8].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 440;
			positionZozor.y = 230;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[3][10].mur == 0 && plateau[3][10].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 550;
			positionZozor.y = 230;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[3][12].mur == 0 && plateau[3][12].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 660;
			positionZozor.y = 230;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[3][14].mur == 0 && plateau[3][14].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 770;
			positionZozor.y = 230;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		/*----------------------------------ligne3--------------------------------------------------*/
		if (plateau[5][2].mur == 0 && plateau[5][2].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 115;
			positionZozor.y = 340;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[5][4].mur == 0 && plateau[5][4].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 230;
			positionZozor.y = 340;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[5][6].mur == 0 && plateau[5][6].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 335;
			positionZozor.y = 340;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[5][8].mur == 0 && plateau[5][8].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 440;
			positionZozor.y = 340;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[5][10].mur == 0 && plateau[5][10].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 550;
			positionZozor.y = 340;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[5][12].mur == 0 && plateau[5][12].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 660;
			positionZozor.y = 340;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[5][14].mur == 0 && plateau[5][14].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 770;
			positionZozor.y = 340;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		/*----------------------------------ligne4--------------------------------------------------*/
		if (plateau[7][2].mur == 0 && plateau[7][2].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 115;
			positionZozor.y = 450;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[7][4].mur == 0 && plateau[7][4].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 230;
			positionZozor.y = 450;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[7][6].mur == 0 && plateau[7][6].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 335;
			positionZozor.y = 450;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[7][8].mur == 0 && plateau[7][8].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 440;
			positionZozor.y = 450;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[7][10].mur == 0 && plateau[7][10].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 550;
			positionZozor.y = 450;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[7][12].mur == 0 && plateau[7][12].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 660;
			positionZozor.y = 450;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[7][14].mur == 0 && plateau[7][14].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 770;
			positionZozor.y = 450;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		/*----------------------------------ligne5--------------------------------------------------*/
		if (plateau[9][2].mur == 0 && plateau[9][2].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 115;
			positionZozor.y = 550;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[9][4].mur == 0 && plateau[9][4].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 230;
			positionZozor.y = 550;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[9][6].mur == 0 && plateau[9][6].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 335;
			positionZozor.y = 550;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[9][8].mur == 0 && plateau[9][8].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 440;
			positionZozor.y = 550;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[9][10].mur == 0 && plateau[9][10].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 550;
			positionZozor.y = 550;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[9][12].mur == 0 && plateau[9][12].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 660;
			positionZozor.y = 550;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
		if (plateau[9][14].mur == 0 && plateau[9][14].flag_interdit == 0) {
			zozor = SDL_LoadBMP("src/interface/image/plateau/mur1.bmp");
			positionZozor.x = 770;
			positionZozor.y = 550;
			SDL_SetColorKey(zozor, SDL_SRCCOLORKEY,
					SDL_MapRGB(zozor->format, 255, 255, 255));
			SDL_SetAlpha(zozor, SDL_SRCALPHA, surb);
			SDL_BlitSurface(zozor, NULL, ecran, &positionZozor);
			SDL_Flip(ecran);
		}
	}
}
