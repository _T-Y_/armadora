/*
*	SDL image et SDL .h permettent de gérer les images.
*	SDL TTF permet de gere l'affichage texte
*/
#include "SDL/SDL.h"
#include "SDL/SDL_image.h" /* Inclusion du header de SDL_image (adapter le dossier au besoin) */
//#include<SDL/SDL_getenv.h>
#include <SDL/SDL_ttf.h> //  Son rôle est de créer uneSDL_Surfaceà partir du texte que vous lui envoyez.
#include "interface_graphique_menu.h"
#include "variables_globales.h"

/*
*	Simple affichage d'une image pour le lancement du jeux.
*/
void AffichageStart(void)
{	// Ecran de bienvenu
	/*
	*   Pointeur vers image
	*/
	SDL_Surface *ecran = NULL, *Fond = NULL; // Pointeur sur image
	
	/*
	*   Variable de positionnement
	*/
	SDL_Rect positionFond; // Position de l'image
    positionFond.x = 0;
    positionFond.y = 0;
    
    /*
    *   Initialisation
    */
    SDL_Init(SDL_INIT_VIDEO);// Init de la SDL
    
    /*
    *   Création de la fenetre
    */
    ecran = SDL_SetVideoMode(702, 678, 32, SDL_HWSURFACE | SDL_DOUBLEBUF); // Créé une fenetre
    
    /*
    *   Information de la fenetre
    */
    SDL_WM_SetCaption("Armadora", NULL);// Titre de la fenetre
    SDL_WM_SetIcon(IMG_Load("src/interface/image/image_menu/arma.bmp"), NULL); // Icon
    Fond = IMG_Load("src/interface/image/image_menu/arma.bmp");// Image de fond
    
    /*
    *   Affichage de la fenetre et de l'image
    */
    SDL_BlitSurface(Fond, NULL, ecran, &positionFond);// Copie de l'image sur la fenetre
    SDL_Flip(ecran);// restaure la fentre avec les copies d'images
	SDL_Delay(3000);
	
	SDL_FreeSurface(Fond); /* On libère la surface */
	SDL_FreeSurface(ecran); /* On libère la surface */
} 

/*
*	Cette fonction offre la possibilité de cliquer sur le nombre de joueur que comporte la partie.
*/
int AffichageChoixNbJoueur(void)
{
	// Chargement de base avec image de fond
	SDL_Surface *ecran = NULL, *Fond = NULL; // Créé un poiteur pour chaque image
	SDL_Event event; // Permet de gere les evenements
	SDL_Rect positionFond;// Permet de positionner l'image
    positionFond.x = 0;
    positionFond.y = 0;
    SDL_Init(SDL_INIT_VIDEO);// Initialise la SDL
    ecran = SDL_SetVideoMode(1000, 700, 32, SDL_HWSURFACE | SDL_DOUBLEBUF); // Créé une fenetre
    SDL_WM_SetCaption("Armadora", NULL);// Titre de la fenetre
    SDL_WM_SetIcon(IMG_Load("src/interface/image/image_menu/arma.bmp"), NULL); // Icon
    Fond = IMG_Load("src/interface/image/image_menu/FondMenu.bmp");// Image de fond
    
	SDL_BlitSurface(Fond, NULL, ecran, &positionFond);// Copie l'image sur la fenetre
	SDL_Flip(ecran);// restaure la fenetre pour generer les copies d'image
	// Message d'erreur si la TTF n'a pas pu se lancer
	if(TTF_Init() == -1)
	{
	    fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
	    exit(EXIT_FAILURE);
	}
	
	SDL_Surface *question = NULL, *deux = NULL, *trois = NULL, *quatre = NULL, *suivant = NULL, *inter = NULL; 
	TTF_Font *police = NULL, *policeChoix = NULL; // Pointeur pour le texte
	police = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 65);// Chargement d'une police
	policeChoix = TTF_OpenFont("src/interface/fonts/Subscribe.ttf", 65);// Idem
	// Si la police n'est pas trouvé, message d'erreur
	if (!police)
    {
    	printf("\nERREUR : chargement police\n");
	}
	SDL_Color couleurNoire = {0, 0, 0};// Créé une couleur d'écriture
	// Genere une phrase avec les parametre de police et de couleur
	question = TTF_RenderText_Solid(police, "Combien de joueurs etes-vous?", couleurNoire);
    // Genere des positions d'images
	SDL_Rect positionQuestion;
	SDL_Rect positionDeux;
	SDL_Rect positionTrois;
	SDL_Rect positionQuatre;
	SDL_Rect positionSuivant;
	// Initialisation des positions
	positionQuestion.x = 100;
	positionQuestion.y = 50;
	positionDeux.x = 500;
	positionDeux.y = 250;
	positionTrois.x = 500;
	positionTrois.y = 350;
	positionQuatre.x = 500;
	positionQuatre.y = 450;
	positionSuivant.x = 750;
	positionSuivant.y = 600;
	
	
	SDL_BlitSurface(question, NULL, ecran, &positionQuestion); /* Blit du texte */
	SDL_Flip(ecran); // rafraichit la fenetre
	
	int continuer = 1; // Variable de boucle while
	int nbJoueur = 0;// Sauvegarde le choix du nombre de joueur
	int bloc = 1;// Permet de faire une pose aprés avoir selectionné "Suivant"
	
	inter = IMG_Load("src/interface/image/image_menu/inter.bmp");
	
	while (continuer)
    {
    	/*
    	*	Si dessous, toutes les generations d'images en fonction des evenements, des variables
    	*	sur la fenetre. 
    	*/
    	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
        SDL_BlitSurface(Fond, NULL, ecran, &positionFond);
        SDL_BlitSurface(question, NULL, ecran, &positionQuestion); /* Blit du texte */
    	
        SDL_WaitEvent(&event);
        
        if(nbJoueur == 2)
        {
          	deux = TTF_RenderText_Solid(policeChoix, "2", couleurNoire);
           	SDL_BlitSurface(deux, NULL, ecran, &positionDeux); /* Blit du texte */
		}
		else
		{
			deux = TTF_RenderText_Solid(police, "2", couleurNoire);
            SDL_BlitSurface(deux, NULL, ecran, &positionDeux); /* Blit du texte */
		}
		
		if(nbJoueur == 3)
        {
            trois = TTF_RenderText_Solid(policeChoix, "3", couleurNoire);
            SDL_BlitSurface(trois, NULL, ecran, &positionTrois); /* Blit du texte */
		}
		else
		{
			trois = TTF_RenderText_Solid(police, "3", couleurNoire);
            SDL_BlitSurface(trois, NULL, ecran, &positionTrois); /* Blit du texte */
		}
		
		if(nbJoueur == 4)
        {
        	quatre = TTF_RenderText_Solid(policeChoix, "4", couleurNoire);
            SDL_BlitSurface(quatre, NULL, ecran, &positionQuatre); /* Blit du texte */
		}
		else
		{
			quatre = TTF_RenderText_Solid(police, "4", couleurNoire);
            SDL_BlitSurface(quatre, NULL, ecran, &positionQuatre); /* Blit du texte */
		}
        
		if(nbJoueur > 0 && bloc == 1)
		{
			suivant = TTF_RenderText_Solid(police, "Suivant", couleurNoire);
			SDL_BlitSurface(suivant, NULL, ecran, &positionSuivant);
		}
		if(nbJoueur > 0 && bloc == 0)
		{
			suivant = TTF_RenderText_Solid(policeChoix, "Suivant", couleurNoire);
			SDL_BlitSurface(suivant, NULL, ecran, &positionSuivant);
		}
		
		  	
        SDL_Flip(ecran);
        
        if(bloc==0) // Image de lancement du jeu
		{
			SDL_Delay(2000);
			SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
			SDL_BlitSurface(inter, NULL, ecran, &positionFond);
			SDL_Flip(ecran);
			SDL_Delay(2000);
			continuer = 0;
		}
        
        /*
        *	Si dessous, tous les evenements posibles avec la souris sur la fenetre.
        *	Chaque evenement renseigne une ou des variable(s)
        */
        switch(event.type) // Switch sur chaque evenement produit sur la fentre
        {
            case SDL_QUIT: // Bouton croix de la fenetre. Quit le jeu
                continuer = 0;
                SDL_FreeSurface(Fond); /* On libère la surface */
				SDL_FreeSurface(ecran);
				SDL_FreeSurface(question);
				SDL_FreeSurface(deux);
				SDL_FreeSurface(trois);
				SDL_FreeSurface(quatre);
				SDL_FreeSurface(suivant);
				SDL_FreeSurface(inter);
				SDL_Quit();//Fin SDL
                exit(1);// Quitte le programme
                break;
            case SDL_MOUSEBUTTONUP: /* Clic de la souris */
            	
            	/*
            	* Evenement en fonction des images nombres de joueur sur l'ecran
            	*/
            	if((event.button.x > positionDeux.x) && (event.button.x < (positionDeux.x + 50))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionDeux.y) && (event.button.y < (positionDeux.y + 50)))
            		{
						nbJoueur = 2;
					}
				}
				if((event.button.x > positionTrois.x) && (event.button.x < (positionTrois.x + 50))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionTrois.y) && (event.button.y < (positionTrois.y + 50)))
            		{
						nbJoueur = 3;
					}
				}
				if((event.button.x > positionQuatre.x) && (event.button.x < (positionQuatre.x + 50))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionQuatre.y) && (event.button.y < (positionQuatre.y + 50)))
            		{
						nbJoueur = 4;
					}
				}
				
				// Bouton Suivant, permet de lancer le jeu apres avoir selectionné le nb de joueur
				
				if((event.button.x > positionSuivant.x) && (event.button.x < (positionSuivant.x + 200))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionSuivant.y) && (event.button.y < (positionSuivant.y + 100)))
            		{
						bloc = 0;
					}
				}
            	
        		break;
        }
    	
        SDL_Flip(ecran);
    }
	
	SDL_FreeSurface(Fond); /* On libère la surface */
	SDL_FreeSurface(ecran);
	SDL_FreeSurface(question);
	SDL_FreeSurface(deux);
	SDL_FreeSurface(trois);
	SDL_FreeSurface(quatre);
	SDL_FreeSurface(suivant);
	SDL_FreeSurface(inter);
	
	return nbJoueur;
}

/*
*	Ci dessous, cette fonction fait apparaitre une ligne pour chaque joueur, avec le choix
*	du heros, du type(joueur ou IA) ainsi que l'equipe si 4 joueurs.
*/
void AffichageDataJoueur(int nbJoueur)
{
	// Ecran de bienvenu
	SDL_Surface *ecran = NULL, *Fond = NULL;// Poiteur sur image
	SDL_Event event;//Permet evenement
	SDL_Rect positionFond; // position de l'image
    positionFond.x = 0;
    positionFond.y = 0;
    
    SDL_Init(SDL_INIT_VIDEO);// SDL Init
    
   	ecran = SDL_SetVideoMode(1000, 700, 32, SDL_HWSURFACE | SDL_DOUBLEBUF); // Création de la fenetre
    
    SDL_WM_SetCaption("Armadora", NULL);// Titre fenetre
    SDL_WM_SetIcon(IMG_Load("src/interface/image/image_menu/arma.bmp"), NULL); // Icon
    Fond = IMG_Load("src/interface/image/image_menu/FondMenu.bmp");// Image de fond
    
    SDL_BlitSurface(Fond, NULL, ecran, &positionFond);// Copie l'image sur la fenetre.
    SDL_Flip(ecran);// rafraichie la fenetre avec les images copiés
	// Création des pointeurs de chaque image qui apparaitra sur l'ecran
	SDL_Surface *mageJ1 = NULL, *mageJ2 = NULL, *mageJ3 = NULL, *mageJ4 = NULL;
	SDL_Surface *elfJ1 = NULL, *elfJ2 = NULL, *elfJ3 = NULL, *elfJ4 = NULL;
	SDL_Surface *orkJ1 = NULL, *orkJ2 = NULL, *orkJ3 = NULL, *orkJ4 = NULL;
	SDL_Surface *trollJ1 = NULL, *trollJ2 = NULL, *trollJ3 = NULL, *trollJ4 = NULL;
	SDL_Surface *type1J = NULL, *type1IA = NULL,*type2J = NULL, *type2IA = NULL,*type3J = NULL, *type3IA = NULL,*type4J = NULL, *type4IA = NULL;
	SDL_Surface *equipe1J1 = NULL,*equipe2J1 = NULL,*equipe1J2 = NULL,*equipe2J2 = NULL,*equipe1J3 = NULL,*equipe2J3 = NULL,*equipe1J4 = NULL,*equipe2J4 = NULL;
	
	// TTF Init, message si erreur
   	if(TTF_Init() == -1)
	{
	    fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
	    exit(EXIT_FAILURE);
	}
	// Pointeur sur image
	SDL_Surface *TYPE = NULL, *EQUIPE = NULL, *HERO = NULL;
	SDL_Surface *un = NULL, *deux = NULL, *trois = NULL, *quatre = NULL;
	SDL_Surface *ia = NULL,*j = NULL,*e1 =NULL, *e2 = NULL,*suivant = NULL,*dem = NULL;
	// Structure de position avec x et y
	SDL_Rect positionia;
	SDL_Rect positionj;
	SDL_Rect positione1;
	SDL_Rect positione2;
	SDL_Rect positionsuivant;
	SDL_Rect positiondem;
	
	// Init position
	positiondem.x = 0;
	positiondem.y = 0;
	
	positionsuivant.x = 50;
	positionsuivant.y = 630;
	
	positionia.x = 680;
	positionia.y = 610;
	positionj.x = 600;
	positionj.y = 610;
	positione1.x = 830;
	positione1.y = 610;
	positione2.x = 910;
	positione2.y = 610;
	
	// pointeur sur police
	TTF_Font *police = NULL, *policeChoix = NULL;
	police = TTF_OpenFont("src/interface/fonts/SubscribeShadow.ttf", 65);// Chargement de la police
	policeChoix = TTF_OpenFont("src/interface/fonts/Subscribe.ttf", 65);// idem
	// Si probleme de chargement de la police
	if (!police)
    {
    	printf("\nERREUR : chargement police\n");
	}
	SDL_Color couleurNoire = {0, 0, 0};
	// Création du texte sur les pointeur, avec police, couleur et phrase.
	ia=TTF_RenderText_Solid(police, "IA", couleurNoire);
	j=TTF_RenderText_Solid(police, "J", couleurNoire);
	e1=TTF_RenderText_Solid(police, "1", couleurNoire);
	e2=TTF_RenderText_Solid(police, "2", couleurNoire);
	
	TYPE = TTF_RenderText_Solid(police, "TYPE", couleurNoire);
	EQUIPE = TTF_RenderText_Solid(police, "EQUIPE", couleurNoire);
	HERO = TTF_RenderText_Solid(police, "HERO", couleurNoire);
	
	un = TTF_RenderText_Solid(police, "1", couleurNoire);
	deux = TTF_RenderText_Solid(police, "2", couleurNoire);
	trois = TTF_RenderText_Solid(police, "3", couleurNoire);
	quatre = TTF_RenderText_Solid(police, "4", couleurNoire);
	// Creation de structure de position 
	SDL_Rect positionType;
	SDL_Rect positionEquipe;
	SDL_Rect positionHero;
	// Init position
	positionHero.x = 250;
	positionHero.y = 50;
	positionType.x = 600;
	positionType.y = 50;
	positionEquipe.x = 800;
	positionEquipe.y = 50;
	
	SDL_Rect positionUn;
	SDL_Rect positionDeux;
	SDL_Rect positionTrois;
	SDL_Rect positionQuatre;
	
	positionUn.x = 20;
	positionUn.y = 160;
	positionDeux.x = 20;
	positionDeux.y = 280;
	positionTrois.x = 20;
	positionTrois.y = 400;
	positionQuatre.x = 20;
	positionQuatre.y = 520;
	// Copie des images sur la fentre
	SDL_BlitSurface(Fond, NULL, ecran, &positionFond);
    SDL_BlitSurface(TYPE, NULL, ecran, &positionType);
	SDL_BlitSurface(HERO , NULL, ecran, &positionHero);
	SDL_BlitSurface(EQUIPE, NULL, ecran, &positionEquipe);
		
	SDL_Delay(100); // Petite delay pour laissé à l'ordinateur un peut de temps.
	SDL_Flip(ecran);// rafraichissement de la fenetre
	dem = IMG_Load("src/interface/image/image_menu/lancement.bmp"); // Image de lancement du jeu
	
//****************************************************************************
//							Image Mage
//****************************************************************************
	/*
	*	Au maximum, il y a 4 images de mages qui apparaitront.
	*	Une pour chaque joueur.
	*/

	SDL_Rect positionMageJ1;
	SDL_Rect positionMageJ2;
	SDL_Rect positionMageJ3;
	SDL_Rect positionMageJ4;
	positionMageJ1.x = 80;
	positionMageJ1.y = 140;
	positionMageJ2.x = 80;
	positionMageJ2.y = 260;
	positionMageJ3.x = 80;
	positionMageJ3.y = 380;
	positionMageJ4.x = 80;
	positionMageJ4.y = 500;
	mageJ1 = IMG_Load("src/interface/image/image_menu/Mage.png");
	mageJ2 = IMG_Load("src/interface/image/image_menu/Mage.png");
	mageJ3 = IMG_Load("src/interface/image/image_menu/Mage.png");
	mageJ4 = IMG_Load("src/interface/image/image_menu/Mage.png");
	SDL_BlitSurface(mageJ1, NULL, ecran, &positionMageJ1);
	SDL_BlitSurface(mageJ2, NULL, ecran, &positionMageJ2);
	SDL_BlitSurface(mageJ3, NULL, ecran, &positionMageJ3);
	SDL_BlitSurface(mageJ4, NULL, ecran, &positionMageJ4);
	SDL_Flip(ecran);
	SDL_Delay(10);
	
//****************************************************************************
//							Image Elf
//****************************************************************************
	
	/*
	*	Au maximum, il y a 4 images d'elf qui apparaitront.
	*	Une pour chaque joueur.
	*/
	SDL_Rect positionElfJ1;
	SDL_Rect positionElfJ2;
	SDL_Rect positionElfJ3;
	SDL_Rect positionElfJ4;
	positionElfJ1.x = 440;
	positionElfJ1.y = 140;
	positionElfJ2.x = 440;
	positionElfJ2.y = 260;
	positionElfJ3.x = 440;
	positionElfJ3.y = 380;
	positionElfJ4.x = 440;
	positionElfJ4.y = 500;
	elfJ1 = IMG_Load("src/interface/image/image_menu/ElfM.png");
	elfJ2 = IMG_Load("src/interface/image/image_menu/ElfM.png");
	elfJ3 = IMG_Load("src/interface/image/image_menu/ElfM.png");
	elfJ4 = IMG_Load("src/interface/image/image_menu/ElfM.png");
	SDL_BlitSurface(elfJ1, NULL, ecran, &positionElfJ1);
	SDL_BlitSurface(elfJ2, NULL, ecran, &positionElfJ2);
	SDL_BlitSurface(elfJ3, NULL, ecran, &positionElfJ3);
	SDL_BlitSurface(elfJ4, NULL, ecran, &positionElfJ4);

	SDL_Delay(10);
	
//****************************************************************************
//							Image Ork
//****************************************************************************
	/*
	*	Au maximum, il y a 4 images d'Orks qui apparaitront.
	*	Une pour chaque joueur.
	*/
	SDL_Rect positionOrkJ1;
	SDL_Rect positionOrkJ2;
	SDL_Rect positionOrkJ3;
	SDL_Rect positionOrkJ4;
	positionOrkJ1.x = 200;
	positionOrkJ1.y = 140;
	positionOrkJ2.x = 200;
	positionOrkJ2.y = 260;
	positionOrkJ3.x = 200;
	positionOrkJ3.y = 380;
	positionOrkJ4.x = 200;
	positionOrkJ4.y = 500;
	orkJ1 = IMG_Load("src/interface/image/image_menu/Ork.png");
	orkJ2 = IMG_Load("src/interface/image/image_menu/Ork.png");
	orkJ3 = IMG_Load("src/interface/image/image_menu/Ork.png");
	orkJ4 = IMG_Load("src/interface/image/image_menu/Ork.png");
	SDL_BlitSurface(orkJ1, NULL, ecran, &positionOrkJ1);
	SDL_BlitSurface(orkJ2, NULL, ecran, &positionOrkJ2);
	SDL_BlitSurface(orkJ3, NULL, ecran, &positionOrkJ3);
	SDL_BlitSurface(orkJ4, NULL, ecran, &positionOrkJ4);
	SDL_Delay(10);
	
//****************************************************************************
//							Image Troll
//****************************************************************************
	
	/*
	*	Au maximum, il y a 4 images de Trolls qui apparaitront.
	*	Une pour chaque joueur.
	*/
	SDL_Rect positionTrollJ1;
	SDL_Rect positionTrollJ2;
	SDL_Rect positionTrollJ3;
	SDL_Rect positionTrollJ4;
	positionTrollJ1.x = 320;
	positionTrollJ1.y = 140;
	positionTrollJ2.x = 320;
	positionTrollJ2.y = 260;
	positionTrollJ3.x = 320;
	positionTrollJ3.y = 380;
	positionTrollJ4.x = 320;
	positionTrollJ4.y = 500;
	trollJ1 = IMG_Load("src/interface/image/image_menu/Troll.png");
	trollJ2 = IMG_Load("src/interface/image/image_menu/Troll.png");
	trollJ3 = IMG_Load("src/interface/image/image_menu/Troll.png");
	trollJ4 = IMG_Load("src/interface/image/image_menu/Troll.png");
	SDL_BlitSurface(trollJ1, NULL, ecran, &positionTrollJ1);
	SDL_BlitSurface(trollJ2, NULL, ecran, &positionTrollJ2);
	SDL_BlitSurface(trollJ3, NULL, ecran, &positionTrollJ3);
	SDL_BlitSurface(trollJ4, NULL, ecran, &positionTrollJ4);
	SDL_Delay(10);
	
	
	SDL_Flip(ecran);

//****************************************************************************
//							Image Type Joueur
//****************************************************************************

	/*
	*	Au maximum, il y a 4 images du choix du type qui apparaitront.
	*	Une pour chaque joueur.
	*/
	SDL_Rect positionType1J;
	SDL_Rect positionType2J;
	SDL_Rect positionType3J;
	SDL_Rect positionType4J;
	positionType1J.x = 600;
	positionType1J.y = 160;
	positionType2J.x = 600;
	positionType2J.y = 280;
	positionType3J.x = 600;
	positionType3J.y = 400;
	positionType4J.x = 600;
	positionType4J.y = 520;
	
	
//****************************************************************************
//							Image Type IA
//****************************************************************************
	
	/*
	*	Au maximum, il y a 4 images du choix du type qui apparaitront.
	*	Une pour chaque joueur.
	*/
	SDL_Rect positionType1IA;
	SDL_Rect positionType2IA;
	SDL_Rect positionType3IA;
	SDL_Rect positionType4IA;
	positionType1IA.x = 680;
	positionType1IA.y = 160;
	positionType2IA.x = 680;
	positionType2IA.y = 280;
	positionType3IA.x = 680;
	positionType3IA.y = 400;
	positionType4IA.x = 680;
	positionType4IA.y = 520;
	
//****************************************************************************
//							Image Equipe1
//****************************************************************************
	
	/*
	*	Au maximum, il y a 4 images du choix d'equipe 1 qui apparaitront.
	*	Une pour chaque joueur.
	*/
	SDL_Rect positionEquipe1J1;
	SDL_Rect positionEquipe1J2;
	SDL_Rect positionEquipe1J3;
	SDL_Rect positionEquipe1J4;
	positionEquipe1J1.x = 820;
	positionEquipe1J1.y = 160;
	positionEquipe1J2.x = 820;
	positionEquipe1J2.y = 280;
	positionEquipe1J3.x = 820;
	positionEquipe1J3.y = 400;
	positionEquipe1J4.x = 820;
	positionEquipe1J4.y = 520;

//****************************************************************************
//							Image Equipe2
//****************************************************************************
	
	/*
	*	Au maximum, il y a 4 images du choix d'equipe 2 qui apparaitront.
	*	Une pour chaque joueur.
	*/
	SDL_Rect positionEquipe2J1;
	SDL_Rect positionEquipe2J2;
	SDL_Rect positionEquipe2J3;
	SDL_Rect positionEquipe2J4;
	positionEquipe2J1.x = 900;
	positionEquipe2J1.y = 160;
	positionEquipe2J2.x = 900;
	positionEquipe2J2.y = 280;
	positionEquipe2J3.x = 900;
	positionEquipe2J3.y = 400;
	positionEquipe2J4.x = 900;
	positionEquipe2J4.y = 520;
	
	/*
	*	Ci dessous, toutes les initialisations des variables pour gerer les images et les données des evenements
	*	créé par la sourie.
	*/
	
	int continuer = 1;
	int bloc = 1;
	
	int blocM1, blocM2, blocM3, blocM4;
	blocM1 = 0;
	blocM2 = 0;
	blocM3 = 0;
	blocM4 = 0;
	int blocO1, blocO2, blocO3, blocO4;
	blocO1 = 0;
	blocO2 = 0;
	blocO3 = 0;
	blocO4 = 0;
	int blocT1, blocT2, blocT3, blocT4;
	blocT1 = 0;
	blocT2 = 0;
	blocT3 = 0;
	blocT4 = 0;
	int blocE1, blocE2, blocE3, blocE4;
	blocE1 = 0;
	blocE2 = 0;
	blocE3 = 0;
	blocE4 = 0;
	
	int J1,J2,J3,J4;
	J1 = 0;
	J2 = 0;
	J3 = 0;
	J4 = 0;
	
	int A1,A2,A3,A4;
	A1 = 0;
	A2 = 0,
	A3 = 0;
	A4 = 0;
	
	int E1J1,E1J2,E1J3,E1J4;
	E1J1=0;
	E1J2=0;
	E1J3=0;
	E1J4=0;
	
	int E2J1,E2J2,E2J3,E2J4;
	E2J1=0;
	E2J2=0;
	E2J3=0;
	E2J4=0;
	
	int nb1,nb2;
	nb1 = 0;
	nb2 = 0;
	
	int info = 0;
	int info1,info2,info3,info4;
	info1 = 0;
	info2 = 0;
	info3 = 0;
	info4 = 0;
	int infoJ1, infoJ2, infoJ3, infoJ4;
	infoJ1 = 0;
	infoJ2 = 0;
	infoJ3 = 0;
	infoJ4 = 0;
	int infoT1, infoT2,infoT3,infoT4;
	infoT1 = 0;
	infoT2 = 0;
	infoT3 = 0;
	infoT4 = 0;
	int infoE1, infoE2, infoE3, infoE4;
	infoE1 = 0;
	infoE2 = 0;
	infoE3 = 0;
	infoE4 = 0;
	
	int transparance = 100; // Transparance des images de heros non selectionné
	
	int affichage = 1;
	// Boucle while pour evenements
	while (continuer)
    {
    	if(affichage == 1) // A chaque evenement, on rafraichit la fentre en fonction des variables
    	{
			// On efface l'écran 
	    	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
		    // On remet les images 
	        SDL_BlitSurface(Fond, NULL, ecran, &positionFond);
	        SDL_BlitSurface(TYPE, NULL, ecran, &positionType);
			SDL_BlitSurface(HERO , NULL, ecran, &positionHero);
			SDL_BlitSurface(un, NULL, ecran, &positionUn);
			SDL_BlitSurface(deux, NULL, ecran, &positionDeux);
			SDL_BlitSurface(ia, NULL, ecran, &positionia);
			SDL_BlitSurface(j, NULL, ecran, &positionj);
			
			if(nbJoueur == 2 && info == 4)
			{
				if(bloc == 1)
				{
					suivant=TTF_RenderText_Solid(police, "Suivant", couleurNoire);
					SDL_BlitSurface(suivant, NULL, ecran, &positionsuivant);
				}
				else
				{
					suivant=TTF_RenderText_Solid(policeChoix, "Suivant", couleurNoire);
					SDL_BlitSurface(suivant, NULL, ecran, &positionsuivant);
				}
			}
			if(nbJoueur == 3 && info == 6)
			{
				if(bloc == 1)
				{
					suivant=TTF_RenderText_Solid(police, "Suivant", couleurNoire);
					SDL_BlitSurface(suivant, NULL, ecran, &positionsuivant);
				}
				else
				{
					suivant=TTF_RenderText_Solid(policeChoix, "Suivant", couleurNoire);
					SDL_BlitSurface(suivant, NULL, ecran, &positionsuivant);
				}
			}
			if(nbJoueur == 4 && info == 12)
			{
				if(bloc == 1)
				{
					suivant=TTF_RenderText_Solid(police, "Suivant", couleurNoire);
					SDL_BlitSurface(suivant, NULL, ecran, &positionsuivant);
				}
				else
				{
					suivant=TTF_RenderText_Solid(policeChoix, "Suivant", couleurNoire);
					SDL_BlitSurface(suivant, NULL, ecran, &positionsuivant);
				}
			}
			
			
			if((nbJoueur > 2))
			{
				SDL_BlitSurface(trois, NULL, ecran, &positionTrois);
			}
			if(nbJoueur == 4)
			{
				SDL_BlitSurface(quatre, NULL, ecran, &positionQuatre);
				SDL_BlitSurface(e1, NULL, ecran, &positione1);
				SDL_BlitSurface(e2, NULL, ecran, &positione2);
			}
			
	    	
	        if(nbJoueur == 4)
	        {
	        	SDL_BlitSurface(EQUIPE, NULL, ecran, &positionEquipe);
			}
			
			SDL_Flip(ecran);
			
			// 1=Mage, 2=Ork, 3=Troll, 4=Elf
			
			//----------------------------------------------------------------Image Mage
			if((blocM2 == 0) && (blocM3 == 0) && (blocM4 == 0) && (blocM1 == 0))
			{
				SDL_SetAlpha(mageJ1, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(mageJ1, NULL, ecran, &positionMageJ1);
				
				SDL_SetAlpha(mageJ2, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(mageJ2, NULL, ecran, &positionMageJ2);
				if(nbJoueur > 2)
				{
					SDL_SetAlpha(mageJ3, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(mageJ3, NULL, ecran, &positionMageJ3);	
				}
				if(nbJoueur == 4)
				{
					SDL_SetAlpha(mageJ4, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(mageJ4, NULL, ecran, &positionMageJ4);
				}
			}
			else if(blocM1 == 1)
			{
				SDL_SetAlpha(mageJ1, SDL_SRCALPHA, 255);
				SDL_BlitSurface(mageJ1, NULL, ecran, &positionMageJ1);
			}
			else if(blocM2 == 1)
			{
				SDL_SetAlpha(mageJ2, SDL_SRCALPHA, 255);
				SDL_BlitSurface(mageJ2, NULL, ecran, &positionMageJ2);
			}
			else if(blocM3 == 1 && nbJoueur > 2)
			{
				SDL_SetAlpha(mageJ3, SDL_SRCALPHA, 255);
				SDL_BlitSurface(mageJ3, NULL, ecran, &positionMageJ3);
			}
			else //(blocM4 == 1 && nbJoueur == 4)
			{
				SDL_SetAlpha(mageJ4, SDL_SRCALPHA, 255);
				SDL_BlitSurface(mageJ4, NULL, ecran, &positionMageJ4);
			}
			
			//----------------------------------------------------------------Image Ork
			if((blocO2 == 0) && (blocO3 == 0) && (blocO4 == 0) && (blocO1 == 0))
			{
				SDL_SetAlpha(orkJ1, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(orkJ1, NULL, ecran, &positionOrkJ1);
				
				SDL_SetAlpha(orkJ2, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(orkJ2, NULL, ecran, &positionOrkJ2);
				if(nbJoueur > 2)
				{
					SDL_SetAlpha(orkJ3, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(orkJ3, NULL, ecran, &positionOrkJ3);	
				}
				if(nbJoueur == 4)
				{
					SDL_SetAlpha(orkJ4, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(orkJ4, NULL, ecran, &positionOrkJ4);
				}
			}
			else if(blocO1 == 1)
			{
				SDL_SetAlpha(orkJ1, SDL_SRCALPHA, 255);
				SDL_BlitSurface(orkJ1, NULL, ecran, &positionOrkJ1);
			}
			else if(blocO2 == 1)
			{
				SDL_SetAlpha(orkJ2, SDL_SRCALPHA, 255);
				SDL_BlitSurface(orkJ2, NULL, ecran, &positionOrkJ2);
			}
			else if(blocO3 == 1 && nbJoueur > 2)
			{
				SDL_SetAlpha(orkJ3, SDL_SRCALPHA, 255);
				SDL_BlitSurface(orkJ3, NULL, ecran, &positionOrkJ3);
			}
			else //(blocO4 == 1 && nbJoueur == 4)
			{
				SDL_SetAlpha(orkJ4, SDL_SRCALPHA, 255);
				SDL_BlitSurface(orkJ4, NULL, ecran, &positionOrkJ4);
			}
			
			//----------------------------------------------------------------Image Troll
			if((blocT2 == 0) && (blocT3 == 0) && (blocT4 == 0) && (blocT1 == 0))
			{
				SDL_SetAlpha(trollJ1, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(trollJ1, NULL, ecran, &positionTrollJ1);
				
				SDL_SetAlpha(trollJ2, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(trollJ2, NULL, ecran, &positionTrollJ2);
				if(nbJoueur > 2)
				{
					SDL_SetAlpha(trollJ3, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(trollJ3, NULL, ecran, &positionTrollJ3);	
				}
				if(nbJoueur == 4)
				{
					SDL_SetAlpha(trollJ4, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(trollJ4, NULL, ecran, &positionTrollJ4);
				}
			}
			else if(blocT1 == 1)
			{
				SDL_SetAlpha(trollJ1, SDL_SRCALPHA, 255);
				SDL_BlitSurface(trollJ1, NULL, ecran, &positionTrollJ1);
			}
			else if(blocT2 == 1)
			{
				SDL_SetAlpha(trollJ2, SDL_SRCALPHA, 255);
				SDL_BlitSurface(trollJ2, NULL, ecran, &positionTrollJ2);
			}
			else if(blocT3 == 1 && nbJoueur > 2)
			{
				SDL_SetAlpha(trollJ3, SDL_SRCALPHA, 255);
				SDL_BlitSurface(trollJ3, NULL, ecran, &positionTrollJ3);
			}
			else //(blocT4 == 1 && nbJoueur == 4)
			{
				SDL_SetAlpha(trollJ4, SDL_SRCALPHA, 255);
				SDL_BlitSurface(trollJ4, NULL, ecran, &positionTrollJ4);
			}
			
			//----------------------------------------------------------------Image Elf
			if((blocE2 == 0) && (blocE3 == 0) && (blocE4 == 0) && (blocE1 == 0))
			{
				SDL_SetAlpha(elfJ1, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(elfJ1, NULL, ecran, &positionElfJ1);
				
				SDL_SetAlpha(elfJ2, SDL_SRCALPHA, transparance);
				SDL_BlitSurface(elfJ2, NULL, ecran, &positionElfJ2);
				if(nbJoueur > 2)
				{
					SDL_SetAlpha(elfJ3, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(elfJ3, NULL, ecran, &positionElfJ3);	
				}
				if(nbJoueur == 4)
				{
					SDL_SetAlpha(elfJ4, SDL_SRCALPHA, transparance);
					SDL_BlitSurface(elfJ4, NULL, ecran, &positionElfJ4);
				}
			}
			else if(blocE1 == 1)
			{
				SDL_SetAlpha(elfJ1, SDL_SRCALPHA, 255);
				SDL_BlitSurface(elfJ1, NULL, ecran, &positionElfJ1);
			}
			else if(blocE2 == 1)
			{
				SDL_SetAlpha(elfJ2, SDL_SRCALPHA, 255);
				SDL_BlitSurface(elfJ2, NULL, ecran, &positionElfJ2);
			}
			else if(blocE3 == 1 && nbJoueur > 2)
			{
				SDL_SetAlpha(elfJ3, SDL_SRCALPHA, 255);
				SDL_BlitSurface(elfJ3, NULL, ecran, &positionElfJ3);
			}
			else //(blocE4 == 1 && nbJoueur == 4)
			{
				SDL_SetAlpha(elfJ4, SDL_SRCALPHA, 255);
				SDL_BlitSurface(elfJ4, NULL, ecran, &positionElfJ4);
			}
			
			
			// ________________________________________________________ Joueur ou IA
			
			if(J1 == 1)
			{	
				type1J = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
				SDL_BlitSurface(type1J, NULL, ecran, &positionType1J);
			}
			else
			{		
				type1J = TTF_RenderText_Solid(police, "X", couleurNoire);
				SDL_BlitSurface(type1J, NULL, ecran, &positionType1J);
			}
			if(J2 == 1)
			{	
				type2J = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
				SDL_BlitSurface(type2J, NULL, ecran, &positionType2J);
			}
			else
			{		
				type2J = TTF_RenderText_Solid(police, "X", couleurNoire);
				SDL_BlitSurface(type2J, NULL, ecran, &positionType2J);
			}
			if(J3 == 1)
			{	
				type3J = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
				SDL_BlitSurface(type3J, NULL, ecran, &positionType3J);
			}
			else
			{	
				if(nbJoueur > 2)
				{
					type3J = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(type3J, NULL, ecran, &positionType3J);
				}	
			}
			if(J4 == 1)
			{	
				type4J = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
				SDL_BlitSurface(type4J, NULL, ecran, &positionType4J);		
			}
			else
			{	
				if(nbJoueur == 4)
				{
					type4J = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(type4J, NULL, ecran, &positionType4J);
				}		
			}
			
			if(A1 == 1)
			{	
				type1IA = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
				SDL_BlitSurface(type1IA, NULL, ecran, &positionType1IA);
			}
			else
			{		
				type1IA = TTF_RenderText_Solid(police, "X", couleurNoire);
				SDL_BlitSurface(type1IA, NULL, ecran, &positionType1IA);
			}
			if(A2 == 1)
			{	
				type2IA = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
				SDL_BlitSurface(type2IA, NULL, ecran, &positionType2IA);
			}
			else
			{		
				type2IA = TTF_RenderText_Solid(police, "X", couleurNoire);
				SDL_BlitSurface(type2IA, NULL, ecran, &positionType2IA);
			}
			if(A3 == 1)
			{	
				type3IA = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
				SDL_BlitSurface(type3IA, NULL, ecran, &positionType3IA);
			}
			else
			{	
				if(nbJoueur > 2)
				{
					type3IA = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(type3IA, NULL, ecran, &positionType3IA);
				}	
			}
			if(A4 == 1)
			{	
				type4IA = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
				SDL_BlitSurface(type4IA, NULL, ecran, &positionType4IA);		
			}
			else
			{	
				if(nbJoueur == 4)
				{
					type4IA = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(type4IA, NULL, ecran, &positionType4IA);
				}		
			}
			
			//--------------------------------------------------------------------- Equipe 1 ou 2?
			if(nbJoueur == 4)
			{
				if(E1J1 == 1)
				{
					equipe1J1 = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
					SDL_BlitSurface(equipe1J1, NULL, ecran, &positionEquipe1J1);
				}
				else
				{
					equipe1J1 = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(equipe1J1, NULL, ecran, &positionEquipe1J1);
				}
				if(E1J2 == 1)
				{
					equipe1J2 = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
					SDL_BlitSurface(equipe1J2, NULL, ecran, &positionEquipe1J2);
				}
				else
				{
					equipe1J2 = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(equipe1J2, NULL, ecran, &positionEquipe1J2);
				}
				if(E1J3 == 1)
				{
					equipe1J3 = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
					SDL_BlitSurface(equipe1J3, NULL, ecran, &positionEquipe1J3);
				}
				else
				{
					equipe1J3 = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(equipe1J3, NULL, ecran, &positionEquipe1J3);
				}
				if(E1J4 == 1)
				{
					equipe1J4 = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
					SDL_BlitSurface(equipe1J4, NULL, ecran, &positionEquipe1J4);
				}
				else
				{
					equipe1J4 = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(equipe1J4, NULL, ecran, &positionEquipe1J4);
				}
				
				if(E2J1 == 1)
				{
					equipe2J1 = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
					SDL_BlitSurface(equipe2J1, NULL, ecran, &positionEquipe2J1);
				}
				else
				{
					equipe2J1 = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(equipe2J1, NULL, ecran, &positionEquipe2J1);
				}
				if(E2J2 == 1)
				{
					equipe2J2 = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
					SDL_BlitSurface(equipe2J2, NULL, ecran, &positionEquipe2J2);
				}
				else
				{
					equipe2J2 = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(equipe2J2, NULL, ecran, &positionEquipe2J2);
				}
				if(E2J3 == 1)
				{
					equipe2J3 = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
					SDL_BlitSurface(equipe2J3, NULL, ecran, &positionEquipe2J3);
				}
				else
				{
					equipe2J3 = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(equipe2J3, NULL, ecran, &positionEquipe2J3);
				}
				if(E2J4 == 1)
				{
					equipe2J4 = TTF_RenderText_Solid(policeChoix, "X", couleurNoire);
					SDL_BlitSurface(equipe2J4, NULL, ecran, &positionEquipe2J4);
				}
				else
				{
					equipe2J4 = TTF_RenderText_Solid(police, "X", couleurNoire);
					SDL_BlitSurface(equipe2J4, NULL, ecran, &positionEquipe2J4);
				}
			}
		}
		affichage = 0;
		
		SDL_Flip(ecran);
		
		
		if(bloc==0)
		{
			SDL_Delay(2000);
			SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
			SDL_BlitSurface(dem, NULL, ecran, &positionFond);
			SDL_Flip(ecran);
			SDL_Delay(2000);
			continuer = 0;
		}
		
		/*
		*	Ci dessous, les evenements sur les images. Pour chaques evenements j'enregistre les données
		*/
		
		SDL_WaitEvent(&event); // Attend un evenement
        
        switch(event.type)// switch pour le type de l'evenement
        {
            case SDL_QUIT://Bouton croix de la fenetre
                continuer = 0;
                exit(1); // Ferme le programme
                break;
            case SDL_MOUSEBUTTONUP: // Clic de la souris 
            
			// pour chaque image, competer les variables blocXX avec les events
				
            	//MAGE
            	if((event.button.x > positionMageJ1.x) && (event.button.x < (positionMageJ1.x + 100))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionMageJ1.y) && (event.button.y < (positionMageJ1.y + 100)))
            		{
						if((blocM2 == 0) && (blocM3 == 0) && (blocM4 == 0))
						{
							blocM1 = 1;
							blocT1 = 0;
							blocE1 = 0;	
							blocO1 = 0;
							joueur[1].hero = 1;
							infoJ1 = 1;
							affichage = 1;	
						}
					}
				}
				if((event.button.x > positionMageJ2.x) && (event.button.x < (positionMageJ2.x + 100))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionMageJ2.y) && (event.button.y < (positionMageJ2.y + 100)))
            		{
						if((blocM1 == 0) && (blocM3 == 0) && (blocM4 == 0))
						{
							blocM2 = 1;
							blocT2 = 0;
							blocE2 = 0;	
							blocO2 = 0;	
							joueur[2].hero = 1;
							infoJ2 = 1;	
							affichage = 1;
						}
					}
				}
				if((nbJoueur == 3) ||(nbJoueur == 4))
				{
					if((event.button.x > positionMageJ3.x) && (event.button.x < (positionMageJ3.x + 100))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionMageJ3.y) && (event.button.y < (positionMageJ3.y + 100)))
	            		{
							if((blocM2 == 0) && (blocM1 == 0) && (blocM4 == 0))
							{
								blocM3 = 1;
								blocT3 = 0;
								blocE3 = 0;
								blocO3 = 0;
								joueur[3].hero = 1;	
								infoJ3 = 1;	
								affichage = 1;	
							}
						}
					}	
				}
				if(nbJoueur == 4)
				{
					if((event.button.x > positionMageJ4.x) && (event.button.x < (positionMageJ4.x + 100))) // 32 car image de 32*32 pixels
            		{
            		if((event.button.y > positionMageJ4.y) && (event.button.y < (positionMageJ4.y + 100)))
	            		{
							if((blocM2 == 0) && (blocM3 == 0) && (blocM1 == 0))
							{
								blocM4 = 1;
								blocT4 = 0;
								blocE4 = 0;
								blocO4 = 0;
								joueur[4].hero = 1;
								infoJ4 = 1;	
								affichage = 1;	
							}
						}
					}	
				}
				
            	
            	//ORK
            	if((event.button.x > positionOrkJ1.x) && (event.button.x < (positionOrkJ1.x + 100))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionOrkJ1.y) && (event.button.y < (positionOrkJ1.y + 100)))
            		{
						if((blocO2 == 0) && (blocO3 == 0) && (blocO4 == 0))
						{
							blocO1 = 1;
							blocM1 = 0;
							blocT1 = 0;
							blocE1 = 0;	
							joueur[1].hero = 2;	
							infoJ1 = 1;
							affichage = 1;
						}
					}
				}
				if((event.button.x > positionOrkJ2.x) && (event.button.x < (positionOrkJ2.x + 100))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionOrkJ2.y) && (event.button.y < (positionOrkJ2.y + 100)))
            		{
						if((blocO1 == 0) && (blocO3 == 0) && (blocO4 == 0))
						{
							blocO2 = 1;
							blocM2 = 0;
							blocT2 = 0;
							blocE2 = 0;	
							joueur[2].hero = 2;	
							infoJ2 = 1;
							affichage = 1;
						}
					}
				}
				if((nbJoueur == 3) ||(nbJoueur == 4))
				{
					if((event.button.x > positionOrkJ3.x) && (event.button.x < (positionOrkJ3.x + 100))) // 32 car image de 32*32 pixels
	            	{
	            		if((event.button.y > positionOrkJ3.y) && (event.button.y < (positionOrkJ3.y + 100)))
	            		{
							if((blocO2 == 0) && (blocO1 == 0) && (blocO4 == 0))
							{
								blocO3 = 1;
								blocM3 = 0;
								blocT3 = 0;
								blocE3 = 0;
								joueur[3].hero = 2;
								infoJ3 = 1;	
								affichage = 1;	
							}
						}
					}
				}
            	if(nbJoueur == 4)
            	{
            		if((event.button.x > positionOrkJ4.x) && (event.button.x < (positionOrkJ4.x + 100))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionOrkJ4.y) && (event.button.y < (positionOrkJ4.y + 100)))
	            		{
							if((blocO2 == 0) && (blocO3 == 0) && (blocO1 == 0))
							{
								blocO4 = 1;
								blocM4 = 0;
								blocT4 = 0;
								blocE4 = 0;
								joueur[4].hero = 2;	
								infoJ4 = 1;	
								affichage = 1;
							}
						}
					}
				}
				
				
				//TROLL
				if((event.button.x > positionTrollJ1.x) && (event.button.x < (positionTrollJ1.x + 100))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionTrollJ1.y) && (event.button.y < (positionTrollJ1.y + 100)))
            		{
						if((blocT2 == 0) && (blocT3 == 0) && (blocT4 == 0))
						{
							blocT1 = 1;
							blocM1 = 0;
							blocO1 = 0;
							blocE1 = 0;	
							joueur[1].hero = 3;	
							infoJ1 = 1;
							affichage = 1;
						}
					}
				}
				if((event.button.x > positionTrollJ2.x) && (event.button.x < (positionTrollJ2.x + 100))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionTrollJ2.y) && (event.button.y < (positionTrollJ2.y + 100)))
            		{
						if((blocT1 == 0) && (blocT3 == 0) && (blocT4 == 0))
						{
							blocT2 = 1;
							blocM2 = 0;
							blocO2 = 0;
							blocE2 = 0;	
							joueur[2].hero = 3;
							infoJ2 = 1;
							affichage = 1;
						}
					}
				}
				if((nbJoueur == 3) ||(nbJoueur == 4))
				{
					if((event.button.x > positionTrollJ3.x) && (event.button.x < (positionTrollJ3.x + 100))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionTrollJ3.y) && (event.button.y < (positionTrollJ3.y + 100)))
	            		{
							if((blocT2 == 0) && (blocT1 == 0) && (blocT4 == 0))
							{
								blocT3 = 1;
								blocM3 = 0;
								blocO3 = 0;
								blocE3 = 0;
								joueur[3].hero = 3;
								infoJ3 = 1;	
								affichage = 1;
							}
						}
					}
				}
				if(nbJoueur == 4)
				{
					if((event.button.x > positionTrollJ4.x) && (event.button.x < (positionTrollJ4.x + 100))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionTrollJ4.y) && (event.button.y < (positionTrollJ4.y + 100)))
	            		{
							if((blocT2 == 0) && (blocT3 == 0) && (blocT1 == 0))
							{
								blocT4 = 1;
								blocM4 = 0;
								blocO4 = 0;
								blocE4 = 0;
								joueur[4].hero = 3;
								infoJ4 = 1;	
								affichage = 1;
							}
						}
					}
				}
			
				
				//ELF
				if((event.button.x > positionElfJ1.x) && (event.button.x < (positionElfJ1.x + 100))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionElfJ1.y) && (event.button.y < (positionElfJ1.y + 100)))
            		{
						if((blocE2 == 0) && (blocE3 == 0) && (blocE4 == 0))
						{
							blocE1 = 1;
							blocM1 = 0;
							blocO1 = 0;
							blocT1 = 0;	
							joueur[1].hero = 4;
							infoJ1 = 1;
							affichage = 1;
						}
					}
				}
				if((event.button.x > positionElfJ2.x) && (event.button.x < (positionElfJ2.x + 100))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionElfJ2.y) && (event.button.y < (positionElfJ2.y + 100)))
            		{
						if((blocE1 == 0) && (blocE3 == 0) && (blocE4 == 0))
						{
							blocE2 = 1;
							blocM2 = 0;
							blocO2 = 0;
							blocT2 = 0;	
							joueur[2].hero = 4;
							infoJ2 = 1;
							affichage = 1;
						}
					}
				}
				if((nbJoueur == 3) ||(nbJoueur == 4))
				{
					if((event.button.x > positionElfJ3.x) && (event.button.x < (positionElfJ3.x + 100))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionElfJ3.y) && (event.button.y < (positionElfJ3.y + 100)))
	            		{
							if((blocE2 == 0) && (blocE1 == 0) && (blocE4 == 0))
							{
								blocE3 = 1;
								blocM3 = 0;
								blocO3 = 0;
								blocT3 = 0;
								joueur[3].hero = 4;	
								infoJ3 = 1;
								affichage = 1;
							}
						}
					}
				}
				if(nbJoueur == 4)
				{
					if((event.button.x > positionElfJ4.x) && (event.button.x < (positionElfJ4.x + 100))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionElfJ4.y) && (event.button.y < (positionElfJ4.y + 100)))
	            		{
							if((blocE2 == 0) && (blocE3 == 0) && (blocE1 == 0))
							{
								blocE4 = 1;
								blocM4 = 0;
								blocO4 = 0;
								blocT4 = 0;
								joueur[4].hero = 4;	
								infoJ4 = 1;
								affichage = 1;
							}
						}
					}
				}
				
				//_______________________________________________________ Joueur ou IA ?
				
				
				if((event.button.x > positionType1J.x) && (event.button.x < (positionType1J.x + 40))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionType1J.y) && (event.button.y < (positionType1J.y + 40)))
            		{
						J1 = 1; // Joueur
						A1 = 0; // IA
						joueur[1].type = 1;
						infoT1 = 1;
						affichage = 1;
					}
				}
				if((event.button.x > positionType2J.x) && (event.button.x < (positionType2J.x + 40))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionType2J.y) && (event.button.y < (positionType2J.y + 40)))
            		{
						J2 = 1; // Joueur
						A2 = 0; // IA
						joueur[2].type = 1;
						infoT2 = 1;
						affichage = 1;
					}
				}
				if(nbJoueur > 2)
				{
					if((event.button.x > positionType3J.x) && (event.button.x < (positionType3J.x + 40))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionType3J.y) && (event.button.y < (positionType3J.y + 40)))
	            		{
							J3 = 1; // Joueur
							A3 = 0; // IA
							joueur[3].type = 1;
							infoT3 = 1;
							affichage = 1;
						}
					}
				}
				if(nbJoueur == 4)
				{
					if((event.button.x > positionType4J.x) && (event.button.x < (positionType4J.x + 40))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionType4J.y) && (event.button.y < (positionType4J.y + 40)))
	            		{
							J4 = 1; // Joueur
							A4 = 0; // IA
							joueur[4].type = 1;
							infoT4 = 1;
							affichage = 1;
						}
					}
				}
				
				if((event.button.x > positionType1IA.x) && (event.button.x < (positionType1IA.x + 40))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionType1IA.y) && (event.button.y < (positionType1IA.y + 40)))
            		{
						J1 = 0; // Joueur
						A1 = 1; // IA
						joueur[1].type = 2;
						infoT1 = 1;
						affichage = 1;
					}
				}
				if((event.button.x > positionType2IA.x) && (event.button.x < (positionType2IA.x + 40))) // 32 car image de 32*32 pixels
            	{
            		if((event.button.y > positionType2IA.y) && (event.button.y < (positionType2IA.y + 40)))
            		{
						J2 = 0; // Joueur
						A2 = 1; // IA
						joueur[2].type = 2;
						infoT2 = 1;
						affichage = 1;
					}
				}
				if(nbJoueur > 2)
				{
					if((event.button.x > positionType3IA.x) && (event.button.x < (positionType3IA.x + 40))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionType3IA.y) && (event.button.y < (positionType3IA.y + 40)))
	            		{
							J3 = 0; // Joueur
							A3 = 1; // IA
							joueur[3].type = 2;
							infoT3 = 1;
							affichage = 1;
						}
					}
				}
				if(nbJoueur == 4)
				{
					if((event.button.x > positionType4IA.x) && (event.button.x < (positionType4IA.x + 40))) // 32 car image de 32*32 pixels
            		{
	            		if((event.button.y > positionType4IA.y) && (event.button.y < (positionType4IA.y + 40)))
	            		{
							J4 = 0; // Joueur
							A4 = 1; // IA
							joueur[4].type = 2;
							infoT4 = 1;
							affichage = 1;
						}
					}
				}
				
				// ------------------------------------------------------------------ Equipe 1 ou 2?
    	
				if(nbJoueur == 4)
		    	{
		    		if((event.button.x > positionEquipe1J1.x) && (event.button.x < (positionEquipe1J1.x + 40))) // 32 car image de 32*32 pixels
		            {
			            if((event.button.y > positionEquipe1J1.y) && (event.button.y < (positionEquipe1J1.y + 40)))
			            {
			            	if(nb1 < 2)
			            	{
				            	E1J1 = 1; // Equipe 1 
								joueur[1].equipe = 1;
								nb1++;
								if(E2J1 == 1)
								{
									nb2 --;
								}
								E2J1 = 0;
								infoE1 = 1;	
								affichage = 1;
							}
						}
					}
					if((event.button.x > positionEquipe1J2.x) && (event.button.x < (positionEquipe1J2.x + 40))) // 32 car image de 32*32 pixels
		            {
			            if((event.button.y > positionEquipe1J2.y) && (event.button.y < (positionEquipe1J2.y + 40)))
			            {
							if(nb1 < 2)
			            	{
				            	E1J2 = 1; // Equipe 1
								joueur[2].equipe = 1;
								nb1++;
								if(E2J2 == 1)
								{
									nb2 --;
								}
								E2J2 = 0;
								infoE2 = 1;	
								affichage = 1;
							}
							
						}
					}
					if((event.button.x > positionEquipe1J3.x) && (event.button.x < (positionEquipe1J3.x + 40))) // 32 car image de 32*32 pixels
		            {
			            if((event.button.y > positionEquipe1J3.y) && (event.button.y < (positionEquipe1J3.y + 40)))
			            {
							if(nb1 < 2)
			            	{
				            	E1J3 = 1; // Equipe 1
								joueur[3].equipe = 1;
								nb1++;
								if(E2J3 == 1)
								{
									nb2 --;
								}
								E2J3 = 0;
								infoE3 = 1;
								affichage = 1;		
							}
							
						}
					}
					if((event.button.x > positionEquipe1J4.x) && (event.button.x < (positionEquipe1J4.x + 40))) // 32 car image de 32*32 pixels
		            {
			            if((event.button.y > positionEquipe1J4.y) && (event.button.y < (positionEquipe1J4.y + 40)))
			            {
							if(nb1 < 2)
			            	{
				            	E1J4 = 1; // Equipe 1 
								joueur[4].equipe = 1;
								nb1++;
								if(E2J4 == 1)
								{
									nb2 --;
								}
								E2J4 = 0;
								infoE4 = 1;	
								affichage = 1;
							}
							
						}
					}
					
					if((event.button.x > positionEquipe2J1.x) && (event.button.x < (positionEquipe2J1.x + 40))) // 32 car image de 32*32 pixels
		            {
			            if((event.button.y > positionEquipe2J1.y) && (event.button.y < (positionEquipe2J1.y + 40)))
			            {
							if(nb2 < 2)
			            	{
				            	E2J1 = 1; // Equipe 2
								joueur[1].equipe = 2;
								nb2++;
								if(E1J1 == 1)
								{
									nb1 --;
								}
								E1J1 = 0;
								infoE1 = 1;	
								affichage = 1;
							}
							
						}
					}
					if((event.button.x > positionEquipe2J2.x) && (event.button.x < (positionEquipe2J2.x + 40))) // 32 car image de 32*32 pixels
		            {
			            if((event.button.y > positionEquipe2J2.y) && (event.button.y < (positionEquipe2J2.y + 40)))
			            {
							if(nb2 < 2)
			            	{
				            	E2J2 = 1; // Equipe 2 
								joueur[2].equipe = 2;
								nb2++;
								if(E1J2 == 1)
								{
									nb1 --;
								}
								E1J2 = 0;
								infoE2 = 1;	
								affichage = 1;
							}
							
						}
					}
					if((event.button.x > positionEquipe2J3.x) && (event.button.x < (positionEquipe2J3.x + 40))) // 32 car image de 32*32 pixels
		            {
			            if((event.button.y > positionEquipe2J3.y) && (event.button.y < (positionEquipe2J3.y + 40)))
			            {
							if(nb2 < 2)
			            	{
				            	E2J3 = 1; // Equipe 2
								joueur[3].equipe = 2;
								nb2++;
								if(E1J3 == 1)
								{
									nb1 --;
								}
								E1J3 = 0;
								infoE3 = 1;
								affichage = 1;
							}
							
						}
					}
					if((event.button.x > positionEquipe2J4.x) && (event.button.x < (positionEquipe2J4.x + 40))) // 32 car image de 32*32 pixels
		            {
			            if((event.button.y > positionEquipe2J4.y) && (event.button.y < (positionEquipe2J4.y + 40)))
			            {
							if(nb2 < 2)
			            	{
				            	E2J4 = 1; // Equipe 2
								joueur[4].equipe = 2;
								nb2++;
								if(E1J4 == 1)
								{
									nb1 --;
								}
								E1J4 = 0;
								infoE4 = 1;
								affichage = 1;
							}
							
						}
					}
				}
				
				info1 = infoJ1 + infoT1 + infoE1;
				info2 = infoJ2 + infoT2 + infoE2;
				info3 = infoJ3 + infoT3 + infoE3;
				info4 = infoJ4 + infoT4 + infoE4;
				
				info = info1 + info2 + info3 + info4; 
				if(nbJoueur == 2 && info == 4)
				{
					if((event.button.x > positionsuivant.x) && (event.button.x < (positionsuivant.x + 200))) // 32 car image de 32*32 pixels
			        {
				        if((event.button.y > positionsuivant.y) && (event.button.y < (positionsuivant.y + 100)))
				        {
							bloc = 0;
							affichage = 1;
						}
					}
				}
				if(nbJoueur == 3 && info == 6)
				{
					if((event.button.x > positionsuivant.x) && (event.button.x < (positionsuivant.x + 200))) // 32 car image de 32*32 pixels
			        {
				        if((event.button.y > positionsuivant.y) && (event.button.y < (positionsuivant.y + 100)))
				        {
							bloc = 0;
							affichage = 1;
						}
					}
				}
				if(nbJoueur == 4 && info == 12)
				{
					if((event.button.x > positionsuivant.x) && (event.button.x < (positionsuivant.x + 200))) // 32 car image de 32*32 pixels
			        {
				        if((event.button.y > positionsuivant.y) && (event.button.y < (positionsuivant.y + 100)))
				        {
							bloc = 0;
							affichage = 1;
						}
					}
				}
			
				suivant=TTF_RenderText_Solid(police, "Suivant", couleurNoire);		
        	break;
        }
        SDL_Flip(ecran); // Colle les images sur la fenetre
    }
	SDL_FreeSurface(Fond); /* On libère la surface */
	SDL_FreeSurface(ecran); /* On libère la surface */
}
