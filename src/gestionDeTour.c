#include "Gestiontour.h"
#include "interface_graphique_plateau.h"
#include "variables_globales.h"
#include "controle.h"
#include <stdio.h>
/*le but dans cette partie est de controler le jeu de facon a ce que les joueurs puissent poser des murs ou poser une armée sur une case choisie dans un intervale de temps  */
/*la fonction Gestiontour permet aux joueurs d'avancer dans le jeu selon leurs tours elle nous permet aussi grace à la variable zapper controler la fin du jeu la variable s'incrémente, si
le joueur ne joue pas son tour elle est automatiquement incrementée, si skip ateint le nombre de joueurs c'est que chaque joueur a passer son tour dans ce cas là, le jeu prends fin  */
int Gestiontour(int numero){
	
	int skip=1;
	int bon=0;

		skip=jouer(numero);// si le joueur joue sont tour skipsera egale a 0

		if(skip==1){
			zapper++;
		}
		else{
			zapper=0;
		}
		
		if (zapper==nombreDeJoueur){
			bon=1;
		}

    return bon;
}

int jouer(int numero){
	int choix = 0;
	int fin=1;
	
	choix= AffichageChoix(joueur[numero].hero,numero);//la fonction AffichageChoix permet de recuperer une valeur selon le choix du joueur,1 pour mur et 2 pour jeton.
	
	if(choix==1){
		fin = jouerMur();//si le joueur pose ses murs la fontion jouerMur va retourner 0
	}
	
	if(choix==2){
		fin=jouerPion(numero);//si le joueur pose son pion la fontion jouerPion va retourner 0
		
	}
	
	return fin;//dasn tout les cas si le joueur joue son tour completement la variable fin sera egale a 0 et sera retourner dasn jouer
}

int jouerMur(){
	int i=0;
	//int x,y;
	while(i<2){
		
		Surbrillance();//nous permet de mettre en surbrillance ou pas les murs du plateau selon leurs disponibilité
		
		poser_mur();//nous permet de poser les murs sur le plateau en fonction de leurs disponibilité ps: seul les murs en surbrillance sont posable  
		i++;
		if(finPoseMur == 1)//controle si le tour jouer mur à été joué au complet (pas de clique sur passer le tour) si oui finPoseMur reste 0 sinon elle passe a 1
		{
			i = 3;
		}
	}
	if (i==2){
		return 0;
	}
//	printf("\nFin jouer mur");
	return 1;
}


int jouerPion(int numero){
	int val;
	int fin = 1;
	val=SelectionnerPion(numero,joueur[numero].hero);//nous permet de poser un jeton sur le plateau en fonction de sa disponibilité ps:les jetons ne peuvent ABSOLUMENT PAS être posés sur les mines.

	if(finPoseJeton == 0)//controle s'il n'y a pas de clique sur passer le tour
	{
		fin = poser_jeton(val,numero);//permetde poser le jeton et retourner 0 en cas de succes et 1 en cas d'echec
	}
	
	return fin;
}









