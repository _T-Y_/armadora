#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "initialisation.h"
#include "variables_globales.h"

//--------------------------------------------------------------------------
// 							Visualisation du plateau
//--------------------------------------------------------------------------

/*
*	Permet d'avoir un affichage sur terminal du plateau
*	Utilis� dans les fontions de controle unitaire
*/
void PrintPlateau(void)
{	
	int x,y;

	printf("\n______________________________________________\n\n\tRepresentation du plateau\n______________________________________________\n\n");
	
	for(y=0;y<11;y++)
	{
		printf("\t");
		for(x=0;x<17;x++)
		{
			if(plateau[y][x].mur == 1 || plateau[y][x].mur == 0) // mur pr�sent ou non  
			{
				printf(" %d",plateau[y][x].mur);
			}
			else if(plateau[y][x].mur == 2) // jeton
			{
				printf(" .");
			}
			else // Vide
			{
				printf(" -");
			}
				
		}
		printf("\n");
	}	
}


//--------------------------------------------------------------------------
// 							Initialisation
//--------------------------------------------------------------------------

/*
*	Initialisation des mines. Valeurs attribu�es al�atoirement
*/
void InitialisationDesMines(void)
{
	plateau[9][13].mine = 1;
	plateau[9][5].mine = 1;
	plateau[7][9].mine = 1;
	plateau[7][1].mine = 1;
	plateau[3][15].mine = 1;
	plateau[3][11].mine = 1;
	plateau[3][3].mine = 1;
	plateau[1][7].mine = 1;
	
	int or[8];
	
	or[0]=7;
	or[1]=8;
	or[2]=4;
	or[3]=5;
	or[4]=6;
	or[5]=6;
	or[6]=7;
	or[7]=5;
	
	int i;
	
	srand (time (NULL));
	
	i = rand () % 8;
	plateau[9][13].orMine = or[i];
	or[i] = 0;
	
	while(or[i] == 0){i = rand () % 8;}
	plateau[9][5].orMine = or[i];
	or[i] = 0;
	
	while(or[i] == 0){i = rand () % 8;}
	plateau[7][9].orMine = or[i];
	or[i] = 0;
	
	while(or[i] == 0){i = rand () % 8;}
	plateau[7][1].orMine = or[i];
	or[i] = 0;
	
	while(or[i] == 0){i = rand () % 8;}
	plateau[3][15].orMine = or[i];
	or[i] = 0;
	
	while(or[i] == 0){i = rand () % 8;}
	plateau[3][11].orMine = or[i];
	or[i] = 0;
	
	while(or[i] == 0){i = rand () % 8;}
	plateau[3][3].orMine = or[i];
	or[i] = 0;
	
	while(or[i] == 0){i = rand () % 8;}
	plateau[1][7].orMine = or[i];
	or[i] = 0;
}

/*
*	Initialisation du plateau. Les murs de bordure sont mis � 1, le reste � 0 ou -1
*/
void InitialisationDuPlateau(void)
{
	int x,y;
	//Initialisation pour les tests
	for(y=0;y<11;y++)
	{
		for(x=0;x<17;x++)
		{
			if((x == 0 || x == 16) && (y%2 == 1))
			{
				plateau[y][x].mur = 1;
				plateau[y][x].jeton = -1;			
			}
			else if((y == 0 || y == 10)&& (x%2 == 1))
			{
				plateau[y][x].mur = 1;
				plateau[y][x].jeton = -1;	
			}
			else if(y%2 == 0 && x%2 == 1)
			{
				plateau[y][x].mur = 0;
				plateau[y][x].jeton = -1;
			}
			else if(y%2 == 1 && x%2 == 0)
			{
				plateau[y][x].mur = 0;
				plateau[y][x].jeton = -1;
			}
			else if(x%2 == 1 && y%2 == 1)// Jeton
			{
				plateau[y][x].jeton = 0;
				plateau[y][x].mur = -1;
			}	
			else // Ni jeton, ni mur
			{
				plateau[y][x].mur = -1;
				plateau[y][x].jeton = -1;
			}
			plateau[y][x].zone1 = -1;
			plateau[y][x].zone2 = -1;
			plateau[y][x].hero = 0;
			plateau[y][x].orMine = 0;
			plateau[y][x].mine = 0;
		}
	}
} 

/*
*	Init du tableau de structure Chemin
*/
void InitMemoireChemin(void)
{
	int i;
	indiceChemin = 0;
	for(i=0;i<40;i++)
	{
		chemin[i].x = -1;
		chemin[i].y = -1;
		chemin[i].direction = -1;
	}
}

/*
*	Init du tableau de structure Memoire Carrefour
*/
void InitMemoireCarrefour(void)
{
	int i;
	indiceCarrefour = 0;
	for(i=0;i<20;i++)
	{
		carrefour[i].flagchoix1 = -1;
		carrefour[i].xchoix1= -1;
		carrefour[i].ychoix1 = -1;
		carrefour[i].directionChoix1 = -1;
		carrefour[i].flagchoix2 = -1;
		carrefour[i].xchoix2= -1;
		carrefour[i].ychoix2 = -1;
		carrefour[i].directionChoix2 = -1;
		carrefour[i].flagchoix3 = -1;
		carrefour[i].xchoix3= -1;
		carrefour[i].ychoix3 = -1;
		carrefour[i].directionChoix3 = -1;
	}
}

