# JEU ARMADORA
![](https://gitlab.com/_Projet_C_/armadora/uploads/8b242fadcf0ac186fb4cef77529f9b2c/Image.jpeg)
## Créateur du jeu
Un jeu de Christwart Conrad,  
illustré par Tony Rochon,  
édité par Blackbook Editions (2013)
## Introduction  
Ce programme est une dématérialisation du jeu Armadora distiné à un OS Linux.
Ce travail a été réalisé dans le cadre d'un projet universitaire à l'UPVD.
Notre tuteur, le professeur et docteur Martin Rosalie @mart1rosalie nous a guidé dans la construction de ce logiciel. 
## Développeur/Etudiant
* Théophile YVARS
* Ilyas El Mossaoui
## Date du projet 
4eme semestre du parcours Licence informatique 2019/2020.

## Liens vers une vidéo explicative du jeu
https://www.youtube.com/watch?v=p2YNiW3VYIs&t=33s

## Contenu du logiciel  
Ce logiciel est entierement ecrit en C. Il utilise des bibliotheque SDL pour avoir une interface graphique.  
Le jeu se joue uniquement à la souri. Sur la fenetre du jeu, des evénements sont liés aux questions posées, et action souhaité sur le plateau.

## Commandes
Toutes les commandes doivent être rentré sur un terminal, dans le dossier armadora.
* commande : make // Pour compiler le projet  
* commande : make clean // Pour effacer les .o  
* commande : make clean_All // Pour effacer les .o + l'executable  

## Installation
Pour compiler et jouer au jeu, voici la démarche.  
1. make  
2. make clean  
3. ./armadora  
## Amélioration
L'IA du jeu n'est pas trés intelligente. Ceux son des fonctions de choi aléatoire qui trouve des coordonnées pour les murs comme pour les pions.
L'implémentation d'un algorithme Minimax est envisageable.

Pour rajouter du confort, l'ajout de son pour les choix de la souris ainsi que de la musique de fond feront l'affaire.
Le bibliotheque SDL gére les son également.
## Regle du jeu

Le principe du jeu - La règle de jeu est d'une simplicité enfantine. La bonne idée, c'est qu'elle se décline en deux versions:

    Une version de base dans laquelle tous les peuples fantastiques sont équivalents et n'ont aucun pouvoir particulier.
    Une version avancée dans laquelle chaque peuple a un pouvoir particulier et peut donc influencer le déroulement du jeu à sa façon.

L'auteur recommande de découvrir le jeu avec sa règle de base. Mais jouer à la règle avancée n'est pas nécessairement mieux. C'est avant tout une question de goût: la règle de base est plus épurée, donc plus proche d'un jeu de réflexion abstrait, tandis que la règle avancée est plus thématisée et du coup un peu plus chaotique.

Dans la règle de base, chaque joueur choisit sa couleur et pose les jetons de son peuple derrière son paravent. Chaque jeton a une valeur de 1 à 5, mais vous cacherez la valeur lorsque vous le poserez sur le plateau.

Le plateau de jeu est au centre de la table, les pépites d'or réparties en tas variables sur les mines d'or.  
![](https://gitlab.com/_Projet_C_/armadora/uploads/4b9c18977a6905fef409cd9f7efeef74/Plateau.jpeg)  
Chaque joueur, à son tour, effectue une action parmi deux possibles:

    Poser un jeton de son peuple sur une case libre, valeur cachée,
    Ou poser deux barrières, la seule règle étant qu'il est interdit de créer un territoire de moins de 4 cases.

La partie s'arrête quand le plateau est complet ou quand tous les joueurs ont décidé de passer.

Les joueurs révèlent les valeurs des jetons. Dans chaque territoire, le joueur qui a le plus grand score gagne toutes les pépites d'or. En cas d'égalité, les joueurs se partagent l'or autant que possible.

Le joueur qui aura le plus de pépites d'or gagnera la partie.

Le verdict - Evidemment, tout fan de jeux de territoires que je suis, Armadora ne peut que me plaire. Son petit format et sa règle épurée lui donnent l'avantage de se transporter facilement, et de s'expliquer et se jouer rapidement. C'est donc un jeu très facile à sortir.

Armadora est un jeu sans hasard, mais la pose des jetons en face cachée en font un jeu de psychologie et de bluff, et donc plein de surprises.
