#include <stdio.h>
#include <stdbool.h> // Pour type Boolean

//Include relatif  CUnit
#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"
#include "controle.h"
#include "initialisation.h"

//#include "CUnit/Automated.h"
//#include "CUnit/Console.h"

/*
Partie 1 = Recherche si mur des 2 cots d'une proposition de mur pour savoir si un zone est potentiellement existante
Partie 2 = Recherche si plusieurs chois possible
Partie 3 = Cherche coordonn pour avancer
PARTIE 7 = liaison entre PARTIE 6 et PARTIE 5
PARTIE 8 = liaison entre PARTIE 7 et PARTIE 1
*/


/************* Test case functions ****************/ 

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 1
//-------------------------------------------------------------------------------------------------------------

//Recherche si mur des 2 cots d'une proposition de mur pour savoir si un zone est potentiellement existante

int x = 15;
int y = 6;

// Test recherche d'un mur sur un coter
void test_MurPresentDroit(void)
{
	CU_ASSERT(MurPresentDroit(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}
void test_MurPresentGauche(void)
{
	CU_ASSERT(MurPresentGauche(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}
void test_MurPresentHaut(void)
{
	CU_ASSERT(MurPresentHaut(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}
void test_MurPresentBas(void)
{
	CU_ASSERT(MurPresentBas(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}

// Test appel recherche d'un mur si mur  cot n'est pas un mur de bordure
void test_MurADroite(void)
{
	CU_ASSERT(MurADroite(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}
void test_MurAGauche(void)
{
	CU_ASSERT(MurAGauche(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}
void test_MurEnHaut(void)
{
	CU_ASSERT(MurEnHaut(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}
void test_MurEnBas(void)
{
	CU_ASSERT(MurEnBas(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}

// Test si mur prsent sur les 6 possibilites autour de la proposition de mur
void test_Cont6PossHorizontal(void)
{
	CU_ASSERT(Cont6PossHorizontal(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}
void test_Cont6PossVertical(void)
{
	CU_ASSERT(Cont6PossVertical(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}
void test_Controle6Poss(void)
{
	CU_ASSERT(Controle6Poss(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}
void test_RechercheMur2Cotes(void)
{
	CU_ASSERT(RechercheMur2Cotes(x,y) == true); // controle si cette fonction avec ces parametres renvoie bien True
}


//-------------------------------------------------------------------------------------------------------------
//													PARTIE 2
//-------------------------------------------------------------------------------------------------------------

// Compte nb de choix possible
void test_CompteNbChoixPossGauche(void)
{
	CU_ASSERT(CompteNbChoixPossGauche() == 1); // controle si cette fonction avec ces parametres renvoie bien un choix
}
void test_CompteNbChoixPossDroite(void)
{
	CU_ASSERT(CompteNbChoixPossDroite() == 1); // controle si cette fonction avec ces parametres renvoie bien un choix
}
void test_CompteNbChoixPossBas(void)
{
	CU_ASSERT(CompteNbChoixPossBas() == 2); // controle si cette fonction avec ces parametres renvoie bien deux choix
}
void test_CompteNbChoixPossHaut(void)
{
	CU_ASSERT(CompteNbChoixPossHaut() == 0); // controle si cette fonction avec ces parametres renvoie bien aucun choix
}

// Pour la recherche du nb de choix possible  partir de l'origine ou non
void test_ControleNbChoixPossible(void)
{
	CU_ASSERT(ControleNbChoixPossible() == 0); // controle si cette fonction avec ces parametres renvoie bien aucun choix
}
void test_ControleNbChoixPossOrigine(void)
{
	CU_ASSERT(ControleNbChoixPossOrigine() == 2); // controle si cette fonction avec ces parametres renvoie bien deux choix
}

// Recherche si plusieurs choix possible
void test_RechercheSiPlusieurChoixPossible(void)
{
	CU_ASSERT(RechercheSiPlusieurChoixPossible() == false); // controle si cette fonction avec ces parametres renvoie bien false
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 3
//-------------------------------------------------------------------------------------------------------------

// Nouvelle coordonne

void test_RechercheCoordGauche(void)
{
	CU_ASSERT(RechercheCoordGauche() == 1); // controle si cette fonction avec ces parametres renvoie bien un choix
}
void test_RechercheCoordDroit(void)
{
	CU_ASSERT(RechercheCoordDroit() == 1); // controle si cette fonction avec ces parametres renvoie bien un choix
}
void test_RechercheCoordBas(void)
{
	CU_ASSERT(RechercheCoordBas() == 0); // controle si cette fonction avec ces parametres renvoie bien aucun choix
}
void test_RechercheCoordHaut(void)
{
	CU_ASSERT(RechercheCoordHaut() == 0); // controle si cette fonction avec ces parametres renvoie bien aucun choix
}
void test_RechercheCoordChoix(void)
{
	CU_ASSERT(RechercheCoordChoix() == 0); // controle si cette fonction avec ces parametres renvoie bien aucun choix
}
/*
void test_MemoireDirection(void)
{
	CU_ASSERT(MemoireDirection() == ); // controle si cette fonction avec ces parametres renvoie bien false
}
void test_MemoireChemin(void)
{
	CU_ASSERT(MemoireChemin() == ); // controle si cette fonction avec ces parametres renvoie bien false
}
void test_GestionMemoireCarrefour(void)
{
	CU_ASSERT(GestionMemoireCarrefour() == ); // controle si cette fonction avec ces parametres renvoie bien false
}
void test_RechercheMemoire(void)
{
	CU_ASSERT(RechercheMemoire() == ); // controle si cette fonction avec ces parametres renvoie bien false
}
void test_Avance(void)
{
	CU_ASSERT(Avance() == ); // controle si cette fonction avec ces parametres renvoie bien false
}
*/


//-------------------------------------------------------------------------------------------------------------
//													PARTIE 7
//-------------------------------------------------------------------------------------------------------------

void test_ControlePotInvalide(void)
{
	CU_ASSERT(ControlePotInvalide(x,y) == false); // controle si cette fonction avec ces parametres renvoie bien false
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 8
//-------------------------------------------------------------------------------------------------------------

void test_ControlePotZone(void)
{
	CU_ASSERT(ControlePotZone(x,y) == false); // controle si cette fonction avec ces parametres renvoie bien false
}
