#include <stdio.h>
#include <stdbool.h> // Pour type Boolean
//Include relatif � CUnit
#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"
//#include "CUnit/Automated.h"
//#include "CUnit/Console.h"

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 2
//-------------------------------------------------------------------------------------------------------------

void test_RechercheDroite(void)
{
	CU_ASSERT(RechercheDroite (4,6) == false); // controle si cette fonction avec ces parametres renvoie bien false
}

void test_RechercheGauche(void)
{
	CU_ASSERT(RechercheGauche (4,6) == false); // controle si cette fonction avec ces parametres renvoie bien false
}

void test_RechercheHaut(void)
{
	CU_ASSERT(RechercheHaut (4,6) == false); // controle si cette fonction avec ces parametres renvoie bien false
}

void test_RechercheBas(void)
{
	CU_ASSERT(RechercheBas (4,6) == false); // controle si cette fonction avec ces parametres renvoie bien false
}

void test_RechercheNouvelleCoord(void)
{
	CU_ASSERT(RechercheNouvelleCoord (4,6) == false); // controle si cette fonction avec ces parametres renvoie bien false
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 3
//-------------------------------------------------------------------------------------------------------------

void test_ComptabilisationNbYMurBas(void)
{
	CU_ASSERT(ComptabilisationNbYMurBas (4,6,-1) == 0); // controle si cette fonction avec ces parametres renvoie bien 0
}

void test_ComptabilisationNbXMurGauche(void)
{
	CU_ASSERT(ComptabilisationNbXMurGauche(2,3,100) == 0); // controle si cette fonction avec ces parametres renvoie bien 0
}

void test_RechercheCoordXGauche(void)
{
	CU_ASSERT(RechercheCoordXGauche (3,2) == 0); // controle si cette fonction avec ces parametres renvoie bien 0
}

void test_RechercheCoordYBas(void)
{
	CU_ASSERT(RechercheCoordYBas (4,5) == -1); // controle si cette fonction avec ces parametres renvoie bien -1
}

void test_ComptabilisationNbYMurHaut(void)
{
	CU_ASSERT(ComptabilisationNbYMurHaut (4,6,5) == 0); // controle si cette fonction avec ces parametres renvoie bien 0
}

void test_ComptabilisationNbXMurDroit(void)
{
	CU_ASSERT(ComptabilisationNbXMurDroit (3,2,3) == 0); // controle si cette fonction avec ces parametres renvoie bien 0
}

void test_RechercheCoordXDroit(void)
{
	CU_ASSERT(RechercheCoordXDroit(3,2) == 0); // controle si cette fonction avec ces parametres renvoie bien 0
}

void test_RechercheCoordYHaut(void)
{
	CU_ASSERT(RechercheCoordYHaut(4,6) == -1); // controle si cette fonction avec ces parametres renvoie bien -1
}

void test_ComptabilisationMur(void)
{
	CU_ASSERT(ComptabilisationMur(4,6) == 0); // controle si cette fonction avec ces parametres renvoie bien 0
}

void test_Boucle1(void)
{
	CU_ASSERT(Boucle1(1) == 1); // controle si cette fonction avec ces parametres renvoie bien 1
}

void test_Boucle2(void)
{
	CU_ASSERT(Boucle2(1) == 1); // controle si cette fonction avec ces parametres renvoie bien 1
}

void test_Boucle3(void)
{
	CU_ASSERT(Boucle3(1) == 1); // controle si cette fonction avec ces parametres renvoie bien 1
}


//-------------------------------------------------------------------------------------------------------------
//													PARTIE 5
//-------------------------------------------------------------------------------------------------------------

void test_CompteDroite(void)
{
	CU_ASSERT(CompteDroite(4,10,3,4) == 0); // controle si cette fonction avec ces parametres renvoie bien 0
}

void test_CompteGauche(void)
{
	CU_ASSERT(CompteGauche(4,10,3,4) == 0); // controle si cette fonction avec ces parametres renvoie bien 0
}

//-------------------------------------------------------------------------------------------------------------
//													PARTIE 6
//-------------------------------------------------------------------------------------------------------------

/*void test_ControleDesScores(void)
{
	CU_ASSERT(ControleDesScores() == 1); // controle si cette fonction avec ces parametres renvoie bien 1
}*/
