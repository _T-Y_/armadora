# CUNIT 2.1.2

## Téléchargement
Pour telecharger la bibliotheque  
https://sourceforge.net/projects/cunit/files/CUnit/2.1-2/

## Installation
Toutes les explications pour l'installation et l'utilisation de la bibliotheque sont sur ;  
https://sites.uclouvain.be/SystInfo/notes/Outils/html/cunit.html

## Exemple d'utilisation

Pour exemple d'utilisation  
http://perso.univ-lemans.fr/~cpiau/L2-Outils%20de%20Programmation/TD5/%20TD5-Annexe.pdf

## libcunit.so.1
Si ERREUR suivante lors de la compilation  
./controle: error while loading shared libraries: libcunit.so.1: cannot open shared object file: No such file or directory

Bien penser à mettre cette commande dans le terminal;  
export LD_LIBRARY_PATH=$HOME/local/lib:$LD_LIBRARY_PATH

## Informations

Comme la librairie n’est pas installée dans les chemins classiques,
il faut pouvoir dire à gcc où se trouvent les fichiers d’entête ainsi
que la librairie afin d’éviter les erreurs de compilation. Pour cela,
il faut spécifier à la compilation l’argument -I${HOME}/local/include
afin de lui dire qu’il doit également aller chercher des fichiers d’entête
dans le dossier $HOME/local/include en plus des chemins classiques tels
que /usr/include et /usr/local/include.

Lors de l’édition des liens avec le linker, il faut spécifier où se trouve
la librairie dynamique afin de résoudre les symboles. Pour cela, il faut
passer l’argument -lcunit pour effectuer la liaison avec la librairie
CUnit ainsi que lui spécifier -L${HOME}/local/lib afin qu’il cherche
également des librairies dans le dossier $HOME/local/lib.

## Commande GCC pour CUNIT

Donc Pour compiler;  
gcc FILE.c -I${HOME}/local/include -lcunit -L${HOME}/local/lib
