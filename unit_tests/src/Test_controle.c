#include <stdio.h>
#include <stdbool.h> // Pour type Boolean
//Include relatif � CUnit
#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"
//#include "CUnit/Automated.h"
//#include "CUnit/Console.h"
//Include relatif au projet
#include "Fonction_Test_controle.h"	// Fonction des tests
#include "variables_globales.h"
#include "initialisation.h"
#include "controle.h"



/* Test Suite setup and cleanup functions: */ 
int init_suite(void) { return 0; } 
int clean_suite(void) { return 0; } 

int main(void)
{
	// Initialisation du plateau pour les tests
	InitialisationDuPlateau();
	InitMemoireChemin();
	InitMemoireCarrefour();
	// Valeur pour test mur[y][x]
	plateau[6][15].mur = 1;
	plateau[6][13].mur = 1;
	plateau[7][12].mur = 1;
	plateau[9][12].mur = 1;
//	plateau[5][6].mur = 1;
//	plateau[7][6].mur = 1;
	
//	plateau[9][6].mur = 1;
	
	value.x = 6;
	value.y = 9;
//	plateau[4][7].mur = 1;
//	plateau[4][5].mur = 1;
//	plateau[5][4].mur = 1;

	PrintPlateau();
	// Protocole pour test
	CU_pSuite MurPresent = NULL;
	CU_pSuite CombienDeChoix = NULL; 
	CU_pSuite Avance = NULL;
	CU_pSuite Liaison = NULL;
	CU_pSuite Liaison8 = NULL;
	/* initialize the CUnit test registry */    
	if ( CUE_SUCCESS != CU_initialize_registry() )       
		return CU_get_error();
		
	/* add a suite to the registry */    
	MurPresent = CU_add_suite( "PARTIE 1 ; TEST SI PRESENCE D UN MUR DE CHAQUE COTE", init_suite, clean_suite );    
	if ( NULL == MurPresent ) 
	{       
	 	CU_cleanup_registry();       
		return CU_get_error();    
	}
	/* add a suite to the registry */    
	CombienDeChoix = CU_add_suite( "PARTIE 2 ; TEST SI PLUSIEURS POSSIBILITES", init_suite, clean_suite );    
	if ( NULL == CombienDeChoix  ) 
	{       
	 	CU_cleanup_registry();       
		return CU_get_error();    
	}
	/* add a suite to the registry */    
	Avance = CU_add_suite( "PARTIE 3 ; CHERCHE DES COORDONNEES POUR AVANCER", init_suite, clean_suite );    
	if ( NULL == Avance  ) 
	{       
	 	CU_cleanup_registry();       
		return CU_get_error();    
	}
	/* add a suite to the registry */    
	Liaison = CU_add_suite( "PARTIE 7 ; LIAISON ENTRE PARTIE 6 ET PARTIE 5", init_suite, clean_suite );    
	if ( NULL == Liaison  ) 
	{       
	 	CU_cleanup_registry();       
		return CU_get_error();    
	}
	Liaison8 = CU_add_suite( "PARTIE 8 = LIAISON ENTRE PARTIE 7 ET PARTIE 1", init_suite, clean_suite );    
	if ( NULL == Liaison8  ) 
	{       
	 	CU_cleanup_registry();       
		return CU_get_error();    
	}
	
	/* add the tests to the suite */    
	//Partie 1
	if(	(NULL == CU_add_test(MurPresent, "test_MurPresentDroit", test_MurPresentDroit)) ||
		(NULL == CU_add_test(MurPresent, "test_MurPresentGauche", test_MurPresentGauche)) ||
		(NULL == CU_add_test(MurPresent, "test_MurPresentHaut", test_MurPresentHaut)) ||
		(NULL == CU_add_test(MurPresent, "test_MurPresentBas", test_MurPresentBas))||
		(NULL == CU_add_test(MurPresent, "test_MurADroite", test_MurADroite))||
		(NULL == CU_add_test(MurPresent, "test_MurAGauche", test_MurAGauche))||
		(NULL == CU_add_test(MurPresent, "test_MurEnHaut", test_MurEnHaut))||
		(NULL == CU_add_test(MurPresent, "test_MurEnBas", test_MurEnBas))||
		(NULL == CU_add_test(MurPresent, "test_Cont6PossHorizontal", test_Cont6PossHorizontal))||
		(NULL == CU_add_test(MurPresent, "test_Cont6PossVertical", test_Cont6PossVertical))||
		(NULL == CU_add_test(MurPresent, "test_Controle6Poss", test_Controle6Poss))||
		(NULL == CU_add_test(MurPresent, "test_RechercheMur2Cotes", test_RechercheMur2Cotes))||
		(NULL == CU_add_test(MurPresent, "test_RechercheMur2Cotes", test_RechercheMur2Cotes)))
	{
		CU_cleanup_registry();       
		return CU_get_error();
	}
	//Partie 2
	if(	(NULL == CU_add_test(CombienDeChoix, "test_CompteNbChoixPossGauche", test_CompteNbChoixPossGauche))||
		(NULL == CU_add_test(CombienDeChoix, "test_CompteNbChoixPossDroite", test_CompteNbChoixPossDroite))||
		(NULL == CU_add_test(CombienDeChoix, "test_CompteNbChoixPossBas", test_CompteNbChoixPossBas))||
		(NULL == CU_add_test(CombienDeChoix, "test_CompteNbChoixPossHaut", test_CompteNbChoixPossHaut))||
		(NULL == CU_add_test(CombienDeChoix, "test_ControleNbChoixPossible", test_ControleNbChoixPossible))||
		(NULL == CU_add_test(CombienDeChoix, "test_ControleNbChoixPossOrigine", test_ControleNbChoixPossOrigine))||
		(NULL == CU_add_test(CombienDeChoix, "test_RechercheSiPlusieurChoixPossible", test_RechercheSiPlusieurChoixPossible)))
	{
		CU_cleanup_registry();
		return CU_get_error();
	}
	//Partie 3
	if(	(NULL == CU_add_test(Avance, "test_RechercheCoordGauche", test_RechercheCoordGauche))||
		(NULL == CU_add_test(Avance, "test_RechercheCoordDroit", test_RechercheCoordDroit))||
		(NULL == CU_add_test(Avance, "test_RechercheCoordBas", test_RechercheCoordBas))||
		(NULL == CU_add_test(Avance, "test_RechercheCoordHaut", test_RechercheCoordHaut))||
		(NULL == CU_add_test(Avance, "test_RechercheCoordChoix", test_RechercheCoordChoix)))
	{
		CU_cleanup_registry();       
		return CU_get_error();
	}
	//Partie 7
	if(	(NULL == CU_add_test(Liaison, "test_ControlePotInvalide", test_ControlePotInvalide)))
	{
		CU_cleanup_registry();       
		return CU_get_error();
	}
	//Partie 8
	if(	(NULL == CU_add_test(Liaison, "test_ControlePotZone", test_ControlePotZone)))
	{
		CU_cleanup_registry();       
		return CU_get_error();
	}
	
	// Run all tests using the basic interface 
	CU_basic_set_mode(CU_BRM_VERBOSE);      
	CU_basic_run_tests();      
	printf("\n");      
	CU_basic_show_failures(CU_get_failure_list());      
	printf("\n\n");   
	/*      
	// Run all tests using the automated interface      
	CU_automated_run_tests();      
	CU_list_tests_to_file();      
	// Run all tests using the console interface      
	CU_console_run_tests();   
	*/      
	/* 
	Clean up registry and return 
	*/ 
	CU_cleanup_registry();       
	return CU_get_error();
	return 1;
}
