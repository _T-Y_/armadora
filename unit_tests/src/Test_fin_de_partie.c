#include <stdio.h>
#include <stdbool.h> // Pour type Boolean
//Include relatif � CUnit
#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"
//#include "CUnit/Automated.h"
//#include "CUnit/Console.h"
//include relatif au projet
#include "fin_de_partie.h"
#include "Fonction_Test_fin_de_partie.h"	// Fonction des tests
#include "variables_globales.h"
#include "initialisation.h"

/* Test Suite setup and cleanup functions: */ 
int init_suite(void) { return 0; } 
int clean_suite(void) { return 0; } 

int main(void)
{
	
	
	InitialisationDuPlateau();
	InitMemoireChemin();
	InitMemoireCarrefour();
	InitialisationDesMines();
/*	
	plateau[6][15].mur = 1;
	plateau[6][13].mur = 1;
	plateau[7][12].mur = 1;
	plateau[9][12].mur = 1;
	plateau[5][12].mur = 1;
	plateau[3][12].mur = 1;
	plateau[1][12].mur = 1;
	
	plateau[8][7].mur = 1;
	plateau[8][9].mur = 1;
	plateau[8][11].mur = 1;
	plateau[6][7].mur = 1;
	plateau[6][9].mur = 1;
	plateau[6][11].mur = 1;
	
	plateau[9][6].mur = 1;
	plateau[7][6].mur = 1; 
	plateau[5][6].mur = 1; 
	plateau[3][6].mur = 1; 
	plateau[1][6].mur = 1; 
	
	plateau[4][1].mur = 1; 
*/
/*	
	plateau[4][1].mur = 1;
	plateau[4][3].mur = 1; 
	plateau[3][4].mur = 1; 
	plateau[1][4].mur = 1; 
	plateau[2][3].mur = 1; 
*/		
	
	plateau[4][1].mur = 1;
	plateau[4][3].mur = 1; 
	plateau[4][5].mur = 1; 
	plateau[3][6].mur = 1; 
	plateau[1][6].mur = 1;
	plateau[2][5].mur = 1;
	
	plateau[5][6].mur = 1; 
	plateau[7][6].mur = 1; 
	plateau[8][5].mur = 1; 
	plateau[9][4].mur = 1;
	
	plateau[6][7].mur = 1;
	
	plateau[5][8].mur = 1; 
	plateau[4][9].mur = 1; 
	plateau[4][11].mur = 1; 
	plateau[5][12].mur = 1;
	plateau[7][12].mur = 1; 
	plateau[8][11].mur = 1; 
	plateau[8][9].mur = 1; 
	plateau[7][8].mur = 1;
	
	plateau[6][13].mur = 1; 
	plateau[6][15].mur = 1; 
	
	PrintPlateau();
	
	
	// Protocole pour test
	CU_pSuite MurPresent = NULL;
	CU_pSuite RechercheZone = NULL;
	CU_pSuite Compte = NULL;
//	CU_pSuite ControleDesScores = NULL;
	/* initialize the CUnit test registry */    
	if ( CUE_SUCCESS != CU_initialize_registry() )       
		return CU_get_error();
	/* add a suite to the registry */    
	MurPresent = CU_add_suite( "PARTIE 2 ; Recherche si mur present", init_suite, clean_suite );    
	if ( NULL == MurPresent ) 
	{       
	 	CU_cleanup_registry();       
		return CU_get_error();    
	}
	/* add a suite to the registry */    
	RechercheZone = CU_add_suite( "PARTIE 3 ; Recherche zone", init_suite, clean_suite );    
	if ( NULL == RechercheZone ) 
	{       
	 	CU_cleanup_registry();       
		return CU_get_error();    
	}
	/* add a suite to the registry */    
	Compte = CU_add_suite( "PARTIE 5 ; Comptabilise les points", init_suite, clean_suite );    
	if ( NULL == RechercheZone ) 
	{       
	 	CU_cleanup_registry();       
		return CU_get_error();    
	}
/*	ControleDesScores = CU_add_suite( "PARTIE 6 ; ControleDesScores", init_suite, clean_suite );
	if ( NULL == RechercheZone ) 
	{       
	 	CU_cleanup_registry();       
		return CU_get_error();    
	}
	*/
		
	/* add the tests to the suite */    
	//Partie 2
	if(	(NULL == CU_add_test(MurPresent, "test_RechercheDroite", test_RechercheDroite)) ||
		(NULL == CU_add_test(MurPresent, "test_RechercheGauche", test_RechercheGauche)) ||
		(NULL == CU_add_test(MurPresent, "test_RechercheHaut", test_RechercheHaut)) ||
		(NULL == CU_add_test(MurPresent, "test_RechercheBas", test_RechercheBas)) ||
		(NULL == CU_add_test(MurPresent, "test_RechercheNouvelleCoord", test_RechercheNouvelleCoord)))
	{
		CU_cleanup_registry();       
		return CU_get_error();
	}
	//Partie 3
	if(	(NULL == CU_add_test(RechercheZone, "test_ComptabilisationNbYMurBas", test_ComptabilisationNbYMurBas)) ||
		(NULL == CU_add_test(RechercheZone, "test_ComptabilisationNbXMurGauche", test_ComptabilisationNbXMurGauche)) ||
		(NULL == CU_add_test(RechercheZone, "test_RechercheCoordXGauche", test_RechercheCoordXGauche)) ||
		(NULL == CU_add_test(RechercheZone, "test_RechercheCoordYBas", test_RechercheCoordYBas)) ||
		(NULL == CU_add_test(RechercheZone, "test_ComptabilisationNbYMurHaut", test_ComptabilisationNbYMurHaut)) ||
		(NULL == CU_add_test(RechercheZone, "test_ComptabilisationNbXMurDroit", test_ComptabilisationNbXMurDroit)) ||
		(NULL == CU_add_test(RechercheZone, "test_RechercheCoordXDroit", test_RechercheCoordXDroit)) ||
		(NULL == CU_add_test(RechercheZone, "test_RechercheCoordYHaut", test_RechercheCoordYHaut)) ||
		(NULL == CU_add_test(RechercheZone, "test_ComptabilisationMur", test_ComptabilisationMur)) ||
		(NULL == CU_add_test(RechercheZone, "test_Boucle1", test_Boucle1)) ||
		(NULL == CU_add_test(RechercheZone, "test_Boucle2", test_Boucle2)) ||
		(NULL == CU_add_test(RechercheZone, "test_Boucle3", test_Boucle3)))
	{
		CU_cleanup_registry();       
		return CU_get_error();
	}
	//Partie 5
	if(	(NULL == CU_add_test(Compte, "test_CompteDroite", test_CompteDroite)) ||
		(NULL == CU_add_test(Compte, "test_CompteGauche", test_CompteGauche)))
	{
		CU_cleanup_registry();       
		return CU_get_error();
	}
	//Partie 6
/*	if(	(NULL == CU_add_test(ControleDesScores, "test_ControleDesScores", test_ControleDesScores)))
	{
		CU_cleanup_registry();       
		return CU_get_error();
	}
	*/
	// Run all tests using the basic interface 
	CU_basic_set_mode(CU_BRM_VERBOSE);      
	CU_basic_run_tests();      
	printf("\n");      
	CU_basic_show_failures(CU_get_failure_list());      
	printf("\n\n");   
	/*      
	// Run all tests using the automated interface      
	CU_automated_run_tests();      
	CU_list_tests_to_file();      
	// Run all tests using the console interface      
	CU_console_run_tests();   
	*/      
	/* 
	Clean up registry and return 
	*/ 
	CU_cleanup_registry();       
	return CU_get_error();
	return 1;
}

