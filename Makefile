HEADERS = src/IA/include/interface_graphique_IA.h  src/IA/include/I_A.h include/variables_globales.h include/controle.h include/fin_de_partie.h include/Gestiontour.h include/initialisation.h include/menu.h src/interface/include/interface_graphique_fin_de_jeu.h src/interface/include/interface_graphique_menu.h src/interface/include/interface_graphique_plateau.h

vpath src
vpath include

default: armadora

main.o: src/main.c $(HEADERS)
	gcc -c src/main.c -o main.o -I./include -I./src/IA/include -lSDL_image -lSDL -lSDL_ttf

gestionDeTour.o: src/gestionDeTour.c $(HEADERS)
	gcc -c src/gestionDeTour.c -o gestionDeTour.o -I./include -I./src/interface/include

controle.o: src/controle.c $(HEADERS)
	gcc -c src/controle.c -o controle.o -I./include -I./src/interface/include

fin_de_partie.o: src/fin_de_partie.c $(HEADERS)
	gcc -c src/fin_de_partie.c -o fin_de_partie.o -I./include -I./src/interface/include

initialisation.o: src/initialisation.c $(HEADERS)
	gcc -c src/initialisation.c -o initialisation.o -I./include -I./src/interface/include

I_A.o: src/IA/src/I_A.c $(HEADERS)
	gcc -c src/IA/src/I_A.c -o I_A.o -I./include -I./src/IA/include

menu.o: src/menu.c $(HEADERS)
	gcc -c src/menu.c -o menu.o -I./include -I./src/interface/include

interface_graphique_fin_de_jeu.o: src/interface/src/interface_graphique_fin_de_jeu.c $(HEADERS)
	gcc -c src/interface/src/interface_graphique_fin_de_jeu.c -o interface_graphique_fin_de_jeu.o -I./include -I./src/interface/include -lSDL_image -lSDL -lSDL_ttf

interface_graphique_menu.o: src/interface/src/interface_graphique_menu.c $(HEADERS)
	gcc -c src/interface/src/interface_graphique_menu.c -o interface_graphique_menu.o -I./include -I./src/interface/include -lSDL_image -lSDL -lSDL_ttf

interface_graphique_plateau.o: src/interface/src/interface_graphique_plateau.c $(HEADERS)
	gcc -c src/interface/src/interface_graphique_plateau.c -o interface_graphique_plateau.o -I./include -I./src/interface/include -lSDL_image -lSDL -lSDL_ttf

interface_graphique_IA.o:src/IA/src/interface_graphique_IA.c $(HEADERS)
	gcc -c src/IA/src/interface_graphique_IA.c -I./include -I./src/IA/include -I./src/interface/include

armadora: main.o gestionDeTour.o controle.o fin_de_partie.o initialisation.o menu.o interface_graphique_fin_de_jeu.o interface_graphique_menu.o interface_graphique_plateau.o I_A.o interface_graphique_IA.o
	gcc main.o gestionDeTour.o controle.o fin_de_partie.o initialisation.o menu.o interface_graphique_fin_de_jeu.o interface_graphique_menu.o interface_graphique_plateau.o I_A.o interface_graphique_IA.o -o armadora -I./include -I./src/interface/include -lSDL_image -lSDL -lSDL_ttf -fno-common

clean_All:
	-rm -f main.o
	-rm -f I_A.o
	-rm -f gestionDeTour.o
	-rm -f armadora.o
	-rm -f controle.o
	-rm -f fin_de_partie.o
	-rm -f initialisation.o
	-rm -f interface_graphique_fin_de_jeu.o
	-rm -f interface_graphique_menu.o
	-rm -f interface_graphique_plateau.o
	-rm -f interface_graphique_IA.o
	-rm -f menu.o
	-rm -f armadora

clean:
	-rm -f main.o
	-rm -f I_A.o
	-rm -f gestionDeTour.o
	-rm -f armadora.o
	-rm -f controle.o
	-rm -f fin_de_partie.o
	-rm -f initialisation.o
	-rm -f interface_graphique_fin_de_jeu.o
	-rm -f interface_graphique_menu.o
	-rm -f interface_graphique_plateau.o
	-rm -f interface_graphique_IA.o
	-rm -f menu.o
