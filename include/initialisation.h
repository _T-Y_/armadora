#ifndef HEADER_CONTROLE
#define HEADER_CONTROLE

	void PrintPlateau(void);
	void InitialisationDuPlateau(void);
	void InitMemoireChemin(void);
	void InitMemoireCarrefour(void);
	void InitialisationDesMines(void);
	
#endif
