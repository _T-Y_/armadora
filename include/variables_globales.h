#ifndef HEADER_VARIABLES_GLOBALES
#define HEADER_VARIABLES_GLOBALES
	
	
	//--------------------------------------------------------------------------
	// 							Variable globale
	//--------------------------------------------------------------------------
	
	int nombreDeJoueur; // Le nombre de joueur dans la partie
	int TEMPS; // Pour le timer de chaque tour
	int finPoseMur;// murs pos�s
	int finPoseJeton;// jeton pos�
	int zapper;// Tout les joueurs ont pass� leurs tour, alors fin de partie
	
	// Structure de donn�es de chaque joueur
	struct dataJoueur  //La stucture des joueurs
	{
	    int type;
	    int hero; // 1=Mage, 2=Ork, 3=Troll, 4=Elf
	    int source[6];
	    int equipe;
	};
	struct dataJoueur joueur[5];
	
	// Donn�es du plateau
	struct donnees
	{
		int flag_interdit;
		int mur;
		int mine;
		int orMine;
		int zone1;
		int zone2;
		int jeton;
		int valeurJeton;
		int hero;
		int joueur;
	};
	typedef struct donnees donnees;
	donnees plateau[11][17]; // mur[y][x]
	
	// Utilisation dans controle.c et fin_de_partie.c
	// Pour le transfert de coord x et y afin d'analyser les possibilit�s
	struct coordmur 
	{
		int x;
		int y;
	};
	typedef struct coordmur coordmur;
	coordmur value; // Pour pass� et modifi� les coordonn�es �tudi� d'une fonction � une autre
	
	// Utilisation dans controle.c et fin_de_partie.c
	// M�morise le chemin pour contr�ler si forme zone interdite
	struct coord 
	{
		int x;
		int y;
		int direction;
	};
	typedef struct coord coord;
	coord chemin[40]; // Sauvegrade le chemin parcouru
	// Utilisation de fin_de_partie
	coord memo; // Pour garder les coords mur bordure quand on entame la recherche interieur
	coord tabOrigine[20]; // C'est 4 tableaux sont utilis� pour d�cider du chemin � prendre pour valid� une zone � la fin de partie
	coord tabChoix1[20];
	coord tabChoix2[20];
	coord tabChoix3[20];
	int indiceChemin;// Pour savoir ou le programme en est dans chemin[indiceChemin]
	
	// Utilisation dans controle.c et fin_de_partie.c
	// M�morise les carrefours. Les choix multiples
	struct memoireCarrefour
	{
	/*	int x;
		int y;
		int direction; */ // Pas sur dans avoir besoin ...
		int marqueurCarrefour; // Pour suprimer les coord chemin qu'il ne veut pas
		int flagchoix1;
		int xchoix1;
		int ychoix1;
		int directionChoix1;
		int flagchoix2;
		int xchoix2;
		int ychoix2;
		int directionChoix2;
		int flagchoix3;
		int xchoix3;
		int ychoix3;
		int directionChoix3;
	};
	typedef struct memoireCarrefour memoireCarrefour;
	memoireCarrefour carrefour[20];
	int indiceCarrefour;
	
	// Utilisation dans controle.c et fin_de_partie.c
	int indiceMemo; // Pour supprimer chemin non souhait�
	
#endif
