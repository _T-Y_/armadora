#ifndef HEADER_FIN
#define HEADER_FIN
	
	void CorrectionMemoire(void);
	void RechercheDepartNouvZone(int x,int y,int direction);
	void RechercheDepartNouvZone(int x,int y,int direction);
	int CompteNbChoixPossHautZoneInte(void);
	int CompteNbChoixPossBasZoneInte(void);
	int CompteNbChoixPossDroiteZoneInte(void);
	int CompteNbChoixPossGaucheZoneInte(void);
	int ControleNbChoixPossibleZoneInte(void);
	int RechercheCoordGaucheZoneInte(void);
	int RechercheCoordDroitZoneInte(void); // Pour un mur horizontal
	int RechercheCoordBasZoneInte(void); // Pour un mur vertical
	int RechercheCoordHautZoneInte(void); // Pour un mur vertical
	int RechercheCoordChoixZoneInte(void);
	
	
	// Partie 1
	
	void Initialisation(void); // Page 2
	
	// Partie 2
	
	_Bool RechercheDroite (int x,int y); // Page 4
	_Bool RechercheGauche (int x,int y); // Page 5
	_Bool RechercheHaut (int x,int y); // Page 6
	_Bool RechercheBas (int x,int y); // Page 7
	_Bool RechercheNouvelleCoord(int x, int y); //Page 3
	
	// Partie 3
	
	int ComptabilisationNbYMurBas(int x, int y, int Y); // Page 20
	int ComptabilisationNbXMurGauche(int x, int y, int X); // Page 21
	int RechercheCoordXGauche(int x, int y); // Page 16
	int RechercheCoordYBas(int x, int y); // Page 17
	int ComptabilisationNbYMurHaut(int x, int y, int Y); // Page 18
	int ComptabilisationNbXMurDroit(int x, int y, int X); // Page 19
	int RechercheCoordXDroit(int x, int y); // Page 14
	int RechercheCoordYHaut(int x, int y); // Page 15
	int ComptabilisationMur(int x, int y); // Page 13
	
	void InitTabOrigine(void); // Page 23
	void InitTabChoix1(void); // Page 24
	void InitTabChoix2(void); // Page 25
	void InitTabChoix3(void); // Page 26
	void InitTab(void); // Page 27
	
	int Boucle1(int var); // Page 28
	int Boucle2(int var); // Page 29
	int Boucle3(int var); // Page 30
	
	void AmeliorationRecherche(void); // Page 22
	
	void DecisionChemin (int compte1, int compte2, int compte3); // Page 31
	
	void RechercheStrategique(void); // Page 12
	
	void ChoixChemin(int x, int y); // Page 11
	
	void VideMemoirChemin(void); // Page 33

	int RechercheNbChoix(int x, int y); // Page 10
	
	void TamponneZone(void); // Page 32
	
	void RechercheZone(int x,int y); // Page 9
	
	// Partie 4
	
	_Bool RechercheSiResteMurBordure(void);// Page 8
	
	// Partie 5
	
	void InitDonneesZone(void); // Page 39
	void MinMaxMineZone(int nbzone); // Page 40
	int CompteDroite(int x,int Max,int y,int nbzone); // Page 43
	int CompteGauche(int x,int Min,int y,int nbzone); // Page 44
	void VainqueurZone(int nbzone); // Page 41
	void ComptabilisePoints(void); // Page 38
	
	// Partie 6
	
	void ControleMurImpossible(void); // Page 37
	void ChoixCheminInte(void); // Page 36
	void  RechercheZoneInte(int x,int y); // Page 35
	void RechercheInterieur(void); // Page 34
	
	void AfficheFinPartie(void); // Page 42
	
	int ControleDesScores(void); // Page 1
#endif
