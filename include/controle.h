#ifndef HEADER_CONTROLE
#define HEADER_CONTROLE

	void InitialisationDuPlateau(void);
	void InitMemoireChemin(void);
	void InitMemoireCarrefour(void);
	void PrintPlateau(void);
	
	// Partie 1
	
	_Bool MurPresentDroit(int x, int y);// Page 12
	_Bool MurPresentGauche(int x, int y);// Page 13
	_Bool MurPresentHaut(int x, int y); // Page 14
	_Bool MurPresentBas(int x, int y); // Page 15
	_Bool MurADroite(int x, int y);// Page 8
	_Bool MurAGauche(int x, int y);// Page 9
	_Bool MurEnHaut(int x, int y); // Page 10
	_Bool MurEnBas(int x, int y);// Page 11
	_Bool Cont6PossHorizontal(int x, int y); // Page 6
	_Bool Cont6PossVertical(int x, int y); // Page 7
	_Bool Controle6Poss(int x, int y); // Page 5
	_Bool RechercheMur2Cotes(int x, int y); // Page 4
	
	// Partie 2
	
	int CompteNbChoixPossGauche(void); // Page 19
	int CompteNbChoixPossDroite(void); // Page 20
	int CompteNbChoixPossBas(void);// Page 21
	int CompteNbChoixPossHaut(void); // Page 22
	int ControleNbChoixPossible(void); // Page 23
	int ControleNbChoixPossOrigine(void); // Page 18
	int RechercheSiPlusieurChoixPossible(void); // Page 17
	
	// Partie 3
	
	int RechercheCoordGauche(void); // Page 27
	int RechercheCoordDroit(void); // Page 28
	int RechercheCoordBas(void); // Page 29
	int RechercheCoordHaut(void); // Page 30
	int RechercheCoordChoix(void); // Page 26
	void MemoireDirection(void); // Page 36
	void MemoireChemin(void); // Page 33
	int GestionMemoireCarrefour(void); // Page 32
	int RechercheMemoire(void); // Page 31
	void Avance(void); // Page 25
	
	// Partie 4
	
	void CoordChoixEnvoie(void);// Page 41
	void EnregistrementChoixHaut(void); // Page 37
	void EnregistrementChoixBas(void); // Page 38
	void EnregistrementChoixDroit(void); // Page 39
	void EnregistrementChoixGauche(void); // Page 40
	void MemoireCarrefour(void); // Page 35
	
	// Partie 5
	
	void RechercheNouvCoord(void); // Page 24
	
	// Partie 6
	
	void InitMemoirCheminOrigine(void); // Page 34
	_Bool ZoneOK(void); // Page 42
	
	// Partie 7
	
	_Bool ControlePotInvalide(int x, int y); // Page 16
	
	// Partie 8
	
	_Bool ControlePotZone(int x, int y); // Page 3
	
	// Partie 9
	
	void Surbrillance(void); // Page 2
	
#endif
